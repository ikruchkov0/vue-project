import AuthService from './auth/auth-service';
import AuthConfig from './auth/auth-config';
import { AuthenticatedUser } from './auth/authenticated-user';

function getAuthService(config: AuthConfig): Promise<AuthService> {
    return new Promise<AuthService>((resolve, reject) => {
        require.ensure([], function(require){
            let authModule = require('./auth');
            let serviceInstance = authModule.getAuthService(config);
            resolve(serviceInstance);
        });
    });
}

export { AuthConfig, AuthService, AuthenticatedUser, getAuthService };