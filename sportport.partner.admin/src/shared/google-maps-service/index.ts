import GoogleMapsService from './google-maps-service';

function getGoogleMapsService(): Promise<GoogleMapsService> {
    return new Promise<GoogleMapsService>((resolve, reject) => {
        require.ensure([], function(require){
            let gmapsModule = require('./google-maps-service-factory');
            let serviceInstance = gmapsModule.getGoogleMapsService();
            resolve(serviceInstance);
        });
    });
}

export { GoogleMapsService, getGoogleMapsService };