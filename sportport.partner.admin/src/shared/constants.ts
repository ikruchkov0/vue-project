export default class AppConstants {
  public static readonly googleMapsApiKey = 'AIzaSyBwAER53yyxjN9gsfy3TTdW5DGoMpxhzmU';
  // public static readonly googleMapsClientKey = 'SportPortKey';

  public static readonly social = {
    instagramUrl: 'https://www.instagram.com/',
    vkUrl: 'https://vk.com/'
  };

  public static readonly errors = {
    serverError: 'Произошла непредвиденная ошибка на сервере.' +
                'Пожалуйста, повторите попытку позднее. ' +
                'Если ошибка будет повторяться обратитесь к администрации сервиса. ' +
                'Спасибо за ваше терпение!'
  };

  public static readonly serverConstants = {
    clientGateway: 'ClientGateway',
    partnerGateway: 'CompaniesApi'
  };
};