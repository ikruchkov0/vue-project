interface AuthConfig {
    clientId: string;
    redirectUri: string;
    postLogoutRedirectUri: string;
    silentRedirectUri: string;
    authority: string;
    scope: string;
}

export default AuthConfig;