import { AuthenticatedUser } from './authenticated-user';

interface AuthService {
    login(): Promise<any>;
    processCallback(): Promise<AuthenticatedUser>;
    getUser(): Promise<AuthenticatedUser>;
    logout(): Promise<any>;
    processLogoutCallback(): Promise<AuthenticatedUser>;
    silentLogin(): Promise<any>;
    processSilentLoginCallback(): Promise<AuthenticatedUser>;
    removeUser(): Promise<any>;
    registerEventHandler(eventName: string, callback: (...ev: any[]) => void);
}

export default AuthService;