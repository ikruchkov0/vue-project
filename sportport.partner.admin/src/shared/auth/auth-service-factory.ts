import AuthService from './auth-service';
import AuthConfig from './auth-config';
import AuthServiceImplementation from './auth-service-impl';
import AuthSettingsProvider from './auth-settings-provider';

let instance: AuthService = null;

export default function getAuthService(config: AuthConfig): AuthService {
    if (!instance) {
        instance = new AuthServiceImplementation(AuthSettingsProvider(config));
    }
    return instance;
}