import OIDC from 'oidc-client';
import AuthService from './auth-service';
import { AuthenticatedUser } from './authenticated-user';
import AuthenticatedUserImplementation from './authenticated-user-impl';

OIDC.Log.logger = console;

export default class AuthServiceImplementation implements AuthService {
    private userManager: OIDC.UserManager;

    constructor(settings: OIDC.UserManagerSettings) {
        this.userManager = new OIDC.UserManager(settings);
        this.userManager.events.addSilentRenewError((error) => {
            console.error('error while renewing the access token', error);
        });
        this.userManager.events.addUserSignedOut(() => console.error('The user has signed out'));
        this.userManager.events.addUserLoaded((loadedUser) => console.info('user loaded'));
    }

    getUser() {
        return this.userManager.getUser().then(this.mapUser).catch((error) => console.error('error while retreaving user', error));
    }

    login() {
        return this.userManager.signinRedirect().catch((error) => console.error('error while signing in', error));
    }

    logout() {
        return this.userManager.signoutRedirect().catch((error) => console.error('error while signing out', error));
    }

    processCallback(): Promise<AuthenticatedUser> {
        return this.userManager.signinRedirectCallback().then(this.mapUser).catch((error) => console.error('error while processing signin callback', error));
    }

    processLogoutCallback(): Promise<any> {
        return this.userManager.signoutRedirectCallback().catch((error) => console.error('error while processing signout callback', error));
    }

    silentLogin(): Promise<any> {
        return this.userManager.signinSilent().catch((error) => console.error('error while silent signing in', error));
    }

    processSilentLoginCallback(): Promise<AuthenticatedUser> {
        return this.userManager.signinSilentCallback().catch((error) => console.error('error while processing silent login callback', error));
    }

    removeUser() {
        return this.userManager.removeUser();
    }

    registerEventHandler(eventName: string, callback: (...ev: any[]) => void) {
        this.userManager.events['add' + eventName](callback);
    }

    private mapUser(user: OIDC.User): AuthenticatedUser {
        if (!user) {
            return;
        }
        return new AuthenticatedUserImplementation(user);
    }

}