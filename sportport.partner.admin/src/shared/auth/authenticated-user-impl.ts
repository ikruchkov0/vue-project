import OIDC from 'oidc-client';
import { AuthenticatedUser } from './authenticated-user';

class AuthenticatedUserImplementation implements AuthenticatedUser {
    accessToken: string;
    idToken: string;
    userName: string;
    userId: string;
    tokenType: string;
    expired: boolean;

    constructor(user: OIDC.User) {
        this.accessToken = user.access_token;
        this.idToken = user.id_token;
        this.userName = user.profile.name;
        this.userId = user.profile.sub;
        this.tokenType = user.token_type;
        this.expired = user.expired;
    }
}

export default AuthenticatedUserImplementation;