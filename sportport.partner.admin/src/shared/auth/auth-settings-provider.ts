import OIDC, { WebStorageStateStore } from 'oidc-client';
import AuthConfig from './auth-config';

function provider(config: AuthConfig) {
    let settings: OIDC.UserManagerSettings = {
        // URL of your OpenID Connect server.
        // The library uses it to access the metadata document
        authority: config.authority,

        client_id: config.clientId,
        redirect_uri: config.redirectUri,
        post_logout_redirect_uri: config.postLogoutRedirectUri,
        silent_redirect_uri: config.silentRedirectUri,

        scope: config.scope,

        // What you expect back from The IdP.
        // In that case, like for all JS-based applications, an identity token
        // and an access token
        response_type: 'id_token token',

        // Scopes requested during the authorisation request

        // Number of seconds before the token expires to trigger
        // the `tokenExpiring` event
        accessTokenExpiringNotificationTime: 4,

        // Do we want to renew the access token automatically when it's
        // about to expire?
        automaticSilentRenew: true,

        // Do we want to filter OIDC protocal-specific claims from the response?
        filterProtocolClaims: true,

        userStore: new WebStorageStateStore({ store: window.localStorage })
    };

    return settings;
}


export default provider;