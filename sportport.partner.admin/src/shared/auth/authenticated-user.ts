interface AuthenticatedUser {
    accessToken: string;
    idToken: string;
    userName: string;
    userId: string;
    tokenType: string;
    expired: boolean;
}

export { AuthenticatedUser };