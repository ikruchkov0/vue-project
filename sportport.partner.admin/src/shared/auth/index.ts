export { default as getAuthService } from './auth-service-factory';
export { default as AuthService } from './auth-service';