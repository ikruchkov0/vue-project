
export class ServerError extends Error {
    statusText: string;
    statusCode: number;
    errorName: string;
    static get errorName() {
        return 'ServerError';
    }

    constructor(statusCode: number, statusText: string, message: string) {
        super(message);
        this.statusText = statusText;
        this.statusCode = statusCode;
        this.errorName = ServerError.errorName;
    }
}

export class AuthorizationError extends ServerError {
    static get errorName() {
        return 'AuthorizationError';
    }

    constructor(statusCode: number, statusText: string, message: string) {
        super(statusCode, statusText, message);
        this.errorName = AuthorizationError.errorName;
    }
 }

export class ValidationError extends ServerError {
    get vlaidationError() {
        return this._validationError;
    }

    static get errorName() {
        return 'ValidationError';
    }

    constructor(statusCode: number, statusText: string, private _validationError: any) {
        super(statusCode, statusText, 'Validation error');
        this.errorName = ValidationError.errorName;
    }
}