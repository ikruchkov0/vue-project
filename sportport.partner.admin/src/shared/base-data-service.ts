import Vue from 'vue';
import * as VueRouter from 'vue-router';
import * as applicationUrls from 'applicationUrls';

import * as ServerErrors from '@sp/shared/server-errors';
import config from '@sp/shared/config';

function requestFactory(url: string, options: RequestInit) {
    console.log('Fetch url: ' + url);
    let schedIndex = url.indexOf('/sched');
    if (schedIndex > -1) {
        url = 'http://localhost:50001' + url.substr(schedIndex + '/sched'.length);
    }
    // console.log('Fetch url: ' + url);
    return new Request(url, options);
}


abstract class BaseDataService {
    constructor(private apiRoot: string,
        private authTokenProvider: () => string,
        private showSpinnerFn: () => void,
        private hideSpinnerFn: () => void) { }

    protected put(url: string, data: any) {
        return this.serverRequest(url, 'PUT', data);
    }

    protected get(url: string, data?: any) {
        return this.serverRequest(url, 'GET', data);
    }

    protected post(url: string, data: any) {
        return this.serverRequest(url, 'POST', data);
    }

    protected delete(url: string) {
        return this.serverRequest(url, 'DELETE');
    }

    private serverRequest(url: string, method: string, data?: any): Promise<any> {

        let requestParams: RequestInit = {
            method: method,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'Authorization': this.authTokenProvider()
            }
        };


        if (data) {
            if (method === 'GET') {
                let queryPart = this.getQueryString(data);
                if (queryPart) {
                    url = url + '?' + queryPart;
                }
            } else {
                requestParams.body = JSON.stringify(data);
            }
        }

        let request = requestFactory(this.mapUrl(url), requestParams);
        this.showSpinnerFn();
        return fetch(request)
            .then((response) => {
                this.hideSpinnerFn();
                return this.handleResponse(response, request);
            })
            .catch((error) => {
                this.hideSpinnerFn();
                this.handleError(error, request);
            });
    }

    private getQueryString(params) {
        let results = [];
        let keys = Object.keys(params);
        for (let key of keys) {
            let value = params[key];
            if (Array.isArray(value)) {
                if (value.length === 0)
                    continue;
                let arrayJoined = value
                    .map(val => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`)
                    .join('&');
                results.push(arrayJoined);
                continue;
            }
            if (value || value === 0) {
                results.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
            }
        }
        let result = results.join('&');
        return result;

        // return Object
        //     .keys(params)
        //     .map(k => {
        //         if (Array.isArray(params[k])) {
        //             return params[k]
        //                 .map(val => `${encodeURIComponent(k)}=${encodeURIComponent(val)}`)
        //                 .join('&');
        //         }

        //         return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`;
        //     })
        //     .join('&');
    }

    private normalizeUrl(url: string) {
        if (url.startsWith('/')) {
            return url;
        }
        return '/' + url;
    }

    private mapUrl(url: string) {
        let requestUrl = this.normalizeUrl(url);
        return this.getUrlsSchema()[this.apiRoot] + requestUrl;
    }

    private getUrlsSchema() {
        return config.Client;
    }

    private async handleResponse(response: Response, request: Request) {
        if (!response.ok) {
            let message = await response.text();
            switch (response.status) {
                case 401:
                    throw new ServerErrors.AuthorizationError(response.status, response.statusText, message);
                case 400:
                    throw new ServerErrors.ValidationError(response.status, response.statusText, message);
                default:
                    throw new ServerErrors.ServerError(response.status, response.statusText, message);
            }
        }
        switch (response.status) {
            case 204:
                console.info(`Request ${request.method}: ${request.url}: No content`);
                return Promise.resolve(null);
            default:
                return response.json();
        }
    }

    private handleError(error, request: Request) {
        console.error(`Request ${request.method}: ${request.url} error`, error);
        throw error;
    }
}

export { BaseDataService };