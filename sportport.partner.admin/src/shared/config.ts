import * as applicationUrls from 'applicationUrls';

let envUrls = process.env.NODE_ENV === 'development'
    ? applicationUrls.Development
    : applicationUrls.Production;

export default envUrls;