import { validate, validateSync, ValidationError } from 'class-validator';

abstract class BaseVM<VmClass> {
    constructor(json?: any) {
        if (json) {
            this.updateFromJson(json);
        }
    }

    clone(): VmClass {
        let constructor = this.constructor.bind.apply(this.constructor, [this.constructor, JSON.parse(this.toJson())]);
        let cloneObj = new constructor();
        for (let key in this) {
            if (this[key] instanceof BaseVM) {
                cloneObj[key] = (<any>this[key]).clone();
            }
            if (this[key] instanceof Array) {
                let arrayProp = <Array<any>><any>this[key];
                if (arrayProp.length > 0 && arrayProp[0] instanceof BaseVM) {
                    cloneObj[key] = arrayProp.forEach((x) => (<any>x).clone());
                }
            }
        }
        return cloneObj;
    }

    validate(skipMissingProperties: boolean = true): Promise<Array<ValidationError>> {
        return validate(this, { skipMissingProperties: skipMissingProperties });
    }

    validateSync(skipMissingProperties: boolean = true): Array<ValidationError> {
        return validateSync(this, { skipMissingProperties: skipMissingProperties });
    }

    protected toJson() {
        return JSON.stringify(this);
    }

    protected updateFromJson(json: any) {
        Object.keys(json).forEach((key) => {
            let obj = json[key];
            this[key] = obj;
        });
    }
}

export { BaseVM };