import { injectable } from 'inversify';

import * as ServerErrors from './server-errors';

interface ServerErrorActions {
    [errorName: string]: () => void;
    default: () => void;
    undefinedError: () => void;
}

@injectable()
export default class ErrorHandler {

    constructor(private actions: ServerErrorActions) { }

    register() {
        if (process.env.NODE_ENV !== 'production') {
            // return;
        }
        if (process.env.VUE_ENV === 'server') {
            return;
        }
        window.onerror = this.handler.bind(this);
        window.addEventListener('unhandledrejection', this.handleRejectionPromise.bind(this));
    }

    private handler(message: string, filename?: string, lineno?: number, colno?: number, error?: Error) {
        console.error(arguments);
        debugger;
    }

    private handleRejectionPromise(event: PromiseRejectionEvent) {
        let error: ServerErrors.ServerError = event.reason;
        console.error(error);
        if (!error) {
            this.actions.undefinedError();
            return;
        }
        let action = this.actions[error.errorName];
        if (!action) {
            this.actions.default();
            return;
        }
        action();
    }
}