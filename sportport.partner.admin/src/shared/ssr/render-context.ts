export interface RenderContext {
    url: string;
    state: any;
    title: string;
    description: string;
    redirectUrl: string;
}