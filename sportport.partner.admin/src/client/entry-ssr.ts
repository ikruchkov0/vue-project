import 'reflect-metadata';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { SportportContainer } from '@sp/client/ioc';
import AppServerSide from './app/ssr/app-server-side';

import { RenderContext } from '@sp/shared/ssr/render-context';

import Vuetify from 'vuetify';

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.use(Vuetify);

function createApp(context: RenderContext) {
    SportportContainer.init();
    SportportContainer.register(AppServerSide);
    return SportportContainer.getInstance<AppServerSide>(AppServerSide).getComponent(context);
}

export { createApp };

