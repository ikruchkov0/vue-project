import 'reflect-metadata';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { SportportContainer } from '@sp/client/ioc';

import Vuetify from 'vuetify';

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.use(Vuetify);

document.readyState !== 'complete' && document.readyState !== 'interactive'
    ? document.addEventListener('DOMContentLoaded', onDocumentRady)
    : onDocumentRady();

function onDocumentRady() {
    SportportContainer.init();
    let app = SportportContainer.createAppInstance();
    let appElement = document.getElementById('app');
    app.run(appElement);
}