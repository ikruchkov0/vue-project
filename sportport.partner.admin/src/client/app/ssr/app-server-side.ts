import * as VueRouter from 'vue-router';
import { injectable } from 'inversify';

import AppComponent from '../components/app/app.component';
import SportportStore from '../store';
import SportportRouter from '../router';
import SportportErrorHandler from '../error-handler';

import { ApplicationActions } from '../store/application/actions';

import { RenderContext } from '@sp/shared/ssr/render-context';

@injectable()
export default class AppServerSide {
    private component: AppComponent;

    constructor(private store: SportportStore, private router: SportportRouter, private errorHandler: SportportErrorHandler) {
        this.component = new AppComponent({ router: this.router, store: this.store });
    }

    async getComponent(context: RenderContext): Promise<AppComponent> {
        await this.initData();
        let matchedComponents = await this.setupRouter(context.url);
        await this.checkComponents(matchedComponents);
        context.state = this.store.state;
        context.title = this.store.state.seo.title;
        context.description = this.store.state.seo.description;
        context.redirectUrl = this.store.state.seo.redirectUrl;
        return this.component;
    }

    private initData() {
        return this.store.dispatch(new ApplicationActions.InitApplication());
    }

    private setupRouter(url: string): Promise<Array<any>> {
        return new Promise((resolve, reject) => {
            console.info(`Serve ${url} from ${this.router.currentRoute.fullPath}`);
            if (url === this.router.currentRoute.fullPath) {
                // do not need to macth router
                resolve(this.router.getMatchedComponents());
            }
            this.router.replace(url, () => resolve(this.router.getMatchedComponents()), (error) => reject(error));
        });
    }

    private async checkComponents(matchedComponents: Array<any>) {
        if (!matchedComponents.length) {
            console.error(`Not found ${this.router.currentRoute.fullPath}`);
            return Promise.reject({ code: 404 });
        }

        return Promise.resolve(matchedComponents);
    }
}