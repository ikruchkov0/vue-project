import { BaseVM } from '@sp/shared/base-vm';

export default class ScheduleSearchVM extends BaseVM<ScheduleSearchVM> {

    startTime: string;
    endTime: string;

    classTitle: string;
    studioTitle: string;
    studioId: number;

    address: string;
    qauantity?: number;
    categoryId: number;
}