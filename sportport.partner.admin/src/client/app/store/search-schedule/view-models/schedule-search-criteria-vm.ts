import { BaseVM } from '@sp/shared/base-vm';

const queryIgnoredProps = ['skip', 'take'];

export default class ScheduleSearchCriteriaVM extends BaseVM<ScheduleSearchCriteriaVM> {
    constructor(json?: any) {
      super(json);
      if (!this.facilities)
        this.facilities = [];
      if (!this.classCategories)
        this.classCategories = [];
    }

    title: string = '';
    facilities: Array<number>;
    classCategories: Array<number>;

    visitDate: string;
    startTime: string;
    endTime: string;

    skip: number;
    take: number;

    public toQueryDictionary() {
        let result = {};
        let keys = Object.keys(this);
        for (let i in keys) {
            let prop = this[keys[i]];
            if (queryIgnoredProps.indexOf(keys[i]) !== -1) {
                continue;
            }
            if (Array.isArray(prop) && prop.length === 0) {
                continue;
            }
            if (prop || prop === 0) {
                result[keys[i].toLowerCase()] = prop;
            }
        }
        if (Object.keys(result).length === 0)
            return null;
        return result;
    }
}