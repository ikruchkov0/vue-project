import * as Vuex from 'vuex';
import RootState from '../root-state';
import SearchScheduleState from './state';
import { SearchScheduleMutations } from './mutations';
import SearchScheduleActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new SearchScheduleState(),
    mutations: SearchScheduleMutations,
    actions: SearchScheduleActions
};

export default Module;