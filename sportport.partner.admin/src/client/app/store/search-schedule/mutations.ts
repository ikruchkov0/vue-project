import * as Vuex from 'vuex';
import State from './state';
import { ScheduleSearchVM } from './view-models';

class SetSearchSchedulesMutation {
    constructor(private _searchScheduleResult: Array<ScheduleSearchVM>, private _isContinue: boolean) { }
    static get type() { return 'setSearchSchedules'; }
    get type() { return SetSearchSchedulesMutation.type; }
    get searchScheduleResult() { return this._searchScheduleResult; }
    get isContinue() { return this._isContinue; }
}

const mutations = {
    [SetSearchSchedulesMutation.type](state: State, payload: SetSearchSchedulesMutation) {
        if (payload.isContinue) {
            state.searchScheduleResult.push(...payload.searchScheduleResult);
        } else {
            state.searchScheduleResult = payload.searchScheduleResult;
        }
    },
};

export { mutations as SearchScheduleMutations,
    SetSearchSchedulesMutation
    };