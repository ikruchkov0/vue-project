import { ScheduleSearchVM } from './view-models';

export default class SearchScheduleState {
  searchScheduleResult: Array<ScheduleSearchVM> = [];
};