import { ScheduleSearchVM, ScheduleSearchCriteriaVM } from './view-models';
import { BaseDataService } from '../base-data-service';

export default class SearchService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    searchSchedules(criteria: ScheduleSearchCriteriaVM): Promise<Array<ScheduleSearchVM>> {
      return this.get(`api/v1/Schedule/Search`, criteria)
          .then((jsonArray) => {
            // console.log('Search result:', jsonArray);
            return jsonArray.map(cc => new ScheduleSearchVM(cc));
        });
    }
}