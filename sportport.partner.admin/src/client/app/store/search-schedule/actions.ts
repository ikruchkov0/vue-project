import * as Vuex from 'vuex';
import RootState from '../root-state';

import SearchService from './search-service';
import State from './state';
import * as Mutations from './mutations';

import { ScheduleSearchCriteriaVM } from './view-models';

class SearchSchedulesAction {
    constructor(private _criteria: ScheduleSearchCriteriaVM) { }
    static get type() { return 'searchSchedulesAction'; }
    get type() { return SearchSchedulesAction.type; }
    get searchCriteria() { return this._criteria; }
}

const actions = {

    [SearchSchedulesAction.type](context: Vuex.ActionContext<State, RootState>, payload: SearchSchedulesAction) {
        let service = new SearchService();
        return service.searchSchedules(payload.searchCriteria).then((schedules) => {
            let mutation = new Mutations.SetSearchSchedulesMutation(schedules,
                payload.searchCriteria.skip === 0 ? false : true);
            context.commit(mutation);
        });
    },
};

const SearchScheduleActions = {
    SearchSchedulesAction
};

export { SearchScheduleActions };

export default actions;