import * as Vuex from 'vuex';
import RootState from '../root-state';

import OrdersService from './orders-service';
import State from './state';
import * as Mutations from './mutations';

import { SubmitOrderVM } from './view-models';

class SubmitOrderAction {
    constructor(private _order: SubmitOrderVM) { }
    static get type() { return 'submitOrderAction'; }
    get type() { return SubmitOrderAction.type; }
    get order() { return this._order; }
}

const actions = {

    [SubmitOrderAction.type](context: Vuex.ActionContext<State, RootState>, payload: SubmitOrderAction) {
        let service = new OrdersService();
        return service.submitOrder(payload.order).then((order) => {

        });
    },
};

const OrdersActions = {
    SubmitOrderAction
};

export { OrdersActions };

export default actions;