import * as Vuex from 'vuex';
import RootState from '../root-state';
import OrdersState from './state';
import { OrdersMutations } from './mutations';
import OrdersActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new OrdersState(),
    mutations: OrdersMutations,
    actions: OrdersActions
};

export default Module;