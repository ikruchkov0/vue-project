import { SubmitOrderVM } from './view-models';
import { BaseDataService } from '../base-data-service';

export default class OrdersService extends BaseDataService {
    constructor() {
        super('ClientApi');
    }

    submitOrder(order: SubmitOrderVM): Promise<any> {
      return this.post(`api/v1/Orders`, order)
          .then((order) => {
            // return jsonArray.map(cc => new ScheduleSearchVM(cc));
        });
    }
}