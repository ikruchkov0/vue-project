import { BaseVM } from '@sp/shared/base-vm';
import { validate,
    IsDefined, IsNotEmpty, IsEmail,
    MinLength, MaxLength, Equals,
    IsMobilePhone, ValidationError } from 'class-validator';

export default class SubmitOrderVM extends BaseVM<SubmitOrderVM> {

    constructor() {
        super();
        this.isAgreementAccepted = false;
    }

    productPlanId: number;

    @IsNotEmpty({ message: 'ФИО является обязательным' })
    @MinLength(10, { message: 'ФИО должно быть не короче $constraint1 символов' })
    userName: string;

    @IsNotEmpty({ message: 'Введите email' })
    @IsEmail({}, { message: 'Введите корректный email адрес (пр. email@sp.ru)' })
    email: string;

    @IsNotEmpty({ message: 'Пароль является обязательным' })
    @MinLength(6, { message: 'Пароль должен быть не короче $constraint1 символов' })
    password: string;

    @IsNotEmpty({ message: 'Введите телефон' })
    @MinLength(10, { message: 'Мобильный телефон должен быть не короче $constraint1 символов' })
    @IsMobilePhone('ru-RU', { message: 'Введите корректный мобильный телефонф, формат 9xxXXXxxxx' })
    phone: string;

    @Equals(true, { message: 'Согласие с пользовательским соглашением является обязательным' })
    isAgreementAccepted: boolean;
}