import { UIState } from './ui';
import { DictionariesState } from './dicts';
import { ApplicationState } from './application';
import { SearchScheduleState } from './search-schedule';
import { OrdersState } from './orders';
import { StudiosState } from './studios';
import { SeoState } from './seo';

export default class RootState {
    ui: UIState;
    dictionaries: DictionariesState;
    application: ApplicationState;
    searchSchedule: SearchScheduleState;
    orders: OrdersState;
    studios: StudiosState;
    seo: SeoState;

    constructor(json?: any) {
        this.ui = new UIState();
        this.dictionaries = new DictionariesState(json ? json.dictionaries : null);
        this.application = new ApplicationState();
        this.searchSchedule = new SearchScheduleState();
        this.orders = new OrdersState();
        this.studios = new StudiosState(json ? json.studios : null);
        this.seo = new SeoState(json ? json.seo : null);
    }
};