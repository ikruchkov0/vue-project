import * as Vuex from 'vuex';
import UIState from './state';

const toggleSidebar = {
    type: 'toggleSidebar'
};

const showSpinner = {
    type: 'showSpinner'
};

const hideSpinner = {
    type: 'hideSpinner'
};


const UIMutations = {
    [toggleSidebar.type](state: UIState) {
        state.showSidebar = !state.showSidebar;
    },

    [showSpinner.type](state: UIState) {
        state.showSpinner = true;
    },

     [hideSpinner.type](state: UIState) {
        state.showSpinner = false;
    },
};

export { UIMutations as UIMutations,
    toggleSidebar,
    showSpinner,
    hideSpinner };