import * as Vuex from 'vuex';
import RootState from '../root-state';
import UIState from './state';

import * as Mutations from './mutations';

const toggleSidebar = {
    type: 'toggleSidebar'
};

const showSpinner = {
    type: 'showSpinner'
};

const hideSpinner = {
    type: 'hideSpinner'
};

const actions = {
    [toggleSidebar.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.toggleSidebar);
    },

    [showSpinner.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.showSpinner);
    },

    [hideSpinner.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.hideSpinner);
    }
};

const UIActions = {
    toggleSidebar,
    showSpinner,
    hideSpinner };

export { UIActions };

export default actions;