export default class UIState {
    showSidebar: boolean = true;
    showSpinner: boolean = false;
}