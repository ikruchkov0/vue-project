import * as Vuex from 'vuex';
import SportportStore from './sportport-store';
import RootState from './root-state';
export { RootState };
export default SportportStore;