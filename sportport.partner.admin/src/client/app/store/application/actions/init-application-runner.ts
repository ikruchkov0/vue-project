import * as Vuex from 'vuex';
import Vue from 'vue';
import RootState from '../../root-state';
import ApplicationState from '../state';

import * as Mutations from '../mutations';

import { UIActions } from '../../ui/actions';
import { DictionariesActions } from '../../dicts/actions';
import { SportportContainer } from '@sp/client/ioc';
import { ApplicationActions } from '.';

class InitApplicationRunner {
    get router() {
        return SportportContainer.getRouter();
    }

    constructor(private context: Vuex.ActionContext<ApplicationState, RootState>) { }

    run(): Promise<any> {
        return this.preloadData().then(() => {
            this.context.commit(Mutations.setContentReady);
            if (process.env.VUE_ENV !== 'server') {
                let initMapServicesAction = new ApplicationActions.InitMapServices();
                this.context.dispatch(initMapServicesAction);
            }


            Promise.resolve(null);
        })
            .catch(error => {
                Promise.reject(error);
            });
    }

    private preloadData(): Promise<any> {

        let promises = [];
        if (!this.context.rootState.dictionaries.classCategories || this.context.rootState.dictionaries.classCategories.length === 0) {
            promises.push(this.context.dispatch(new DictionariesActions.LoadClassCategoriesAction()));
        }
        if (!this.context.rootState.dictionaries.facilities || this.context.rootState.dictionaries.facilities.length === 0) {
            promises.push(this.context.dispatch(new DictionariesActions.LoadStudioFacilitiesDictAction()));
        }

        return Promise.all(promises);
    }
}

export default InitApplicationRunner;
