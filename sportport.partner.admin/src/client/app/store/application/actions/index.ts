import InitApplicationRunner from './init-application-runner';
import * as Mutations from '../mutations';

import { getGoogleMapsService } from '@sp/shared/google-maps-service';
import AppConstants from '@sp/shared/constants';

class InitApplication {
    static get type() { return 'initApplication'; }
    get type() { return InitApplication.type; }
};

class InitMapServices {
    static get type() { return 'initMapServices'; }
    get type() { return InitMapServices.type; }
};

const actions = {
    [InitApplication.type]: (context) => new InitApplicationRunner(context).run(),

    [InitMapServices.type]: (context) => {
        return getGoogleMapsService().then((service) => {
            return service.load(AppConstants.googleMapsApiKey).then(google => {
                let mapsMutation = new Mutations.InitGMapServicesMutation(google);
                context.commit(mapsMutation);
            });
        });
    }
};

const ApplicationActions = { InitApplication, InitMapServices };
export { ApplicationActions };
export default actions;