import * as Vuex from 'vuex';
import { decorate, injectable } from 'inversify';

import RootState from './root-state';

import { UIModule } from './ui';
import { DictionariesModule } from './dicts';
import { ApplicationModule } from './application';
import { SearchScheduleModule } from './search-schedule';
import { OrdersModule } from './orders';
import { StudiosModule } from './studios';
import { SeoModule } from './seo';

decorate(injectable(), Vuex.Store);

@injectable()
class SportportStore extends Vuex.Store<RootState> {
    constructor() {
        super({
            strict: process.env.NODE_ENV !== 'production',
            modules: {
                application: ApplicationModule,
                ui: UIModule,
                dictionaries: DictionariesModule,
                searchSchedule: SearchScheduleModule,
                orders: OrdersModule,
                studios: StudiosModule,
                seo: SeoModule
            }
        });
    }
}

export { RootState };
export default SportportStore;