import * as Vuex from 'vuex';
import RootState from '../root-state';

import { StudioVM, ClassCategoryVM, ScheduleSearchCriteriaVM } from '@sp/client/view-models';
import StudiosService from './studios-service';
import State from './state';
import * as Mutations from './mutations';
import { SeoActions } from '../seo';

class LoadStudioDetailsAction {
    constructor(private _studioId: number) { }

    static get type() { return 'loadStudioDetails'; }
    get type() { return LoadStudioDetailsAction.type; }
    get studioId() { return this._studioId; }
}

class SearchStudiosAction {
    constructor(private _criteria: ScheduleSearchCriteriaVM) { }
    static get type() { return 'searchStudiosAction'; }
    get type() { return SearchStudiosAction.type; }
    get searchCriteria() { return this._criteria; }
}

class GetUrlDescriptionAction {
    constructor(private _relativeUrl: string) { }
    static get type() { return 'GetUrlDescriptionAction'; }
    get type() { return GetUrlDescriptionAction.type; }
    get relativeUrl() { return this._relativeUrl; }
}

class GetCriteriaDescriptionAction {
    constructor(private _criteria: ScheduleSearchCriteriaVM, private _urlPath?: string) { }
    static get type() { return 'getCriteriaDescriptionAction'; }
    get type() { return GetCriteriaDescriptionAction.type; }
    get searchCriteria() { return this._criteria; }
    get urlPath() { return this._urlPath; }
}

const actions = {

    [LoadStudioDetailsAction.type](context: Vuex.ActionContext<State, RootState>, payload: LoadStudioDetailsAction) {
        let service = new StudiosService();
        return service.getStudioDetails(payload.studioId).then((studio) => {
            let mutation = new Mutations.SetStudioDetailsMutation(studio);
            context.dispatch(new SeoActions.SetTitleAction(studio.title));
            context.dispatch(new SeoActions.SetDescriptionAction(studio.title));
            context.commit(mutation);
        });
    },

    [SearchStudiosAction.type](context: Vuex.ActionContext<State, RootState>, payload: SearchStudiosAction) {
        let service = new StudiosService();
        return service.searchStudios(payload.searchCriteria).then((studiosSearchResult) => {
            let studiosMutation = new Mutations.SetSearchStudiosMutation(studiosSearchResult.studios,
                payload.searchCriteria.skip === 0 ? false : true);
            context.commit(studiosMutation);
        });
    },

    [GetUrlDescriptionAction.type](context: Vuex.ActionContext<State, RootState>, payload: GetUrlDescriptionAction) {
        let service = new StudiosService();
        return service.searchDescription(payload.relativeUrl).then((descriptionResponse) => {
            handleDescriptionResponse(descriptionResponse, context);
        });
    },

    [GetCriteriaDescriptionAction.type](context: Vuex.ActionContext<State, RootState>, payload: GetCriteriaDescriptionAction) {
        let service = new StudiosService();
        return service.searchCriteriaDescription(payload.searchCriteria).then((descriptionResponse) => {
            handleDescriptionResponse(descriptionResponse, context);

            // TODO: Set Redirect
            let searchDescription = descriptionResponse.searchDescription;
            if (searchDescription && payload.urlPath) {
                let urlPath = payload.urlPath;
                let path = `${urlPath}/${searchDescription.relativeUrl}`;
                context.dispatch(new SeoActions.SetRedirectAction(path));
            }
        });
    },
};

const handleDescriptionResponse = (descriptionResponse, context) => {
    let description = descriptionResponse;
    if (description) {
        let criteria = descriptionResponse.searchCriteria;
        context.commit(new Mutations.SetSearchCriteriaMutation(criteria));

        context.dispatch(new SeoActions.SetTitleAction(description.seoTitle));
        context.dispatch(new SeoActions.SetDescriptionAction(description.seoDescription));
    }
    let descMutation = new Mutations.SetSearchDescriptionMutation(description);
    context.commit(descMutation);
};

const StudiosActions = {
    LoadStudioDetailsAction,
    SearchStudiosAction,
    GetUrlDescriptionAction,
    GetCriteriaDescriptionAction
};

export { StudiosActions };

export default actions;