import * as Vuex from 'vuex';
import RootState from '../root-state';
import StudiosState from './state';
import { StudiosMutations } from './mutations';
import StudiosActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new StudiosState(),
    mutations: StudiosMutations,
    actions: StudiosActions
};

export default Module;