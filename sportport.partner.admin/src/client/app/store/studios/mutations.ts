import * as Vuex from 'vuex';
import State from './state';
import { StudioVM, StudiosSearchDescriptionVM } from './view-models';
import { ScheduleSearchCriteriaVM } from '@sp/client/view-models';

class SetStudioDetailsMutation {
    constructor(private _studio: StudioVM) { }
    static get type() { return 'setStudioDetails'; }
    get type() { return SetStudioDetailsMutation.type; }
    get studio() { return this._studio; }
}

class SetSearchStudiosMutation {
    constructor(private _searchStudiosResult: Array<StudioVM>, private _isContinue: boolean) { }
    static get type() { return 'setSearchStudios'; }
    get type() { return SetSearchStudiosMutation.type; }
    get searchStudiosResult() { return this._searchStudiosResult; }
    get isContinue() { return this._isContinue; }
}

class SetSearchDescriptionMutation {
    constructor(private _searchDescriptionResult: StudiosSearchDescriptionVM) { }
    static get type() { return 'setSearchDescriptionMutation'; }
    get type() { return SetSearchDescriptionMutation.type; }
    get searchDescriptionResult() { return this._searchDescriptionResult; }
}

class SetSearchCriteriaMutation {
    constructor(private _criteria: ScheduleSearchCriteriaVM) {}
    static get type() { return 'setSearchCriteriaMutation'; }
    get type() { return SetSearchCriteriaMutation.type; }
    get searchCriteria() { return this._criteria; }
}

const mutations = {
    [SetStudioDetailsMutation.type](state: State, payload: SetStudioDetailsMutation) {
        state.currentStudio = payload.studio;
    },

    [SetSearchStudiosMutation.type](state: State, payload: SetSearchStudiosMutation) {
        if (payload.isContinue) {
            state.searchStudiosResult.push(...payload.searchStudiosResult);
        } else {
            state.searchStudiosResult = payload.searchStudiosResult;
        }
    },

    [SetSearchDescriptionMutation.type](state: State, payload: SetSearchDescriptionMutation) {
        state.searchDescription = payload.searchDescriptionResult;
    },

    [SetSearchCriteriaMutation.type](state: State, payload: SetSearchCriteriaMutation) {
        state.searchCriteria = payload.searchCriteria;
    },
};

export { mutations as StudiosMutations,
    SetStudioDetailsMutation,
    SetSearchStudiosMutation,
    SetSearchDescriptionMutation,
    SetSearchCriteriaMutation
};