import { StudioVM, ScheduleSearchCriteriaVM, StudiosDescriptionResponseVM, StudiosSearchResultVM } from '@sp/client/view-models';
import { BaseDataService } from '../base-data-service';
import AppConstants from '@sp/shared/constants';

export default class StudiosService extends BaseDataService {
    constructor() {
        super(AppConstants.serverConstants.clientGateway);
    }

    searchStudios(criteria: ScheduleSearchCriteriaVM): Promise<StudiosSearchResultVM> {
      return this.get(`api/v1/Studios/Search`, criteria)
          .then((json) => {
            let result = new StudiosSearchResultVM(json);
            return result;
        });
    }

    getStudioDetails(studioId: number): Promise<StudioVM> {
        return this.get(`api/v1/Studios/${studioId}`)
            .then((json) => {
                let studio = new StudioVM(json);
                return studio;
            });
    }

    searchDescription(relativeUrl: string): Promise<StudiosDescriptionResponseVM> {
        return this.get(`api/v1/Studios/Search/${relativeUrl}`)
          .then((json) => {
            return new StudiosDescriptionResponseVM(json);
        });
    }

    searchCriteriaDescription(criteria: ScheduleSearchCriteriaVM): Promise<StudiosDescriptionResponseVM> {
        return this.get(`api/v1/Studios/Search/description`, criteria)
          .then((json) => {
            return new StudiosDescriptionResponseVM(json);
        });
    }
}