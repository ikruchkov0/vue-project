import { BaseVM } from '@sp/shared/base-vm';
import { ScheduleSearchCriteriaVM, StudiosSearchDescriptionVM } from '@sp/client/view-models';

export default class StudiosDescriptionResponseVM extends BaseVM<StudiosDescriptionResponseVM> {
    constructor(json?: any) {
        super(json);
        if (json) {
            this.searchCriteria = new ScheduleSearchCriteriaVM(json.searchCriteria);
            this.searchDescription = new StudiosSearchDescriptionVM(json);
        }
    }

    searchDescription: StudiosSearchDescriptionVM;

    searchCriteria: ScheduleSearchCriteriaVM;
}