import { BaseVM } from '@sp/shared/base-vm';
import { StudiosSearchDescriptionVM, StudioVM } from '@sp/client/view-models';

export default class StudiosSearchResultVM extends BaseVM<StudiosSearchResultVM> {
    constructor(json?: any) {
      super(json);
      // this.searchDescription = new StudiosSearchDescriptionVM(json.searchDescription);
    }

    searchDescription: StudiosSearchDescriptionVM;
    studios: Array<StudioVM>;
}