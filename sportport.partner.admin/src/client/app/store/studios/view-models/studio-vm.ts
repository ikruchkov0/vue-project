import { validate,
    IsDefined, IsMobilePhone, IsPositive,
    IsNotEmpty, IsEmail, MinLength,
     MaxLength, Min, Max, ValidationError } from 'class-validator';
import { BaseVM } from '@sp/shared/base-vm';
import { ClassCategoryVM } from '@sp/client/view-models';

export default class StudioVM extends BaseVM<StudioVM> {
    id: number;

    companyId: number;

    @IsDefined({ message: 'Активность не определна' })
    enabled: Boolean;

    @IsDefined({ message: 'Введите название студии' })
    @IsNotEmpty({ message: 'Введите название студии' })
    @MinLength(3, { message: 'Название должно быть не короче $constraint1 символов' })
    title: string;

    description: string;

    @IsDefined({ message: 'Введите телефон студии' })
    @IsNotEmpty({ message: 'Введите телефон студии' })
    @MinLength(6, { message: 'Телефон должен быть не короче $constraint1 символов' })
    phone: string;

    // @MinLength(10, { message: 'Мобильный телефон должен быть не короче $constraint1 символов' })
    // @IsMobilePhone('ru-RU', { message: 'Телефон не соответствует формату мобильного номера' })
    mobile: string;

    @IsDefined({ message: 'Введите email' })
    @IsNotEmpty({ message: 'Введите email' })
    @IsEmail({}, { message: 'Введите корректный email адрес' })
    email: string;

    instagram: string;
    vkontakte: string;
    website: string;

    @IsDefined({ message: 'Введите город' })
    @IsNotEmpty({ message: 'Введите город' })
    @MinLength(2, { message: 'Город должен быть не короче $constraint1 символов' })
    city: string;

    @IsDefined({ message: 'Введите адрес' })
    @IsNotEmpty({ message: 'Введите адрес' })
    @MinLength(8, { message: 'Адрес должен быть не короче $constraint1 символов' })
    address: string;

    @IsDefined({ message: 'Введите область' })
    @IsNotEmpty({ message: 'Введите область' })
    @MinLength(2, { message: 'Область должна быть не короче $constraint1 символов' })
    state: string;

    lat: number;

    lng: number;

    zip: number;

    facilities: Array<number>;

    classCategories: Array<ClassCategoryVM>;

    static fromJsonArray(jsonArray: Array<any>): Array<StudioVM> {
        let studios = new Array<any>();
        for (let json of jsonArray) {
            const studio = new StudioVM(json);
            studios.push(studio);
        }
        return studios;
    }
};

