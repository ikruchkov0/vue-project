import { BaseVM } from '@sp/shared/base-vm';
import { ScheduleSearchCriteriaVM } from '@sp/client/view-models';

export default class StudiosSearchDescriptionVM extends BaseVM<StudiosSearchDescriptionVM> {

    id: number;
    title: string;
    description: string;
    relativeUrl: string;

    seoDescription: string;
    seoTitle: string;
}