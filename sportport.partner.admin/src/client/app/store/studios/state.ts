import { StudioVM, StudiosSearchDescriptionVM } from './view-models';
import { ScheduleSearchCriteriaVM } from '@sp/client/view-models';

export default class StudiosState {
  currentStudio: StudioVM;
  searchStudiosResult: Array<StudioVM> = [];
  searchDescription: StudiosSearchDescriptionVM = new StudiosSearchDescriptionVM();
  searchCriteria: ScheduleSearchCriteriaVM = new ScheduleSearchCriteriaVM();

  constructor(json?: any) {
    if (json && json.searchStudiosResult) {
      this.currentStudio = new StudioVM(json.currentStudio);
    }
    if (json && json.searchStudiosResult) {
      this.searchStudiosResult = json.searchStudiosResult.map(x => new StudioVM(x));
    }
    if (json && json.searchDescription && json.searchDescription instanceof Array) {
      this.searchDescription = json.searchDescription.map(x => new StudiosSearchDescriptionVM(x));
    }
    if (json && json.searchCriteria && json.searchCriteria instanceof Array) {
      this.searchCriteria = json.searchCriteria.map(x => new ScheduleSearchCriteriaVM(x));
    }
  }
};