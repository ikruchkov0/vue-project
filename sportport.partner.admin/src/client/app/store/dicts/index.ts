import DictionariesState from './state';
import DictionariesModule from './module';
import * as DictionariesActions from './actions';

export { DictionariesState, DictionariesModule, DictionariesActions };