import * as Vuex from 'vuex';
import RootState from '../root-state';

import DictionariesService from './services/dictionaries-service';
import ClientDictionariesService from './services/dictionaries-client-service';
import State from './state';
import * as Mutations from './mutations';

class LoadClassCategoriesAction {
    constructor() { }

    static get type() { return 'loadClassCategories'; }
    get type() { return LoadClassCategoriesAction.type; }
}

class LoadStudioFacilitiesDictAction {
    constructor() { }

    static get type() { return 'loadStudioFacilitiesDict'; }
    get type() { return LoadStudioFacilitiesDictAction.type; }
}

class LoadProductPlansDictAction {
    constructor() { }

    static get type() { return 'loadProductPlansDict'; }
    get type() { return LoadProductPlansDictAction.type; }
}

const actions = {

    [LoadClassCategoriesAction.type](context: Vuex.ActionContext<State, RootState>, payload: LoadClassCategoriesAction) {
        let service = new DictionariesService();
        return service.getClassCategories().then((cats) => {
            let mutation = new Mutations.SetClassCategoriesMutation(cats);
            context.commit(mutation);
        });
    },

    [LoadStudioFacilitiesDictAction.type](context: Vuex.ActionContext<State, RootState>, payload: LoadStudioFacilitiesDictAction) {
        let service = new DictionariesService();
        return service.loadStudioFacilitiesDict().then((facilitiesDict) => {
            let mutation = new Mutations.SetStudioFacilitiesDictMutation(facilitiesDict);
            context.commit(mutation);
        });
    },

    [LoadProductPlansDictAction.type](context: Vuex.ActionContext<State, RootState>, payload: LoadProductPlansDictAction) {
        let service = new ClientDictionariesService();
        return service.getProductPlanTypes().then((productPlans) => {
            let mutation = new Mutations.SetProductPlansDictMutation(productPlans);
            context.commit(mutation);
        });
    },
};

const DictionariesActions = {
    LoadClassCategoriesAction,
    LoadStudioFacilitiesDictAction,
    LoadProductPlansDictAction
};

export { DictionariesActions };

export default actions;