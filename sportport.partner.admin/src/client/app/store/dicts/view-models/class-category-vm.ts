import { validate, IsDefined, IsNotEmpty, IsEmail, MinLength, MaxLength, Min, Max, ValidationError } from 'class-validator';
import { BaseVM } from '@sp/shared/base-vm';

export default class ClassCategoryVM extends BaseVM<ClassCategoryVM> {
    id: number;

    @IsDefined({ message: 'Введите название категории' })
    @IsNotEmpty({ message: 'Введите название категории' })
    @MinLength(3, { message: 'Название должно быть не короче $constraint1 символов' })
    title: string;

    alias: string;

    description: string;

    selected: boolean;
};

