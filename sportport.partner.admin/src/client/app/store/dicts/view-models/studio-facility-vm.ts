import { BaseVM } from '@sp/shared/base-vm';

export default class StudioFacilityVM extends BaseVM<StudioFacilityVM> {

    id: number;

    title: string;

    selected: boolean;

    alias: string;

}