import { BaseVM } from '@sp/shared/base-vm';

export default class ProductPlanVM extends BaseVM<ProductPlanVM> {

    id: number;

    title: string;

    price: number;

    description: string;

    selected: boolean;

}