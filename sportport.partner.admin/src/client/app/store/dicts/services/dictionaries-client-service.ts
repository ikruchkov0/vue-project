import { ProductPlanVM } from '@sp/client/view-models';
import { BaseDataService } from '../../base-data-service';

export default class ClientDictionariesService extends BaseDataService {
    constructor() {
        super('ClientApi');
    }

    getProductPlanTypes(): Promise<Array<ProductPlanVM>> {
      return this.get(`api/v1/Products/Plans`)
        .then((jsonArray) => jsonArray.map(cc => new ProductPlanVM(cc)));
    }
}