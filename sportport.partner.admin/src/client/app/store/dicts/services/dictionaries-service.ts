import { ClassCategoryVM, StudioFacilityVM } from '@sp/client/view-models';
import { BaseDataService } from '../../base-data-service';

export default class DictionariesService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    getClassCategories(): Promise<Array<ClassCategoryVM>> {
      return this.get(`api/v1/Classes/ClassCategories`)
          .then((jsonArray) => jsonArray.map(cc => new ClassCategoryVM(cc)));
    }

    loadStudioFacilitiesDict(): Promise<Array<StudioFacilityVM>> {
        return this.get(`api/v1/Studios/Facilities`)
          .then((jsonArray) => jsonArray.map(cc => new StudioFacilityVM(cc)));
    }
}