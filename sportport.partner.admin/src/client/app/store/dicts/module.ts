import * as Vuex from 'vuex';
import RootState from '../root-state';
import DictionariesState from './state';
import { DictionariesMutations } from './mutations';
import DictionariesActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new DictionariesState(),
    mutations: DictionariesMutations,
    actions: DictionariesActions
};

export default Module;