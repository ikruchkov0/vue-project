import { ClassCategoryVM, StudioFacilityVM, ProductPlanVM } from './view-models';

const facilityIcons = [
  { id: 1, iconName: 'towel' },
  { id: 2, iconName: 'shower' },
  { id: 4, iconName: 'watercooler' },
  { id: 8, iconName: 'wardrobe' },
  { id: 16, iconName: 'parking' },
];

export default class DictionariesState {
  classCategories: Array<ClassCategoryVM> = [];
  facilities: Array<StudioFacilityVM> = [];
  productPlans: Array<ProductPlanVM> = [];
  facilityIcons = facilityIcons;

  constructor(json?: any) {
    if (json) {
      if (json.classCategories) {
        this.classCategories = json.classCategories.map(x => new ClassCategoryVM(x));
      }
      if (json.facilities) {
        this.facilities = json.facilities.map(x => new StudioFacilityVM(x));
      }
      if (json.productPlans) {
        this.productPlans = json.productPlans.map(x => new ProductPlanVM(x));
      }
    }
  }
};