import * as Vuex from 'vuex';
import State from './state';
import { ClassCategoryVM, StudioFacilityVM, ProductPlanVM } from './view-models';

class SetClassCategoriesMutation {
    constructor(private _categories: Array<ClassCategoryVM>) { }
    static get type() { return 'setClassCategories'; }
    get type() { return SetClassCategoriesMutation.type; }
    get categories() { return this._categories; }
}

class SetStudioFacilitiesDictMutation {
    constructor(private _facilitiesDict: Array<StudioFacilityVM>) { }
    static get type() { return 'setStudioFacilitiesDict'; }
    get type() { return SetStudioFacilitiesDictMutation.type; }
    get facilitiesDict() { return this._facilitiesDict; }
}

class SetProductPlansDictMutation {
    constructor(private _productPlansDict: Array<ProductPlanVM>) { }
    static get type() { return 'setProductPlansDict'; }
    get type() { return SetProductPlansDictMutation.type; }
    get productPlansDict() { return this._productPlansDict; }
}

const mutations = {
    [SetClassCategoriesMutation.type](state: State, payload: SetClassCategoriesMutation) {
        // payload.categories.forEach(cc => cc.selected = false);
        state.classCategories = payload.categories;
    },

    [SetStudioFacilitiesDictMutation.type](state: State, payload: SetStudioFacilitiesDictMutation) {
        state.facilities = payload.facilitiesDict;
    },

    [SetProductPlansDictMutation.type](state: State, payload: SetProductPlansDictMutation) {
        state.productPlans = payload.productPlansDict;
    }

};

export { mutations as DictionariesMutations,
    SetClassCategoriesMutation,
    SetStudioFacilitiesDictMutation,
    SetProductPlansDictMutation
    };