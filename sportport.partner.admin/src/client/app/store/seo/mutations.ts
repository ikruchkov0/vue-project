import * as Vuex from 'vuex';
import SeoState from './state';

const setTitle = {
    type: 'setTitle'
};

const setDescription = {
    type: 'setDescription'
};

const setRedirect = {
    type: 'setRedirect'
};

const SeoMutations = {
    [setTitle.type](state: SeoState, payload: string) {
        state.title = payload;
    },

    [setDescription.type](state: SeoState, payload: string) {
        state.description = payload;
    },

    [setRedirect.type](state: SeoState, payload: string) {
        state.redirectUrl = payload;
    }
};

export { SeoMutations as SeoMutations,
    setTitle,
    setDescription,
    setRedirect };