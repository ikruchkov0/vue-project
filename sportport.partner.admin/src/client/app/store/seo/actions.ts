import * as Vuex from 'vuex';
import RootState from '../root-state';
import SeoState from './state';

import * as Mutations from './mutations';

class SetTitleAction {
    static get type() { return 'setTitle'; }
    get type() { return SetTitleAction.type; }
    get title() { return this.pageTitle; }

    constructor(private pageTitle: string) { }
};

class SetDescriptionAction {
    static get type() { return 'setDescription'; }
    get type() { return SetDescriptionAction.type; }
    get description() { return this.pageDescription; }

    constructor(private pageDescription: string) { }
};

class SetRedirectAction {
    static get type() { return 'setRedirect'; }
    get type() { return SetRedirectAction.type; }
    get redirectUrl() { return this._redirectUrl; }

    constructor(private _redirectUrl: string) { }
};

const actions = {
    [SetTitleAction.type](context: Vuex.ActionContext<SeoState, RootState>, payload: SetTitleAction) {
        context.commit(Mutations.setTitle.type, payload.title);
    },

    [SetDescriptionAction.type](context: Vuex.ActionContext<SeoState, RootState>, payload: SetDescriptionAction) {
        context.commit(Mutations.setDescription.type, payload.description);
    },

    [SetRedirectAction.type](context: Vuex.ActionContext<SeoState, RootState>, payload: SetRedirectAction) {
        context.commit(Mutations.setRedirect.type, payload.redirectUrl);
    }
};

const SeoActions = {
    SetTitleAction,
    SetDescriptionAction,
    SetRedirectAction
};

export { SeoActions };

export default actions;