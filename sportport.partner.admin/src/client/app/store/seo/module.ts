import * as Vuex from 'vuex';
import RootState from '../root-state';
import SeoState from './state';
import { SeoMutations } from './mutations';
import SeoActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new SeoState(),
    mutations: SeoMutations,
    actions: SeoActions
};

export default Module;