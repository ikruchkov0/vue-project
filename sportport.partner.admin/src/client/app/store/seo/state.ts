export default class SeoState {
    title: string = '';
    description: string = '';
    redirectUrl: string = '';

    constructor(json?: any) {
        if (json) {
            this.title = json.title;
            this.description = json.description;
            this.redirectUrl = json.redirectUrl;
        }
    }
}