import { injectable } from 'inversify';

import * as ServerErrors from '@sp/shared/server-errors';
import SharedErrorHandler from '@sp/shared/error-handler';

import SportportStore from './store';

@injectable()
export default class SportportErrorHandler extends SharedErrorHandler {

    constructor(private store: SportportStore) {
        super({
            default: () => console.log('error'),
            undefinedError: () => console.log('error')
        });
    }
}