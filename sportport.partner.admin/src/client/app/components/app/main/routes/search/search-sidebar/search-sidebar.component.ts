import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './search-sidebar.template.html';

import {
  ClassCategoryVM,
  ScheduleSearchCriteriaVM
} from '@sp/client/view-models';
import { SearchScheduleActions } from '@sp/client/actions';

import * as moment from 'moment';
import 'moment/locale/ru';

import './search-sidebar.less';


class SidebarModeConstants {
  static Schedule: string = 'schedule';
  static Studios: string = 'studios';
}

@WithRender
@Component({
    components: {
    },
    name: 'SearchSidebar',
    props: {
      'itemsPerPage': { type: Number, required: true },
      'isBusy': { type: Boolean, required: false },
      'mode': { type: String, required: false, default: SidebarModeConstants.Schedule },
      'criteria': { type: ScheduleSearchCriteriaVM, required: false, default: () => new ScheduleSearchCriteriaVM() },
      'criteriaUpdatedCbk': { type: Function, required: false, default: (criteria) => Promise.resolve(null) }
    }
})

export default class SearchSidebar extends SportportVue {
    minutesInHour = 60;
    minStartTime = 8;
    maxTimeInMinutes = 900;
    sliderStep = 30;
    isBusy: boolean;
    criteriaUpdatedCbk: Function;

    mode: string;
    criteria: ScheduleSearchCriteriaVM;

    itemsPerPage;

    view = {
        classCategories: [],
        facilities: [],
        currentDay: null,
        daysOfWeek: [],
        startTimeMinutes: 0, // start time in minutes, considering start at 8 AM and end at 11 PM. 15 hours
        title: ''
    };

    get convertedStartTime() {
        let hours = Math.floor(this.view.startTimeMinutes / this.minutesInHour) + this.minStartTime;
        let hoursStr = hours < 10 ? '0' + hours : hours;

        let minutes = this.view.startTimeMinutes % this.minutesInHour;
        let minutesStr = minutes < 10 ? '0' + minutes : minutes;

        return hoursStr + ':' + minutesStr;
    }

    get sidebar() {
        return this.$$store.state.ui.showSidebar;
    }

    get isScheduleMode() {
      return this.mode === SidebarModeConstants.Schedule;
    }

    created() {
        let slicedCats = this.$$store.state.dictionaries.classCategories.slice();
        slicedCats.forEach(cc => this.view.classCategories.push({
            id: cc.id,
            title: cc.title,
            selected: this.criteria.classCategories.indexOf(cc.id) !== -1
        }));

        let slicedFacilities = this.$$store.state.dictionaries.facilities.slice();
        slicedFacilities.forEach(f => this.view.facilities.push({
            id: f.id,
            title: f.title,
            selected: this.criteria.facilities.indexOf(f.id) !== -1
        }));

        this.view.title = this.criteria.title;

        this.setupDates();

        this.updateCriteria();
    }

    setupDates() {
      moment.locale('ru');
      for (let dayNum = 0; dayNum < 7; dayNum += 1) {
        let day = moment().add(dayNum, 'days');
        this.view.daysOfWeek.push({title: day.format('DD MMM dddd'), date: day.format()});
      }
      this.view.currentDay = this.view.daysOfWeek[0];
    }

    dayChanged(dayObj) {
      this.view.currentDay = dayObj;
    }

    sliderChange(value) {
        let deviation = this.sliderStep - value % this.sliderStep;
        this.view.startTimeMinutes = this.view.startTimeMinutes + deviation;
    }

    updateCriteria(): Promise<any> {
      let criteria = new ScheduleSearchCriteriaVM();

      if (this.isScheduleMode) {
        criteria.visitDate = this.view.currentDay.date;
        criteria.startTime = this.convertedStartTime;
        criteria.endTime = '23:00:00';
      }

      criteria.title = this.view.title;
      if (this.view.facilities.length > 0)
        criteria.facilities = this.view.facilities
          .filter(f => f.selected)
          .map(f => f.id);
      if (this.view.classCategories.length > 0)
        criteria.classCategories = this.view.classCategories
          .filter(cc => cc.selected)
          .map(cc => cc.id);

      criteria.skip = 0;
      criteria.take = this.itemsPerPage;

      return this.criteriaUpdatedCbk(criteria);
    }

    searchItems() {
        this.updateCriteria()
          .then(() => {
            this.$emit('search');
          })
          .catch(err => {

          });
    }
}