import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './studio-card.template.html';

import { StudioVM } from '@sp/client/view-models';

@WithRender
@Component({
    components: {
    },
    name: 'studio-card',
    props: ['studio']
})
export default class StudioCard extends SportportVue {
    studio: StudioVM;
}