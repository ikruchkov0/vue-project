import {
    ScheduleSearchCriteriaVM } from '@sp/client/view-models';
import { StudiosActions } from '@sp/client/actions';

export class SearchRouteHandler {

    getQueryString(params) {
        if (!params) {
            return '';
        }
        let results = [];
        let keys = Object.keys(params);
        for (let key of keys) {
            let value = params[key];
            if (Array.isArray(value)) {
                if (value.length === 0)
                    continue;
                let arrayJoined = value
                    .map(val => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`)
                    .join('&');
                results.push(arrayJoined);
                continue;
            }
            if (value || value === 0) {
                results.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
            }
        }
        let result = results.join('&');
        if (result) {
            result = `?${result}`;
        }
        return result;
    }

    populateCriteriaFromUrlQuery(query, store): ScheduleSearchCriteriaVM {
        let queryCategories = query.classcategories;
        let queryFacilities = query.facilities;
        let queryTitle = query.title;
        let queryMetro = query.metro;

        let criteria = new ScheduleSearchCriteriaVM();

        if (queryCategories) {
            criteria.classCategories = this.getArrayValue(queryCategories)
                .map(v => parseInt(v, 10));
        }
        if (queryFacilities) {
            criteria.facilities = this.getArrayValue(queryFacilities)
                .map(v => parseInt(v, 10));
        }
        if (queryTitle) {
            criteria.title = queryTitle;
        }
        // TODO: Metro
        return criteria;
    }

    private getArrayValue(queryValue): Array<any> {
        if (!queryValue)
            return [];
        if (Array.isArray(queryValue) && queryValue.length > 0) {
            return queryValue;
        }
        else {
            return [parseInt(queryValue, 10)];
        }
    }

    handleRouteQuery(query, store, urlPath?: string): Promise<any> {
        let criteria = this.populateCriteriaFromUrlQuery(query, store);
        console.log('HandleRouteQuery', criteria);
        return store.dispatch(new StudiosActions.GetCriteriaDescriptionAction(criteria, urlPath))
            .then(() => {
            })
            .catch(err => {
                // TODO: Handle error
            });
    }

    handleRouteParams(params, store): Promise<any> {
        let firstSubpath = params.firstSubpath;
        if (firstSubpath) {
            let relativeUrl = firstSubpath;
            let studiosState = store.state.studios;
            console.log('handleRouteParams', params);
            return store.dispatch(new StudiosActions.GetUrlDescriptionAction(relativeUrl))
                .then(() => {
                })
                .catch(err => {
                    // TODO: Handle error
                });
        }

        return Promise.resolve({});
    }

    mergeCriteries(firstCriteria: ScheduleSearchCriteriaVM, secondCriteria: ScheduleSearchCriteriaVM): ScheduleSearchCriteriaVM {
        if (!secondCriteria)
            return firstCriteria;
        if (!firstCriteria)
            return secondCriteria;

        let resultCriteria = Object.assign(new ScheduleSearchCriteriaVM(),
                firstCriteria, secondCriteria);
        resultCriteria.facilities = this.mergeArrays(resultCriteria.facilities, firstCriteria.facilities);
        resultCriteria.classCategories = this.mergeArrays(resultCriteria.classCategories, firstCriteria.classCategories);

        return resultCriteria;
    }

    private mergeArrays(resultArray: Array<any>, mergableArray: Array<any>): Array<any> {
        if (!resultArray)
            return mergableArray;
        if (!mergableArray)
            return resultArray;

        return [
            ...resultArray,
            ...mergableArray
        ];
    }

}