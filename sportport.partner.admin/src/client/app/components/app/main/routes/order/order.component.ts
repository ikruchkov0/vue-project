import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './order.template.html';

import { ProductPlanVM, SubmitOrderVM } from '@sp/client/view-models';
import { ValidationError } from 'class-validator';
import { OrdersActions, DictionariesActions } from '@sp/client/actions';

import './order.less';

@WithRender
@Component({
    components: {

    },
    name: 'Order'
})
export default class Order extends SportportVue {
    view = {
        selctedProductPlan: new ProductPlanVM(),
        productPlans: [],
        isBusy: false,
        currentStep: 1,
        cardHeight: '520px',
        cardTitleHeight: '40px',
        isShowDialogWarning: false,
        dialogText: 'Необходимо согласие с пользовательским соглашением.'
    };

    validationResults: Array<ValidationError> = [];
    order: SubmitOrderVM = new SubmitOrderVM();

    get productPlans(): Array<ProductPlanVM> {
        return this.$$store.state.dictionaries.productPlans;
    }

    created() {
        this.order = Object.assign(new SubmitOrderVM(), this.order, { userName: '', email: '', phone: '', password: ''});

        this.loadProductPlans();

        // TODO: replace with this.productPlans.slice()
        // this.view.productPlans = [
        //     { id: 1, title: 'Начальный', price: 1600, description: 'Start desc. Seat amet dollar swift umo tecto ame spectrome.' },
        //     { id: 2, title: 'Базовый', price: 2500, description: 'Basic desc. Seat amet dollar swift umo tecto ame spectrome.' },
        //     { id: 3, title: 'Спорт', price: 3600, description: 'Sport desc. Seat amet dollar swift umo tecto ame spectrome.' }
        // ];
    }

    validate(propertyName?: string) {
        let errors = this.order.validateSync();
        if (propertyName) {
            let newError = errors.find(e => e.property === propertyName);
            if (!newError) {
                this.validationResults = this.validationResults.filter(vr => vr.property !== propertyName);
                return;
            }
            let existingError = this.validationResults.find(vr => vr.property === propertyName);
            if (existingError) {
                existingError = { ...existingError, newError};
            } else {
                this.validationResults = [ ...this.validationResults, newError];
            }
            return;
        }
        this.validationResults = errors;
    }

    controlHint(propertyName) {
        let propState = this.validationResults.find(vr => vr.property === propertyName);
        if (propState) {
            let messages = Object.keys(propState.constraints).map(x => propState.constraints[x]);
            return messages[0];
        } else {
            return true;
        }
    }

    nextStep() {
        this.view.isBusy = true;
        this.validate(null);

        if (!this.checkAgreement()) {
            this.view.isBusy = false;
            return;
        }

        if (this.validationResults.length > 0) {
            this.view.isBusy = false;
            return;
        }

        if (!this.view.selctedProductPlan || !this.view.selctedProductPlan.id) {
            this.showWarningDialog('Абонемент выбран некорректно. Пожалуйста, перезагрузите страницу и повторите снова.');
            this.view.isBusy = false;
            return;
        }

        this.order.productPlanId = this.view.selctedProductPlan.id;

        this.$$store.dispatch(new OrdersActions.SubmitOrderAction(this.order))
            .then(orderResult => {
                this.view.currentStep = 2;
                this.view.isBusy = false;
            })
            .catch(error => {
                this.handleServerError(error);
            });

    }

    private loadProductPlans() {
        this.view.isBusy = true;
        this.$$store.dispatch(new DictionariesActions.LoadProductPlansDictAction())
            .then(res => {
                this.view.isBusy = false;

                let productPlanId = parseInt(this.$route.query.productPlan, 10);
                if (!isNaN(productPlanId)) {
                    let foundPlan = this.view.productPlans.find(pp => pp.id === productPlanId);
                    if (foundPlan) {
                        this.view.selctedProductPlan = foundPlan;
                        return;
                    }
                }
                this.view.selctedProductPlan = this.view.productPlans[0];
            })
            .catch(error => {
                this.handleServerError(error);
            });
    }

    private checkAgreement(): boolean {
        if (this.order.isAgreementAccepted === false) {
            this.showWarningDialog('Необходимо согласие с пользовательским соглашением.');
            return false;
        }

        return true;
    }

    private handleServerError(error) {
        this.showWarningDialog(
            'Произошла непредвиденная ошибка на сервере.' +
            'Пожалуйста, повторите попытку позднее. ' +
            'Если ошибка будет повторяться обратитесь к администрации сервиса. ' +
            'Спасибо за ваше терпение!');
        this.view.isBusy = false;
    }

    private showWarningDialog(text: string) {
        this.view.dialogText = text;
        this.view.isShowDialogWarning = true;
    }

}