import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './search.template.html';

import { ClassCategoryVM, ScheduleSearchCriteriaVM, ScheduleSearchVM } from '@sp/client/view-models';
import { SearchScheduleActions } from '@sp/client/actions';

import ScheduleCard from './schedule-card/schedule-card.component';
import SearchSidebar from '../search-sidebar/search-sidebar.component';

import InfiniteLoading from 'vue-infinite-loading';

import './search.less';

@WithRender
@Component({
    components: {
        'schedule-card': ScheduleCard,
        'search-sidebar': SearchSidebar,
        'infiniteLoading': InfiniteLoading
    },
    name: 'Search'
})
export default class Search extends SportportVue {
    currentCriteria: ScheduleSearchCriteriaVM;
    itemsPerPage = 5;
    currentItemsCount = 0;

    isBusy: boolean = false;

    get searchResults(): Array<ScheduleSearchVM> {
        return this.$$store.state.searchSchedule.searchScheduleResult;
    }

    loadMore() {
        if (this.currentCriteria) {
            this.currentCriteria.skip = this.searchResults.length;
            this.search();
        }
    }

    criteriaUpdated(scheduleSearchCriteria: ScheduleSearchCriteriaVM) {
        this.currentCriteria = scheduleSearchCriteria;
    }

    search() {
        this.isBusy = true;
        let scheduleSearchCriteria = this.currentCriteria;
        this.$$store.dispatch(
            new SearchScheduleActions.SearchSchedulesAction(scheduleSearchCriteria))
            .then(() => {
                this.isBusy = false;
                if (this.searchResults.length === this.currentItemsCount) {
                    console.log('INFINITE: NO MORE RESULTS');
                    this.emitRef(this.$refs.infiniteLoading, '$InfiniteLoading:complete');
                    return;
                }
                this.currentItemsCount = this.searchResults.length;
                this.emitRef(this.$refs.infiniteLoading, '$InfiniteLoading:loaded');
            })
            .catch(errors => {
                this.isBusy = false;
                this.emitRef(this.$refs.infiniteLoading, '$InfiniteLoading:complete');
                // TODO: Log error
            });
    }

    emitRef(emitRefTarget, eventName: string) {
        if (emitRefTarget) {
            emitRefTarget['$emit'](eventName);
        }
    }
}