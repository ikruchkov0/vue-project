import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './schedule-card.template.html';

import { ScheduleSearchVM } from '@sp/client/view-models';

import './schedule-card.less';

@WithRender
@Component({
    components: {
    },
    name: 'schedule-card',
    props: ['scheduleItem']
})
export default class ScheduleCard extends SportportVue {
    scheduleItem: ScheduleSearchVM;

    formattedTime(timeString) {
        return timeString.substr(0, 5);
    }

    getClassCategory(classCategoryId) {
      let classCategory = this.$$store.state.dictionaries.classCategories
        .filter(cc => cc.id === classCategoryId);
      if (classCategory.length === 0)
        return '?';
      return classCategory[0].title;
    }
}