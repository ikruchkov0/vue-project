import Search from './search/schedule/search.component';
import SearchStudios from './search/studios/search-studios.component';
import Order from './order/order.component';
import Studio from './studio/studio.component';

export default [
    {
        path: ''
    },
    {
        name: 'search',
        path: 'search',
        component: Search
    },
    {
        name: 'order',
        path: 'order',
        component: Order
    },
    {
        path: ':city?/search/studios/:firstSubpath?/:secondSubpath?',
        name: 'studios',
        component: SearchStudios
    },
    {
        path: 'studios/:id',
        name: 'studio',
        component: Studio
    }
];