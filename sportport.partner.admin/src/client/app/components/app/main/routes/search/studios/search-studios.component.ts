import Component from 'vue-class-component';
import Vue from 'vue';
import SportportVue from '@sp/client/sportport-vue';
import {
    ScheduleSearchCriteriaVM,
    StudioVM,
    StudiosSearchDescriptionVM } from '@sp/client/view-models';
import { StudiosActions, SeoActions } from '@sp/client/actions';
import { SportportContainer } from '@sp/client/ioc';

import * as WithRender from './search-studios.template.html';

import SearchSidebar from '../search-sidebar/search-sidebar.component';

import InfiniteLoading from 'vue-infinite-loading';

import StudioCard from './studio-card/studio-card.component';
import { SearchRouteHandler } from './search-route-handler';

import './search-studios.less';

@WithRender
@Component({
    components: {
        'studio-card': StudioCard,
        'search-sidebar': SearchSidebar,
        'infiniteLoading': InfiniteLoading
    },
    name: 'SearchStudios'
})
export default class SearchStudios extends SportportVue {
    currentCriteria: ScheduleSearchCriteriaVM;
    itemsPerPage = 20;
    currentItemsCount = 0;

    isBusy: boolean = false;
    routeHelper: SearchRouteHandler;

    get facilitiesDict() {
        return this.$$store.state.dictionaries.facilities;
    }

    get classCategoriesDict() {
        return this.$$store.state.dictionaries.classCategories;
    }

    get studiosState() {
        return this.$$store.state.studios;
    }

    get searchResults(): Array<StudioVM> {
        return this.studiosState.searchStudiosResult;
    }

    get searchDescription(): StudiosSearchDescriptionVM {
        return this.studiosState.searchDescription;
    }

    get searchCriteria(): ScheduleSearchCriteriaVM {
        return this.studiosState.searchCriteria;
    }

    get pageTitle(): string {
        if (this.searchDescription && this.searchDescription.title) {
            return this.searchDescription.title;
        }
        return null;
    }

    get pageDescription(): string {
        if (this.searchDescription && this.searchDescription.description) {
            return this.searchDescription.description;
        }
        return null;
    }

    created() {
        this.routeHelper = new SearchRouteHandler();
        if (this.searchDescription && this.searchDescription.relativeUrl) {
            this.currentCriteria = this.searchCriteria;
            this.handleSeoUrl();
        } else {
            let queryCriteria = this.routeHelper.populateCriteriaFromUrlQuery(this.$route.query, this.$$store);
            this.currentCriteria = queryCriteria;
        }

    }

    beforeRouteEnter(to, from, next) {
        let store = SportportContainer.getStore();
        let routeHelper = new SearchRouteHandler();

        if (to.params.firstSubpath) { // SEO URL
            routeHelper.handleRouteParams(to.params, store)
                .then(x => next(), err => next());
        } else {
            routeHelper.handleRouteQuery(to.query, store, to.path)
                .then(x => next(), err => next());
        }
    }

    // when route changes and this component is already rendered,
    // the logic will be slightly different.
    beforeRouteUpdate(to, from, next) {
        console.log('ROUTE UPDATED', to.path);
        let queryPart = this.routeHelper.getQueryString(to.query);
        let urlPath = to.path + queryPart;
        window.history.replaceState(null, null, urlPath);
    }

    loadMore() {
        if (this.currentCriteria) {
            this.currentCriteria.skip = this.searchResults.length;
            this.loadData();
        }
    }

    criteriaUpdated(studiosSearchCriteria: ScheduleSearchCriteriaVM): Promise<any> {
        this.currentCriteria = studiosSearchCriteria;
        return Promise.resolve(null);
    }

    search() {
        this.$$store.dispatch(new StudiosActions.GetCriteriaDescriptionAction(this.currentCriteria))
            .then(() => {
                this.setUrlDescription();
            })
            .catch(err => {
                // TODO: Handle error
            });
        this.loadData();
    }

    private loadData() {

        this.isBusy = true;
        let studiosSearchCriteria = this.currentCriteria;
        this.$$store.dispatch(
            new StudiosActions.SearchStudiosAction(studiosSearchCriteria))
            .then(() => {
                this.isBusy = false;

                if (this.searchResults.length === this.currentItemsCount) {
                    console.log('INFINITE: NO MORE RESULTS');
                    this.emitRef(this.$refs.infiniteLoading, '$InfiniteLoading:complete');
                    return;
                }
                this.currentItemsCount = this.searchResults.length;
                this.emitRef(this.$refs.infiniteLoading, '$InfiniteLoading:loaded');
            })
            .catch(errors => {
                this.isBusy = false;
                this.emitRef(this.$refs.infiniteLoading, '$InfiniteLoading:complete');
                console.log('STUDIOS FETCH FAILED');
                // TODO: Log error
            });
    }

    setUrlDescription() {
        if (this.searchDescription && this.searchDescription.relativeUrl) {
            this.handleSeoUrl();
        } else {
            // TODO: FORCE TO Set url search params
            // UPDATE SEARCH
            if (this.currentCriteria) {
                let query = this.currentCriteria.toQueryDictionary();
                console.log('ROUTER QUERY', query);
                this.$router.push({name: 'studios', query: query});
            }
        }
    }

    private handleSeoUrl() {
        let relativeUrl = this.searchDescription.relativeUrl;
        let urlParts = relativeUrl.split('/');
        // Note: Checking if the same url
        if (this.$route.params.firstSubpath === urlParts[0]
            && this.$route.params.secondSubpath === urlParts[1]) {
            return;
        }
        if (urlParts.length === 1) {
            console.log('SEO URL');
            this.$router.push({name: 'studios', params: {firstSubpath: relativeUrl}});
        }
        if (urlParts.length === 2) {
            this.$router.push({name: 'studios',
                params: {firstSubpath: urlParts[0], secondSubpath: urlParts[1]}
            });
        }
    }

    emitRef(emitRefTarget, eventName: string) {
        if (emitRefTarget) {
            emitRefTarget['$emit'](eventName);
        }
    }
}