import Component from 'vue-class-component';

import SportportVue from '@sp/client/sportport-vue';

import * as WithRender from './studio-card.template.html';

@WithRender
@Component({
    components: {
    },
    name: 'StudioCard',
    props: {
      'isBusy': { type: Boolean, required: false },
      'height': { type: String, required: false, default: 'auto' }
    }
})
export default class StudioCard extends SportportVue {
  isBusy: boolean;
  height: string;
}