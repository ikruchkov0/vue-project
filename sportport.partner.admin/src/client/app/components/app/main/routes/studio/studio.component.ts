import Component from 'vue-class-component';

import SportportVue from '@sp/client/sportport-vue';
import { StudioVM } from '@sp/client/view-models';
import { StudiosActions } from '@sp/client/actions';
import { GoogleMapDirective } from '@sp/shared/directives';
import { SvgIcon } from '@sp/shared/components';
import AppConstants from '@sp/shared/constants';
import { ModalDialog } from '@sp/client/shared-components';

import * as WithRender from './studio.template.html';

import './studio.less';
import StudioCard from './studio-card/studio-card.component';
import StudioDescription from './studio-description/studio-description.component';
import StudioGallery from './studio-gallery/studio-gallery.component';

import { SportportContainer } from '@sp/client/ioc';

@WithRender
@Component({
  components: {
    'svg-icon': SvgIcon,
    'studio-card': StudioCard,
    'modal-dialog': ModalDialog,
    'studio-description': StudioDescription,
    'studio-gallery': StudioGallery,
  },
  directives: {
    'google-map': GoogleMapDirective
  },
  name: 'Studio'
})
export default class Studio extends SportportVue {
  view = {
    isBusy: false,
    dialogModalVisible: false
  };
  studioLoaded = false;

  get studioId() {
    return Number.parseInt(this.$route.params.id);
  }

  get storedStudio(): StudioVM {
    return this.$$store.state.studios.currentStudio;
  }

  get studio(): StudioVM {
    if (this.studioLoaded) {
      return this.storedStudio;
    }
    return null;
  }

  get studioEmailLink(): string {
    if (this.studioLoaded && this.studio.email) {
      return 'mailto:' + this.studio.email + '?subject=[SportPort] Студия ' + this.studio.title;
    }
    return null;
  }

  get studioVkLink(): string {
    if (this.studioLoaded && this.studio.vkontakte) {
      return AppConstants.social.vkUrl + this.studio.vkontakte;
    }
    return null;
  }

  get studioInstaLink(): string {
    if (this.studioLoaded && this.studio.instagram) {
      return AppConstants.social.instagramUrl + this.studio.instagram;
    }
    return null;
  }

  get GoogleMaps() {
    return this.$$store.state.application.googleMaps;
  }

  get errorConstants() {
    return AppConstants.errors;
  }

  facilityIcon(facilityId: number) {
    let iconInfo = this.$$store.state.dictionaries.facilityIcons.find(fi => fi.id === facilityId);
    if (iconInfo) {
      return iconInfo.iconName;
    }
    return null;
  }

  facilityName(facilityId: number) {
    let facility = this.$$store.state.dictionaries.facilities.find(fi => fi.id === facilityId);
    if (facility) {
      return facility.title;
    } else {
      return 'Приятное дополнение';
    }
  }

  created() {
    this.view.isBusy = true;
    this.loadStudio();
  }

  beforeRouteEnter(to, from, next) {
    let studioId = Number.parseInt(to.params.id);
    let store = SportportContainer.getStore();
    if (store.state.studios.currentStudio && store.state.studios.currentStudio.id === studioId) {
      next();
      return;
    }
    store.dispatch(new StudiosActions.LoadStudioDetailsAction(studioId))
      .then(x => next(), err => next());
  }

  // when route changes and this component is already rendered,
  // the logic will be slightly different.
  beforeRouteUpdate(to, from, next) {
    this.studioLoaded = false;
    this.loadStudio().then(() => next(), (err) => next());
  }

  private loadStudio() {
    if (!this.studioId) {
      return Promise.resolve(null);
    }

    if (this.studioLoaded) {
      return Promise.resolve({});
    }

    if (this.storedStudio && this.studioId === this.storedStudio.id) {
      this.view.isBusy = false;
      this.studioLoaded = true;
      return Promise.resolve({});
    }

    this.$$store.dispatch(new StudiosActions.LoadStudioDetailsAction(this.studioId))
      .then(() => {
        this.view.isBusy = false;
        this.studioLoaded = true;
      })
      .catch(() => {
        this.view.isBusy = false;
        this.view.dialogModalVisible = true;
      });
  }
}