import Component from 'vue-class-component';

import SportportVue from '@sp/client/sportport-vue';
import { StudioVM } from '@sp/client/view-models';
import {  } from '@sp/client/actions';

import * as WithRender from './studio-description.template.html';

import StudioCard from '../studio-card/studio-card.component';

@WithRender
@Component({
    components: {
        'studio-card': StudioCard
    },
    name: 'StudioDescription',
    props: ['studio', 'isBusy']
})
export default class StudioDescription extends SportportVue {
    studio: StudioVM;
    isBusy: boolean;
}