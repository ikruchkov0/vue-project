import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './main.template.html';

import { Content, Header, Footer } from './components';


import { ApplicationActions } from '@sp/client/actions';

import './main.less';

@WithRender
@Component({
    components: {
        'main-header': Header,
        'main-content': Content,
        'main-footer': Footer
    },
    name: 'Main',
})
export default class Main extends SportportVue {
    mounted() {
    }
 }