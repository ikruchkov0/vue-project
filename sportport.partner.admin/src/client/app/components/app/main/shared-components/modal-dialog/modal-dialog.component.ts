import Component from 'vue-class-component';

import SportportVue from '@sp/client/sportport-vue';
import {  } from '@sp/client/view-models';
import {  } from '@sp/client/actions';

import * as WithRender from './modal-dialog.template.html';

@WithRender
@Component({
    components: {
    },
    name: 'ModalDialog',
    props: {
        'isVisible': { type: Boolean, required: true, default: false },
        'title': { type: String, required: true, default: 'Window' },
        'icon': { type: String, required: false, default: null },
        'type': { type: String, required: false, default: 'info' }
    }
})
export default class ModalDialog extends SportportVue {
    isVisible: boolean;
    title: string;
    icon: string;
    type: string;

    warningClasses = 'yellow darken-3';
    errorClasses = 'red darken-3';
    infoClasses = 'blue darken-1';

    get headerClasses() {
        if (this.type === 'info') {
            return this.infoClasses;
        }
        if (this.type === 'warning') {
            return this.warningClasses;
        }
        if (this.type === 'error') {
            return this.errorClasses;
        }
        return '';
    }
}