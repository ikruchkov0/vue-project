import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './content.template.html';
import './content.less';

@WithRender
@Component({
    name: 'Content',
    components: {
    },
})
export default class Content extends SportportVue {
}