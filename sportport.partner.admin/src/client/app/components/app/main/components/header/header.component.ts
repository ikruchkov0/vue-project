import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './header.template.html';
import { UIActions } from '@sp/client/actions';

import './header.less';

@WithRender
@Component({
    components: {
    },
    name: 'Header',
})
export default class Header extends SportportVue {

    navigate(routeName: string) {
        this.$router.push({name: routeName});
    }

    get showSidebar() {
        return this.$$store.state.ui.showSidebar;
    }

    menuIconClick() {
      if (this.showSidebar) {
        this.$$store.dispatch(UIActions.toggleSidebar);
      }
    }

    sideIconClick() {
        this.$$store.dispatch(UIActions.toggleSidebar);
    }
}