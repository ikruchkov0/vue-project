import Content from './content/content.component';
import Header from './header/header.component';
import Footer from './footer/footer.component';

export { Content, Header, Footer };