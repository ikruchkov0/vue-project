import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './footer.template.html';

import './footer.less';

@WithRender
@Component({
    components: {
    },
    name: 'Footer',
})
export default class Footer extends SportportVue {
}