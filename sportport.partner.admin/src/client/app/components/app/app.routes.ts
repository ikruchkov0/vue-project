import Main from './main/main.component';
import MainRoutes from './main/routes';

let publicPath = process.env.ENV_PUBLIC_PATH !== undefined
    ? process.env.ENV_PUBLIC_PATH.toString().replace(/\/$/, '')
    : '/';

const redirect = 'studios';
const routes: Array<any> = [
     {
        path: publicPath,
        meta: {
            text: 'Главная',
        },
        component: Main,
        children: MainRoutes
    }
];

if (process.env.VUE_ENV !== 'server') {
    routes.push({ path: '', redirect: redirect });
    routes.push({ path: '*', redirect: redirect });
}

export default routes;
