import SportportVue from '@sp/client/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './app.template.html';
import '../../../styles.styl';

@WithRender
@Component({
  name: 'App'
})
export default class App extends SportportVue {
    get nava() {
        return true;
    }
}