import * as VueRouter from 'vue-router';
import { injectable } from 'inversify';

import AppComponent from './components/app/app.component';
import SportportStore from './store';
import SportportRouter from './router';
import SportportErrorHandler from './error-handler';

import { ApplicationActions } from './store/application/actions';
import { RootState } from './store';

@injectable()
export default class App {
    private component: AppComponent;

    constructor(private store: SportportStore, private router: SportportRouter, private errorHandler: SportportErrorHandler) {
        this.preloadState();
        this.component = new AppComponent({ router: this.router, store: this.store });
        this.errorHandler.register();
    }

    run(el: HTMLElement) {
        this.store.dispatch(new ApplicationActions.InitApplication())
            .then(() => {
                this.component.$mount(el);
            })
            .catch(error => {

            });
    }

    private preloadState() {
        let initialState;
        try {
            let initialStateStr = (<any>window).INITIAL_STATE;
            initialState = initialStateStr ? JSON.parse(initialStateStr) : null;
        }
        catch (ex) {
            initialState = null;
            console.log('Error while parsing initial state', ex);
        }

        if (initialState) {
            let initialRootState = new RootState(initialState);
            this.store.replaceState(initialRootState);
        }
    }
}