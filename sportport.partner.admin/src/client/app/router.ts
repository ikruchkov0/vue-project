import VueRouter from 'vue-router';
import appRoutes from './components/app/app.routes';

import { injectable } from 'inversify';

@injectable()
class SportportRouter extends VueRouter {
    constructor() {
        super({ routes: appRoutes, mode: 'history' });
    }
}

export default SportportRouter;

