import Vue from 'vue';
import SportportStore from '../app/store';
import Component from 'vue-class-component';

// Register the router hooks with thier names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate'
]);

class SportportVue extends Vue {
    get $$store(): SportportStore {
        return <SportportStore>this.$store;
    }
}


export default SportportVue;