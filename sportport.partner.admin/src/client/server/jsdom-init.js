const jsdom = require('jsdom');
const { JSDOM } = jsdom;

module.exports = function () {
    const dom = new JSDOM(`<!DOCTYPE html`, {
        url: 'http://localhost/',
    });
    return { window: dom.window, document: dom.window.document };
};