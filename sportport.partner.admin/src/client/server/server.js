const http = require('http');
const fs = require('fs');
const fetch = require('node-fetch');
const RequestHandler = require('./request-handler');
const renderer = require('vue-server-renderer').createRenderer();
const vm = require('vm');
const createDom = require('./jsdom-init');

let port = 50099;
let host = 'http://localhost';
let index = process.argv.findIndex(x => x === '--port' || x === '-p');
if (index > -1 && process.argv.length > (index + 1)) {
  port = Number(process.argv[index]);
  if (!port) {
    console.error('Server port should be specified use --port <port> or -p <port>');
    return;
  }
}

index = process.argv.findIndex(x => x === '--host' || x === '-h');
if (index > -1 && process.argv.length > (index + 1)) {
  host = process.argv[index];
  if (!port) {
    console.error('Server host should be specified use --host <host> or -h <host>');
    return;
  }
}

let createAppFactory;

http.createServer((request, response) => {
  request.addListener('end', function () {

    fs.readFile('./dist/client.ssr/ssr.main.js', 'utf-8', (err, content) => {

      const script = new vm.Script(content);
      createAppFactory = () => {
        let dom = createDom();
        const sandbox = {
          module: {},
          console: console,
          require: require,
          window: dom.window,
          document: dom.document,
          fetch: fetch,
          Request: fetch.Request,
          setTimeout: setTimeout
        };
        const context = new vm.createContext(sandbox);
        script.runInContext(context);
        return sandbox.module.exports.createApp;
      }
      let handler = new RequestHandler(createAppFactory, renderer, host, port);
      console.log(`Handle request ${request.url}`);
      handler.handleRequest(request, response);
    });
  }).resume();
}).listen(port);

console.log(`Listening on port ${port}`);