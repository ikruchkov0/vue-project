const fs = require('fs');
const path = require('path');

const staticFileExtentions = [
    '.js',
    '.css',
    '.map',
    '.ttf',
    '.woff'
];

const cache = {
    htmlContent: null,
    timestamp: 0
};

module.exports = class RequestHandler {
    constructor(createApp, renderer, host, port) {
        this.renderer = renderer;
        this.createApp = createApp;
        this.host = host;
        this.port = port;
    }

    handleRequest(request, response) {
        let staticPath = './dist';
        this.request = request;
        this.response = response;
        let handleAsStatic = staticFileExtentions.reduce((result, ext, index) => result || (request.url.indexOf(ext) >= 0), false);
        if (handleAsStatic) {
            let filePath = path.join(staticPath, request.url);
            fs.readFile(filePath, 'utf-8', (err, content) => this.staticFileLoadedCallback(err, content));
            return;
        }
        let indexFilePath = path.join(staticPath, '/client/index.html');
        if (cache.htmlContent) {
            return this.indexHtmlLoadCallback(null, cache.htmlContent);
        }
        fs.readFile(indexFilePath, 'utf-8', (err, content) => this.indexHtmlLoadCallback(err, content));
    }

    staticFileLoadedCallback(err, content) {
        if (err) {
            return this.errorHandler(err, 404, true);
        }
        this.response.writeHead(200);
        this.response.end(content);
    }

    createAppCallback(app) {
        console.log('app created');
        if (this.context.redirectUrl) {
            console.log('Redirect url', this.context.redirectUrl);
            let redirectUrl = `${this.host}${this.port && this.port != 80 ? ':' + this.port : ''}${this.context.redirectUrl}`;
            this.response.writeHead(302, {'Location': redirectUrl});
            this.response.end();
            return;
        }
        this.renderer.renderToString(app, (err, html) => this.renderCallback(err, html));
    }

    renderCallback(err, html) {
        console.log('app rendered');
        if (err) {
            return this.errorHandler(err);
        }

        let result = this.addAppHtml(html);
        result = this.enrichIndexHtml(result);
        RequestHandler.htmlCache = this.indexContent;
        this.response.writeHead(200);
        this.response.end(result);
    }


    indexHtmlLoadCallback(err, indexHtml) {
        console.log('index.html ready');
        if (err) {
            return this.errorHandler(err);
        }
        this.indexContent = indexHtml;
        this.context = { url: this.request.url }
        this.createApp()(this.context).then((app) => this.createAppCallback(app), (err) => this.errorHandler(err));
    }

    errorHandler(err, statusCode, skipLog) {
        statusCode = statusCode || (err ? err.code : 0) || 500;
        let message = !err || !err.message ? JSON.stringify(err) : err.message;
        if (!skipLog)  {
            console.error('Some error');
            console.error(err);
        }
        this.response.writeHead(statusCode);
        this.response.end('Internal Server Error\n' + message);
    }

    addAppHtml(appHtml) {
        return this.indexContent.replace('<!-- SSR CONTENT PLACEHOLDER -->', appHtml);
    }

    enrichIndexHtml(indexHtml) {
        let result = indexHtml;
        if (this.context.state) {
            let stateStr = `<script>window.INITIAL_STATE = '${JSON.stringify(this.context.state)}';</script>`;
            result = result.replace('<!-- INITIAL STATE PLACEHOLDER -->', stateStr);
        }
        if (this.context.title) {
            let titleStr = `<title>${this.context.title}</title>`;
            result = result.replace(/<title>(.*)<\/title>/i, titleStr);
        }
        if (this.context.description) {
            let descriptionStr = `<meta name="description" content="${this.context.description}">`;
            result = result.replace('<!-- DESCRIPTION PLACEHOLDER -->', descriptionStr);
        }
        return result;
    }

}