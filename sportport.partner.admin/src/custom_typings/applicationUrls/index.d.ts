declare module 'applicationUrls' {
  interface ApplicationUrls {
    Development: UrlsDict;
    Production: UrlsDict;    
  }

  interface UrlsDict {
    Client: Client;
  }

  interface Client {
    Identity: string;
    PartnerFrontend: string;
    PartnerFrontendReturnUrl: string;
    PartnerFrontendSilentRedirectUrl: string;
    CompaniesApi: string;
    PartnerApi: string;
  }

  const applicationUrls: ApplicationUrls
  export = applicationUrls
}