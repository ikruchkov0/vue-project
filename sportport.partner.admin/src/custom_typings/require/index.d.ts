interface Require {
    ensure(deps: Array<string>, callback: (require: Require) => void);
    (dep: string): any;
}


declare let require: Require;