interface Env {
    NODE_ENV: string;
    ENV_PUBLIC_PATH: string;
    VUE_ENV: string;
}

interface Process {
    env : Env;
}


declare let process: Process;