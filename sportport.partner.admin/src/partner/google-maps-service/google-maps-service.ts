interface GoogleMapsService {
    load(key: any): Promise<any>;
}

export default GoogleMapsService;