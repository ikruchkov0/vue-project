import GoogleMapsService from './google-maps-service';
import GoogleMapsServiceImpl from './google-maps-service-impl';

let instance: GoogleMapsService = null;

export function getGoogleMapsService(): GoogleMapsService {
    if (!instance) {
        instance = new GoogleMapsServiceImpl();
    }
    return instance;
}