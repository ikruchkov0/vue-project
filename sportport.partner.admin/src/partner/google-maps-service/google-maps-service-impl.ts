import * as GoogleMapsLoader from 'google-maps';

import GoogleMapsService from './google-maps-service';

class GoogleMapsServiceImpl implements GoogleMapsService {
      load(key: any): Promise<any> {
            GoogleMapsLoader.KEY = key;
            GoogleMapsLoader.VERSION = '3.26';
            GoogleMapsLoader.LANGUAGE = 'ru';
            GoogleMapsLoader.REGION = 'RU';
            GoogleMapsLoader.LIBRARIES = ['geometry', 'places', 'drawing'];
            return new Promise((resolve, reject) => {
                  GoogleMapsLoader.load(function (google) {
                        resolve(google);
                  });
            });
      }
}

export default GoogleMapsServiceImpl;