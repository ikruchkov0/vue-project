// import '@sp/shared/polyfill';
import 'reflect-metadata';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { SportportContainer } from '@sp/partner/ioc';

Vue.use(VueRouter);
Vue.use(Vuex);

console.log(document.readyState);
document.readyState !== 'complete'
    ? document.addEventListener('DOMContentLoaded', onDocumentRady)
    : onDocumentRady();

function onDocumentRady() {
    SportportContainer.init();
    let app = SportportContainer.createAppInstance();
    let appElement = document.getElementById('app');
    console.log(appElement);
    app.run(appElement);
}