import * as Vuex from 'vuex';
import State from './state';
import { ClassCategoryVM } from './view-models';

class SetClassCategoriesMutation {
    constructor(private _categories: Array<ClassCategoryVM>) { }
    static get type() { return 'setClassCategories'; }
    get type() { return SetClassCategoriesMutation.type; }
    get categories() { return this._categories; }
}

const mutations = {
    [SetClassCategoriesMutation.type](state: State, payload: SetClassCategoriesMutation) {
        state.classCategories = payload.categories;
    },
};

export { mutations as ClassesMutations,
    SetClassCategoriesMutation
     };