import { ClassCategoryVM } from './view-models';

export default class StudiosState {
  classCategories: Array<ClassCategoryVM> = [];
};
