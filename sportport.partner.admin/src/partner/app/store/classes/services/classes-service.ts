import { ClassCategoryVM } from '@sp/partner/view-models';
import { BaseDataService } from '../../base-data-service';

export default class ClassService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    getClassCategories(): Promise<Array<ClassCategoryVM>> {
      return this.get(`api/v1/Classes/ClassCategories`)
          .then((jsonArray) => jsonArray.map(cc => new ClassCategoryVM(cc)));
    }

}