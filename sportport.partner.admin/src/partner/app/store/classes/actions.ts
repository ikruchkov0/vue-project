import * as Vuex from 'vuex';
import RootState from '../root-state';

import { ClassCategoryVM } from '@sp/partner/view-models';
import ClassesService from './services/classes-service';
import State from './state';
import * as Mutations from './mutations';

class LoadClassCategoriesAction {
    constructor() { }

    static get type() { return 'loadClassCategories'; }
    get type() { return LoadClassCategoriesAction.type; }
}

const actions = {

    [LoadClassCategoriesAction.type](context: Vuex.ActionContext<State, RootState>, payload: LoadClassCategoriesAction) {
        let service = new ClassesService();
        return service.getClassCategories().then((cats) => {
            let mutation = new Mutations.SetClassCategoriesMutation(cats);
            context.commit(mutation);
        });
    },

};

const ClassActions = {
    LoadClassCategoriesAction
};

export { ClassActions };

export default actions;