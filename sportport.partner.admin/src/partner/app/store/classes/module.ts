import * as Vuex from 'vuex';
import RootState from '../root-state';
import ClassesState from './state';
import { ClassesMutations } from './mutations';
import ClassesActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new ClassesState(),
    mutations: ClassesMutations,
    actions: ClassesActions
};

export default Module;