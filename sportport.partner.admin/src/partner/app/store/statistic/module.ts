import * as Vuex from 'vuex';
import RootState from '../root-state';
import StatisticState from './state';
import { StatisticMutations } from './mutations';
import StatisticActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new StatisticState(),
    mutations: StatisticMutations,
    actions: StatisticActions
};

export default Module;