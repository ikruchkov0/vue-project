import * as Vuex from 'vuex';
import RootState from '../root-state';
import StatisticState from './state';
import StatisticService from './services/statistic-service';

import * as Mutations from './mutations';

class LoadStatisticPerMonthAction {
    static get type() { return 'loadStatisticPerMonth'; }
    get type() { return LoadStatisticPerMonthAction.type; }
}

const actions = {
    [LoadStatisticPerMonthAction.type](context: Vuex.ActionContext<StatisticState, RootState>, payload: LoadStatisticPerMonthAction) {
        let service = new StatisticService();
        return service.getStatistic(context.rootState.company.currentCompany.id, 'Month', true).then((statistic) => {
            let mutation = new Mutations.SetStatisticDataMutation(statistic);
            context.commit(mutation);
        });
    }
};

export { LoadStatisticPerMonthAction };

export default actions;