import { BaseVM } from '@sp/shared/base-vm';

class DatedValue<TValue> {
    Date: Date;
    Value: TValue;
}

export class PartnerStatisticVM extends BaseVM<PartnerStatisticVM> {
    address: string;
    title: string;
    revenue: number;
    visited: number;
    canceled: number;
    pending: number;
    visitedDetails: Array<DatedValue<number>>;
    revenueDetails: Array<DatedValue<number>>;
    affiliates: Array<PartnerStatisticVM>;
    classes: Array<PartnerStatisticVM>;
}