import { PartnerStatisticVM } from './view-models';

export default class StatisticState {
    data: PartnerStatisticVM;
};