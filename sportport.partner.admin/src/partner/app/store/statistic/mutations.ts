import * as Vuex from 'vuex';
import StatisticState from './state';
import { PartnerStatisticVM } from './view-models';

class SetStatisticDataMutation {
    constructor(private _statistic: PartnerStatisticVM) { }
    static get type() { return 'setStatisticData'; }
    get type() { return SetStatisticDataMutation.type; }
    get statistic() { return this._statistic; }
}

const StatisticMutations = {
    [SetStatisticDataMutation.type](state: StatisticState, payload: SetStatisticDataMutation) {
        state.data = payload.statistic;
    }
};

export { StatisticMutations as StatisticMutations, SetStatisticDataMutation };