import { PartnerStatisticVM } from '../view-models';
import { BaseDataService } from '../../base-data-service';

type Period = 'Day' | 'Week' | 'Month' | 'Halfyear' | 'Year';

export default class StatisticService extends BaseDataService {

    constructor() {
        super('CompaniesApi');
    }

    getStatistic(companyId: number, period: Period, withStudios: boolean): Promise<PartnerStatisticVM> {
        return this.get(`api/v1/Companies/${companyId}/Statistic/${period}?withStudios=${withStudios}`).then((json) => new PartnerStatisticVM(json));
    }
};