import * as VueRouter from 'vue-router';
import * as applicationUrls from 'applicationUrls';

import { SportportContainer } from '@sp/partner/ioc';
import { UIActions } from './ui/actions';

import { BaseDataService as SharedBaseDataService }  from '@sp/shared/base-data-service';
import config from '@sp/shared/config';

abstract class BaseDataService extends SharedBaseDataService {
    get store() {
        return SportportContainer.getStore();
    }

    constructor(apiRoot: string) {
        super(apiRoot, () => this.getAuthToken(), () => this.showSpinner(), () => this.hideSpinner());
    }

    private getAuthToken() {
        let user = this.store.state.profile.user;
        if (!user) {
            return;
        }

        return `${user.auth.tokenType} ${user.auth.accessToken}`;
    }

    private showSpinner() {
        SportportContainer.getStore().dispatch(UIActions.showSpinner);
    }

    private hideSpinner() {
        SportportContainer.getStore().dispatch(UIActions.hideSpinner);
    }
}

export { BaseDataService };