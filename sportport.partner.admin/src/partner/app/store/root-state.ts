import { UIState } from './ui';
import { ProfileState } from './profile';
import { CompanyState } from './company';
import { StudiosState } from './studios';
import { StatisticState } from './statistic';
import { SchedulesState } from './schedule';
import { ClassesState } from './classes';
import { ApplicationState } from './application';

export default class RootState {
    ui: UIState;
    profile: ProfileState;
    company: CompanyState;
    studios: StudiosState;
    statistic: StatisticState;
    schedule: SchedulesState;
    classes: ClassesState;
    application: ApplicationState;
};