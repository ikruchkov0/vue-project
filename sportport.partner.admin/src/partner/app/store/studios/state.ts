import { StudioVM, StudioFacilityVM } from './view-models';

export default class StudiosState {
    companyStudios: Array<StudioVM> = [];
    facilitiesDict: Array<StudioFacilityVM> = [];
};
