import { StudioVM, ClassCategoryVM, StudioFacilityVM } from '@sp/partner/view-models';
import { BaseDataService } from '../../base-data-service';

export default class StudioService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    getCompanyStudios(companyId: number): Promise<Array<StudioVM>> {
        return this.get(`api/v1/Companies/${companyId}/Studios`)
            .then((jsonArray) => StudioVM.fromJsonArray(jsonArray));
    }

    getStudioDetails(studioId: number): Promise<StudioVM> {
        return this.get(`api/v1/Studios/${studioId}`)
            .then((json) => {
                let studio = new StudioVM(json);
                return studio;
            });
    }

    addStudio(companyId: number, studio: StudioVM): Promise<Array<StudioVM>> {
        return this.post(`api/v1/Companies/${companyId}/Studios`, studio)
            .then((json) => {
                return this.getCompanyStudios(companyId);
            });
    }

    updateStudio(studioId: number, studio: StudioVM): Promise<StudioVM> {
        studio.classCategories = null;
        return this.put(`api/v1/Studios/${studioId}`, studio)
            .then((json) => {
                return this.getStudioDetails(json.id);
            });
    }

    deleteStudio(studioId: number): Promise<number> {
        return this.delete(`api/v1/Studios/${studioId}`)
            .then((id) => {
                return id;
            });
    }

    updateStudioClassCategories(studioId: number, categories: Array<ClassCategoryVM>): Promise<StudioVM> {
        return this.post(`api/v1/Studios/${studioId}/ClassCategories`, categories)
            .then((jsonArray) => {
                return this.getStudioDetails(studioId);
            });
    }

    loadStudioFacilitiesDict(): Promise<Array<StudioFacilityVM>> {
        return this.get(`api/v1/Studios/Facilities`)
          .then((jsonArray) => jsonArray.map(cc => new StudioFacilityVM(cc)));
    }
}