import * as Vuex from 'vuex';
import StudiosState from './state';
import { StudioVM, StudioFacilityVM } from './view-models';

class SetStudiosMutation {
    constructor(private _studios: Array<StudioVM>) { }
    static get type() { return 'setCurrentStudios'; }
    get type() { return SetStudiosMutation.type; }
    get studios() { return this._studios; }
}

class SetStudioDetailsMutation {
    constructor(private _studio: StudioVM) { }
    static get type() { return 'setStudioDetails'; }
    get type() { return SetStudioDetailsMutation.type; }
    get studio() { return this._studio; }
}

class DeleteStudioMutation {
    constructor(private _studioId: number) { }
    static get type() { return 'deleteStudio'; }
    get type() { return DeleteStudioMutation.type; }
    get studioId() { return this._studioId; }
}

class SetStudioFacilitiesDictMutation {
    constructor(private _facilitiesDict: Array<StudioFacilityVM>) { }
    static get type() { return 'setStudioFacilitiesDict'; }
    get type() { return SetStudioFacilitiesDictMutation.type; }
    get facilitiesDict() { return this._facilitiesDict; }
}

const mutations = {
    [SetStudiosMutation.type](state: StudiosState, payload: SetStudiosMutation) {
        state.companyStudios = payload.studios;
    },

    [SetStudioDetailsMutation.type](state: StudiosState, payload: SetStudioDetailsMutation) {
        let arrayIndex = state.companyStudios.findIndex(s => s.id === payload.studio.id);
        state.companyStudios.splice(arrayIndex, 1, payload.studio);
    },

    [DeleteStudioMutation.type](state: StudiosState, payload: DeleteStudioMutation) {
        state.companyStudios = state.companyStudios.filter(s => s.id !== payload.studioId);
    },

    [SetStudioFacilitiesDictMutation.type](state: StudiosState, payload: SetStudioFacilitiesDictMutation) {
        state.facilitiesDict = payload.facilitiesDict;
    },
};

export { mutations as StudiosMutations,
    SetStudiosMutation,
    SetStudioDetailsMutation,
    DeleteStudioMutation,
    SetStudioFacilitiesDictMutation
};