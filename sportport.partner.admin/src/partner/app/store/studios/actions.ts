import * as Vuex from 'vuex';
import RootState from '../root-state';

import { StudioVM, ClassCategoryVM } from '@sp/partner/view-models';
import StudioService from './services/studio-service';
import StudiosState from './state';
import * as Mutations from './mutations';

class LoadStudiosAction {
    static get type() { return 'loadStudios'; }
    get type() { return LoadStudiosAction.type; }
}

class LoadStudioDetailsAction {
    constructor(private _studioId: number) { }

    static get type() { return 'loadStudioDetails'; }
    get type() { return LoadStudioDetailsAction.type; }
    get studioId() { return this._studioId; }
}

class AddStudioAction {
    constructor(private _studio: StudioVM) { }
    static get type() { return 'addStudio'; }
    get type() { return AddStudioAction.type; }
    get studio() { return this._studio; }
}

class UpdateStudioAction {
    constructor(private _studioId: number, private _studio: StudioVM) { }

    static get type() { return 'updateStudio'; }
    get type() { return UpdateStudioAction.type; }
    get studioId() { return this._studioId; }
    get studio() { return this._studio; }
}

class DeleteStudioAction {
    constructor(private _studioId: number) { }

    static get type() { return 'deleteStudio'; }
    get type() { return DeleteStudioAction.type; }
    get studioId() { return this._studioId; }
}

class UpdateStudioClassCategoriesAction {
    constructor(private _studioId: number, private _classCategories: Array<ClassCategoryVM>) { }

    static get type() { return 'updateStudioClassCategories'; }
    get type() { return UpdateStudioClassCategoriesAction.type; }
    get studioId() { return this._studioId; }
    get classCategories() { return this._classCategories; }
}

class LoadStudioFacilitiesDictAction {
    constructor() { }

    static get type() { return 'loadStudioFacilitiesDict'; }
    get type() { return LoadStudioFacilitiesDictAction.type; }
}

const actions = {
    [LoadStudiosAction.type](context: Vuex.ActionContext<StudiosState, RootState>) {
        let service = new StudioService();
        return service.getCompanyStudios(context.rootState.company.currentCompany.id).then((studios) => {
            let mutation = new Mutations.SetStudiosMutation(studios);
            context.commit(mutation);
        });
    },

    [LoadStudioDetailsAction.type](context: Vuex.ActionContext<StudiosState, RootState>, payload: LoadStudioDetailsAction) {
        let service = new StudioService();
        return service.getStudioDetails(payload.studioId).then((studio) => {
            let mutation = new Mutations.SetStudioDetailsMutation(studio);
            context.commit(mutation);
        });
    },

    [UpdateStudioAction.type](context: Vuex.ActionContext<StudiosState, RootState>, payload: UpdateStudioAction) {
        let service = new StudioService();
        return service.updateStudio(payload.studioId, payload.studio).then((studio) => {
            let mutation = new Mutations.SetStudioDetailsMutation(studio);
            context.commit(mutation);
        });
    },

    [AddStudioAction.type](context: Vuex.ActionContext<StudiosState, RootState>, payload: AddStudioAction) {
        let service = new StudioService();
        return service.addStudio(context.rootState.company.currentCompany.id, payload.studio).then((studios) => {
            let mutation = new Mutations.SetStudiosMutation(studios);
            context.commit(mutation);
        });
    },

    [DeleteStudioAction.type](context: Vuex.ActionContext<StudiosState, RootState>, payload: DeleteStudioAction) {
        let service = new StudioService();
        return service.deleteStudio(payload.studioId).then((studioId) => {
            let mutation = new Mutations.DeleteStudioMutation(studioId);
            context.commit(mutation);
        });
    },

    [UpdateStudioClassCategoriesAction.type](context: Vuex.ActionContext<StudiosState, RootState>, payload: UpdateStudioClassCategoriesAction) {
        let service = new StudioService();
        return service.updateStudioClassCategories(payload.studioId, payload.classCategories).then((studio) => {
            let mutation = new Mutations.SetStudioDetailsMutation(studio);
            context.commit(mutation);
        });
    },

    [LoadStudioFacilitiesDictAction.type](context: Vuex.ActionContext<StudiosState, RootState>, payload: LoadStudioFacilitiesDictAction) {
        let service = new StudioService();
        return service.loadStudioFacilitiesDict().then((facilitiesDict) => {
            let mutation = new Mutations.SetStudioFacilitiesDictMutation(facilitiesDict);
            context.commit(mutation);
        });
    },
};

const StudioActions = {
    LoadStudiosAction,
    LoadStudioDetailsAction,
    AddStudioAction,
    UpdateStudioAction,
    DeleteStudioAction,
    UpdateStudioClassCategoriesAction,
    LoadStudioFacilitiesDictAction
};

export { StudioActions };

export default actions;