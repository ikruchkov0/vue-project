import { AuthenticatedUser } from '@sp/shared/sportport-auth';

class AuthData {
    id: number;
    accessToken: string;
    idToken: string;
    tokenType: string;
    expired: boolean;
}
class Profile {
    name: string;
    role: string = 'Partner';
    avatarUrl: string = '/img/user2-160x160.jpg';
}

export default class UserVM {
    profile: Profile;
    auth: AuthData;

    constructor(user: AuthenticatedUser) {
        this.auth = {
            id: Number(user.userId),
            accessToken: user.accessToken,
            idToken: user.idToken,
            tokenType: user.tokenType,
            expired: user.expired
        };
        this.profile = {
            name: user.userName,
            role: 'Partner',
            avatarUrl: '/img/user2-160x160.jpg'
        };
    }
};