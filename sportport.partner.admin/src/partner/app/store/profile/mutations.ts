import * as Vuex from 'vuex';
import ProfileState from './state';
import UserVM from './view-models/user-vm';

class SetUserMutation {
    static get type() { return 'setUser'; }

    get type() { return SetUserMutation.type; }

    get user() { return this._user; }

    constructor(private _user: UserVM) {
    }
};


const ProfileMutations = {
    [SetUserMutation.type](state: ProfileState, payload: SetUserMutation) {
        state.user = payload.user;
    }
};

export { ProfileMutations as ProfileMutations, SetUserMutation };