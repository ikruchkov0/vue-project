import config from '@sp/shared/config';
import { AuthConfig } from '@sp/shared/sportport-auth';

const authConfig: AuthConfig = {
    authority: config.Client.Identity,

    clientId: 'PartnerPortal',
    redirectUri: config.Client.PartnerFrontendReturnUrl,
    postLogoutRedirectUri: config.Client.PartnerFrontend,
    silentRedirectUri: config.Client.PartnerFrontendSilentRedirectUrl,
    // Scopes requested during the authorisation request
    scope: 'partnergw openid profile'
};

export default authConfig;
