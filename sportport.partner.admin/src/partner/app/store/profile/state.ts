import User from './view-models/user-vm';

export default class ProfileState {
    user: User = null;
};