import * as Vuex from 'vuex';
import RootState from '../root-state';
import ProfileState from './state';
import { getAuthService } from '@sp/shared/sportport-auth';
import { UserVM } from '@sp/partner/view-models';

import { SetUserMutation } from './mutations';

import { SportportContainer } from '@sp/partner/ioc';
import AuthConfig from './auth-config';

class LoginAction {
    static get type() { return 'login'; }

    get type() { return LoginAction.type; }
}

class ProcessCallbackAction {
    static get type() { return 'processCallback'; }
    get type() { return ProcessCallbackAction.type; }
    get redirectAction() { return this._redirectAction; }
    constructor(private _redirectAction: () => void) { }
}

class SilentLoginAction {
    static get type() { return 'silentLogin'; }
    get type() { return SilentLoginAction.type; }
}

class ClearUserAction {
    static get type() { return 'clearUser'; }
    get type() { return ClearUserAction.type; }
}

class ProcessSilentCallbackAction {
    static get type() { return 'processSilentCallback'; }
    get type() { return ProcessSilentCallbackAction.type; }
    get redirectAction() { return this._redirectAction; }
    constructor(private _redirectAction: () => void) { }
}

class LogoutAction {
    static get type() { return 'logout'; }
    get type() { return LogoutAction.type; }
}

class InitUserAction {
    constructor(private _userLoadedCallback: (...ev: any[]) => void, private _signOutCallback: (...ev: any[]) => void) { }
    static get type() { return 'initUser'; }
    get type() { return InitUserAction.type; }
    get signOutCallback() { return this._signOutCallback; }
    get userLoadedCallback() { return this._userLoadedCallback; }
}

const actions = {
    [LoginAction.type](context: Vuex.ActionContext<ProfileState, RootState>) {
        return getAuthService(AuthConfig).then((authService) => authService.login());
    },

    [SilentLoginAction.type](context: Vuex.ActionContext<ProfileState, RootState>) {
        return getAuthService(AuthConfig).then((authService) => authService.silentLogin());
    },

    [LogoutAction.type](context: Vuex.ActionContext<ProfileState, RootState>) {
        return getAuthService(AuthConfig).then((authService) => authService.logout());
    },

    [ClearUserAction.type](context: Vuex.ActionContext<ProfileState, RootState>) {
        return getAuthService(AuthConfig).then((authService) => authService.removeUser());
    },

    [InitUserAction.type](context: Vuex.ActionContext<ProfileState, RootState>, payload: InitUserAction) {
        return getAuthService(AuthConfig).then((authService) => {

            authService.registerEventHandler('UserLoaded', payload.userLoadedCallback);
            authService.registerEventHandler('UserSignedOut', payload.signOutCallback);

            return authService.getUser().then((result) => {
                if (!result) {
                    return;
                }

                let user = new UserVM(result);
                if (user.auth.expired) {
                    return;
                }
                context.commit(new SetUserMutation(user));
                return user;
            });
        });
    },

    [ProcessCallbackAction.type](context: Vuex.ActionContext<ProfileState, RootState>, payload: ProcessCallbackAction) {
        return getAuthService(AuthConfig).then((authService) => {
            authService.processCallback().then((result) => {
                if (!result) {
                    return;
                }
                let user = new UserVM(result);
                context.commit(new SetUserMutation(user));
                payload.redirectAction();
                return user;
            });
        });
    },

    [ProcessSilentCallbackAction.type](context: Vuex.ActionContext<ProfileState, RootState>, payload: ProcessSilentCallbackAction) {
        return getAuthService(AuthConfig).then((authService) => {
            authService.processSilentLoginCallback().then((result) => {
                if (!result) {
                    return;
                }
                let user = new UserVM(result);
                context.commit(new SetUserMutation(user));
                payload.redirectAction();
                return user;
            });
        });
    }
};

const ProfileActions = { LoginAction, LogoutAction, InitUserAction, ProcessCallbackAction, SilentLoginAction, ProcessSilentCallbackAction, ClearUserAction };

export { ProfileActions };

export default actions;