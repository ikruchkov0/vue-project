import * as Vuex from 'vuex';
import ProfileState from './state';
import RootState from '../root-state';
import { ProfileMutations } from './mutations';
import ProfileActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new ProfileState(),
    mutations: ProfileMutations,
    actions: ProfileActions
};

export default Module;