import { ClassVM } from './view-models';

interface StudioClassesMap {
    [studioId: number]: Array<ClassVM>;
}

export default class SchedulesState {
    studios: StudioClassesMap = {};
};