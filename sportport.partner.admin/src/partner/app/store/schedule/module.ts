import * as Vuex from 'vuex';
import RootState from '../root-state';
import SchedulesState from './state';
import { SchedulesMutations } from './mutations';
import SchedulesActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new SchedulesState(),
    mutations: SchedulesMutations,
    actions: SchedulesActions
};

export default Module;