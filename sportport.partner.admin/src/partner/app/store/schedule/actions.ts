import * as Vuex from 'vuex';
import ScheduleService from './services/schedule-service';
import StudioService from '../studios/services/studio-service';
import ClassesService from './services/classes-service';
import RootState from '../root-state';
import DashboardState from './state';
import { ScheduleVM, DayOfWeek, ClassVM } from './view-models';

import * as Mutations from './mutations';

class SaveScheduleAction {
    constructor(private _studioId: number, private _classId: number, private _dayOfWeek: DayOfWeek, private _schedule: Array<ScheduleVM>) { }

    static get type() { return 'saveSchedule'; }
    get type() { return SaveScheduleAction.type; }
    get classId() { return this._classId; }
    get studioId() { return this._studioId; }
    get dayOfWeek() { return this._dayOfWeek; }
    get schedule() { return this._schedule; }
}

class AddClassAction {
    constructor(private _studioId: number, private _classItem: ClassVM) { }

    static get type() { return 'addClass'; }
    get type() { return AddClassAction.type; }
    get classItem() { return this._classItem; }
    get studioId() { return this._studioId; }
}

class SaveClassAction {
    constructor(private _studioId: number, private _classItem: ClassVM) { }

    static get type() { return 'saveClass'; }
    get type() { return SaveClassAction.type; }
    get classItem() { return this._classItem; }
    get studioId() { return this._studioId; }
}

class DeleteClassAction {
    constructor(private _studioId: number, private _classItem: ClassVM) { }

    static get type() { return 'deleteClass'; }
    get type() { return DeleteClassAction.type; }
    get classItem() { return this._classItem; }
    get studioId() { return this._studioId; }
}

class LoadClassesForStudioAction {
    constructor(private _studioId: number) { }

    static get type() { return 'loadClassesForStudios'; }
    get type() { return LoadClassesForStudioAction.type; }
    get studioId() { return this._studioId; }
}

const actions = {
    [SaveScheduleAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: SaveScheduleAction) {
        let service = new ScheduleService();
        return service.saveSchedule(payload.classId, DayOfWeek[payload.dayOfWeek], payload.schedule).then(() => {
            return service.getSchedule(payload.classId, DayOfWeek[payload.dayOfWeek]).then((schedule) => {
                let mutation = new Mutations.SetScheduleMutation(payload.studioId, payload.classId, payload.dayOfWeek, schedule);
                context.commit(mutation);
            });
        });
    },
    [LoadClassesForStudioAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: LoadClassesForStudioAction) {
        let service = new ClassesService();
        return service.getClasses(payload.studioId).then((classes) => {
            let mutation = new Mutations.SetClassesForStudioMutation(payload.studioId, classes);
            context.commit(mutation);
        });
    },
    [AddClassAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: AddClassAction) {
        let service = new ClassesService();
        return service.addClass(payload.studioId, payload.classItem).then((classItem) => {
            return context.dispatch(new LoadClassesForStudioAction(payload.studioId));
        });
    },
    [SaveClassAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: AddClassAction) {
        let service = new ClassesService();
        return service.saveClass(payload.studioId, payload.classItem).then((classItem) => {
            return context.dispatch(new LoadClassesForStudioAction(payload.studioId));
        });
    },
    [DeleteClassAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: AddClassAction) {
        let service = new ClassesService();
        return service.deleteClass(payload.studioId, payload.classItem.id).then((classItem) => {
            return context.dispatch(new LoadClassesForStudioAction(payload.studioId));
        });
    }
};

const ScheduleActions = {
    SaveScheduleAction,
    LoadClassesForStudioAction,
    DeleteClassAction,
    AddClassAction,
    SaveClassAction
};

export { ScheduleActions as ScheduleActions };

export default actions;