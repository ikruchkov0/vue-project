import * as moment from 'moment';
import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export default function IsTime(property: string, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: 'isTime',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [relatedPropertyName] = args.constraints;
                    const relatedValue = (args.object as any)[relatedPropertyName];
                    let timeValue = moment(value, 'HH:mm', true);
                    return timeValue.isValid();
                }
            }
        });
    };
}