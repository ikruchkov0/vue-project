enum ScheduleStatus {
    Added,
    Edited,
    Disabled,
    Deleted
};

export default ScheduleStatus;