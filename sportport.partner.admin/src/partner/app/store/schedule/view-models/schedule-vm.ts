import { validate, IsNotEmpty, IsEmail, Min, MaxLength, IsInt, IsPositive } from 'class-validator';
import * as moment from 'moment';
import { BaseVM } from '@sp/shared/base-vm';

import DayOfWeek from './day-of-week';
import ScheduleStatus from './schedule-status';
import IsTime from './time-validation';

export default class ScheduleVM extends BaseVM<ScheduleVM> {
    id: number;
    status: ScheduleStatus;
    dayOfWeek: DayOfWeek;

    @IsTime('time', {
        message: 'Значение ($value) должно соотвестветствовать формату ЧЧ:ММ'
    })
    @IsNotEmpty({
        message: 'Введите время начала занятия'
    })
    startTime: string;

    @IsTime('time', {
        message: 'Значение ($value) должно соотвестветствовать формату ЧЧ:ММ'
    })
    @IsNotEmpty({
        message: 'Введите время окончания занятия'
    })
    endTime: string;

    @IsNotEmpty({
        message: 'Введите коллличество свободных мест'
    })
    @Min(1, {
        message: 'Колличество занятий должн быть больше нуля'
    })
    qauantity?: number;

    enabled: boolean;
    classId: number;
    studioId: number;
    classCategoryId: number;
    classroomId: number;

    protected updateFromJson(json: any) {
        super.updateFromJson(json);
        this.startTime = moment(json.startTime, 'HH:mm:ss').format('HH:mm');
        this.endTime = moment(json.endTime, 'HH:mm:ss').format('HH:mm');
    }
};