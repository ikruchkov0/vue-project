import { validate, IsDefined, IsNotEmpty, MinLength, MaxLength, IsPositive } from 'class-validator';
import { BaseVM } from '@sp/shared/base-vm';
import ScheduleVM from './schedule-vm';

export default class ClassVM extends BaseVM<ClassVM> {

    id: number;

    @IsDefined({ message: 'Класс не привязан к студии' })
    @IsPositive({ message: 'Идентификатор студии задан не верно' })
    studioId: number;

    @IsDefined({ message: 'Класс не привязан к категории' })
    classCategoryId: number;

    @IsDefined({ message: 'Введите название класса' })
    @IsNotEmpty({ message: 'Введите название класса' })
    @MinLength(3, { message: 'Название должно быть не короче $constraint1 символов' })
    @MaxLength(60, { message: 'Название должно быть не длиннее $constraint1 символов' })
    title: string;

    @IsDefined({ message: 'Активность не определна' })
    enabled: Boolean;

    schedules: Array<ScheduleVM>;

    updateFromJson(json: any) {
        super.updateFromJson(json);
        this.schedules = json.schedules.map(x => new ScheduleVM(x));
    }
};