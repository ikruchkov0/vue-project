import { ClassVM } from '../view-models';
import { BaseDataService }  from '../../base-data-service';

export default class ClassesService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    getClasses(studioId: number, withSchedules: boolean = true): Promise<Array<ClassVM>> {
        return this.get(`api/v1/Studios/${studioId}/Classes?withSchedules=${withSchedules}`).then((classes) => classes.map(json => new ClassVM(json)));
    }

    addClass(studioId: number, classItem: ClassVM): Promise<ClassVM> {
        return this.post(`api/v1/Studios/${studioId}/Classes`, classItem).then((json) =>  new ClassVM(json));
    }

    saveClass(studioId: number, classItem: ClassVM): Promise<ClassVM> {
        return this.put(`api/v1/Studios/${studioId}/Classes`, classItem).then((json) =>  new ClassVM(json));
    }

    deleteClass(studioId: number, classId: number): Promise<any> {
        return this.delete(`api/v1/Studios/${studioId}/Classes/${classId}`);
    }
}