import { ScheduleVM, DayOfWeek } from '../view-models';
import { BaseDataService }  from '../../base-data-service';

export default class ScheduleService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    getSchedule(classId: number, dayOfWeek: string): Promise<Array<ScheduleVM>> {
        return this.get(`api/v1/Classes/${classId}/Schedule/${dayOfWeek}`).then((schedule) => schedule.map(json => new ScheduleVM(json)));
    }

    saveSchedule(classId: number, dayOfWeek: string, schedule: Array<ScheduleVM>): Promise<Array<ScheduleVM>> {
        return this.post(`api/v1/Classes/${classId}/Schedule/${dayOfWeek}`, schedule).then(schedule => {
            return schedule.map(json => new ScheduleVM(json));
        });
    }
}