import * as Vuex from 'vuex';
import Vue from 'vue';
import ScheduleState from './state';
import { ScheduleVM, DayOfWeek } from './view-models';
import { StudioVM, ClassVM } from '@sp/partner/view-models';

class SetScheduleMutation {
    constructor(private _studioId: number, private _classId: number, private _day: DayOfWeek, private _schedule: Array<ScheduleVM>, ) { }
    static get type() { return 'setSchedule'; }
    get type() { return SetScheduleMutation.type; }
    get schedule() { return this._schedule; }
    get classId() { return this._classId; }
    get studioId() { return this._studioId; }
    get day() { return this._day; }
}

class SetClassesForStudioMutation {
    constructor(private _studioid: number, private _classes: Array<ClassVM>) { }
    static get type() { return 'setClassesForStudio'; }
    get type() { return SetClassesForStudioMutation.type; }
    get studioId() { return this._studioid; }
    get classes() { return this._classes; }
}

class SetClassMutation {
    constructor(private _studioid: number, private _classItem: ClassVM) { }
    static get type() { return 'setClass'; }
    get type() { return SetClassMutation.type; }
    get studioId() { return this._studioid; }
    get classItem() { return this._classItem; }
}

const mutations = {
    [SetScheduleMutation.type](state: ScheduleState, payload: SetScheduleMutation) {
        let studio = state.studios[payload.studioId];
        if (!studio) {
            return;
        }
        let classItem = studio.find(x => x.id === payload.classId);
        if (!classItem) {
            return;
        }
        classItem.schedules = classItem.schedules.filter(x => x.dayOfWeek !== payload.day).concat(payload.schedule);
    },
    [SetClassesForStudioMutation.type](state: ScheduleState, payload: SetClassesForStudioMutation) {
        let studios = { ...state.studios };
        studios[payload.studioId] = payload.classes;
        state.studios = studios;
    },
    [SetClassMutation.type](state: ScheduleState, payload: SetClassMutation) {
        let studios = { ...state.studios };
        studios[payload.studioId] = studios[payload.studioId].filter(x => x.id !== payload.classItem.id).concat([payload.classItem]);
        state.studios = studios;
    }
};

export { mutations as SchedulesMutations, SetScheduleMutation, SetClassesForStudioMutation, SetClassMutation };