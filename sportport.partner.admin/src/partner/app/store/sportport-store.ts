import * as Vuex from 'vuex';
import { decorate, injectable } from 'inversify';

import RootState from './root-state';
import { UIModule } from './ui';
import { ProfileModule } from './profile';
import { CompanyModule } from './company';
import { StudiosModule } from './studios';
import { StatisticModule } from './statistic';
import { SchedulesModule } from './schedule';
import { ClassesModule } from './classes';
import { ApplicationModule } from './application';

decorate(injectable(), Vuex.Store);

@injectable()
class SportportStore extends Vuex.Store<RootState> {
    constructor() {
        super({
            strict: process.env.NODE_ENV !== 'production',
            modules: {
                ui: UIModule,
                profile: ProfileModule,
                company: CompanyModule,
                statistic: StatisticModule,
                schedule: SchedulesModule,
                studios: StudiosModule,
                classes: ClassesModule,
                application: ApplicationModule
            }
        });
    }
}

export { RootState };
export default SportportStore;