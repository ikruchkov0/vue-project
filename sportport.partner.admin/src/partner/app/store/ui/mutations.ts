import * as Vuex from 'vuex';
import UIState from './state';

const toggleSidebar = {
    type: 'toggleSidebar'
};

const toggleControlSidebar = {
    type: 'toggleControlSidebar'
};

const showSpinner = {
    type: 'showSpinner'
};

const showMainContent = {
    type: 'showMainContent'
};

const hideSpinner = {
    type: 'hideSpinner'
};

type AlertType = 'warning' | 'error';

class ShowAlertMutation {
    constructor(private _message: string, private _type: AlertType) { }
    static get type() { return 'showAlert'; }
    get type() { return ShowAlertMutation.type; }
    get message() { return this._message; }
    get alertType() { return this._type; }
}

const hideAlert = {
    type: 'hideAlert'
};

const UIMutations = {
    [toggleSidebar.type](state: UIState) {
        state.minimizeSidebar = !state.minimizeSidebar;
    },

    [showSpinner.type](state: UIState) {
        state.showSpinner = true;
    },

    [showMainContent.type](state: UIState) {
        state.mainContentVisible = true;
    },

     [hideSpinner.type](state: UIState) {
        state.showSpinner = false;
    },

    [toggleControlSidebar.type](state: UIState) {
        state.showControlSidebar = !state.showControlSidebar;
    },

    [ShowAlertMutation.type](state: UIState, payload: ShowAlertMutation) {
        state.showAlert = true;
        state.alertMessage = payload.message;
        state.alertType = payload.alertType;
    },

    [hideAlert.type](state: UIState) {
        state.showAlert = false;
    }
};

export { UIMutations as UIMutations, toggleSidebar, toggleControlSidebar, showSpinner, hideSpinner, ShowAlertMutation, hideAlert, showMainContent };