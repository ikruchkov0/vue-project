import * as Vuex from 'vuex';
import RootState from '../root-state';
import UIState from './state';
import { UIMutations } from './mutations';
import UIActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new UIState(),
    mutations: UIMutations,
    actions: UIActions
};

export default Module;