import * as Vuex from 'vuex';
import RootState from '../root-state';
import UIState from './state';

import * as Mutations from './mutations';

const toggleSidebar = {
    type: 'toggleSidebar'
};

const toggleControlSidebar = {
    type: 'toggleControlSidebar'
};

const showSpinner = {
    type: 'showSpinner'
};

const hideSpinner = {
    type: 'hideSpinner'
};

const showMainContent = {
    type: 'showMainContent'
};

type AlertType = 'warning' | 'error';

class ShowAlertAction {
    constructor(private _message: string, private _type: AlertType) { }
    static get type() { return 'showAlert'; }
    get type() { return ShowAlertAction.type; }
    get message() { return this._message; }
    get alertType() { return this._type; }
}

const hideAlert = {
    type: 'hideAlert'
};

const actions = {
    [toggleSidebar.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.toggleSidebar);
    },

    [toggleControlSidebar.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.toggleControlSidebar);
    },

    [showSpinner.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.showSpinner);
    },

    [hideSpinner.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.hideSpinner);
    },

    [ShowAlertAction.type](context: Vuex.ActionContext<UIState, RootState>, payload: ShowAlertAction) {
        context.commit(new Mutations.ShowAlertMutation(payload.message, payload.alertType));
    },

    [hideAlert.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.hideAlert);
    },

    [showMainContent.type](context: Vuex.ActionContext<UIState, RootState>) {
        context.commit(Mutations.showMainContent);
    }
};

const UIActions = { toggleSidebar, toggleControlSidebar, showSpinner, hideSpinner, ShowAlertAction, hideAlert, showMainContent };

export { UIActions };

export default actions;