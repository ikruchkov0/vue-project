export default class UIState {
    minimizeSidebar: boolean = true;
    showControlSidebar: boolean = false;
    showSpinner: boolean = false;
    showAlert: boolean = false;
    alertMessage: string = '';
    alertType: string = '';
    mainContentVisible: boolean = false;
};