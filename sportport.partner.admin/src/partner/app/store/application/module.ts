import * as Vuex from 'vuex';
import RootState from '../root-state';
import ApplicationState from './state';
import { ApplicationMutations } from './mutations';
import ApplicationActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new ApplicationState(),
    mutations: ApplicationMutations,
    actions: ApplicationActions
};

export default Module;