import * as Vuex from 'vuex';
import ApplicationState from './state';

const setContentReady = {
    type: 'setContentReady'
};

class InitGMapServicesMutation {
  constructor(private _googleServices: any) { }
  static get type() { return 'initGMapServicesMutation'; }
  get type() { return InitGMapServicesMutation.type; }
  get googleServices() { return this._googleServices; }
}

const ApplicationMutations = {
    [setContentReady.type](state: ApplicationState) {
        state.contentIsReady = true;
    },
    [InitGMapServicesMutation.type](state: ApplicationState, payload: InitGMapServicesMutation) {
        const google = payload.googleServices;
        state.geocoder = new google.maps.Geocoder();
        state.geocoderStatus = google.maps.GeocoderStatus;
        state.googleMaps = google.maps;
    }
};



export { ApplicationMutations as ApplicationMutations, setContentReady, InitGMapServicesMutation};