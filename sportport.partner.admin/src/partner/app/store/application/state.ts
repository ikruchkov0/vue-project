export default class ApplicationState {
    contentIsReady: boolean = false;
    geocoder: any = null;
    geocoderStatus: any = null;
    googleMaps: any = null;
};