import * as Vuex from 'vuex';
import RootState from '../../root-state';
import ApplicationState from '../state';

import * as Mutations from '../mutations';

import { SportportContainer } from '@sp/partner/ioc';

interface Payload {
    force: boolean;
}

class RedirectToDefaultRunner {
    get router() {
        return SportportContainer.getRouter();
    }

    constructor(private context: Vuex.ActionContext<ApplicationState, RootState>, private payload: Payload) { }

    run() {
        if (this.payload.force || !this.router.currentRoute.name) {
            let studios = this.context.rootState.studios.companyStudios;
            let classes = Object.keys(this.context.rootState.schedule.studios)
                .map(x => this.context.rootState.schedule.studios[x])
                .reduce((array, result) => result.conact(array), []);
            if (!studios || studios.length === 0) {
                this.router.push({ name: 'studios' });
            }
            /*else if (!classes || classes.length === 0) {
                this.router.push({ name: 'schedule', params: { 'id': studios[0].id.toString() } });
            }*/
            else {
                this.router.push({ name: 'dashboard' });
            }
        }
    }
}

export default RedirectToDefaultRunner;
