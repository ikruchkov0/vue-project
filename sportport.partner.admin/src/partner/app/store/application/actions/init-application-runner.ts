import * as Vuex from 'vuex';
import RootState from '../../root-state';
import ApplicationState from '../state';

import * as Mutations from '../mutations';

import { ProfileActions } from '../../profile/actions';
import { UIActions } from '../../ui/actions';
import { StudioActions } from '../../studios/actions';
import { ClassActions } from '../../classes/actions';
import { CompanyActions } from '../../company/actions';
import { SportportContainer } from '@sp/partner/ioc';
import { ApplicationActions } from '.';

class InitApplicationRunner {
    get router() {
        return SportportContainer.getRouter();
    }

    get isAuthorized() {
        return this.context.rootState.profile.user;
    }

    constructor(private context: Vuex.ActionContext<ApplicationState, RootState>) { }

    run(): Promise<any> {
        let action = new ProfileActions.InitUserAction(() => {}, () => this.onUserSignedOut());
        let initMapServicesAction = new ApplicationActions.InitMapServices();
        this.context.dispatch(initMapServicesAction);
        return this.context.dispatch(action).then(() => this.onUserLoaded());
    }

    private onUserSignedOut() {
        let action = new ProfileActions.ClearUserAction();
        this.context.dispatch(action);
    }

    private onUserLoaded(): Promise<any> {
        if (this.isAuthorized) {
            return this.preloadData().then(() => {
                this.context.commit(Mutations.setContentReady);
                return this.context.dispatch(new ApplicationActions.RedirectToDefault()).then(() => this.showRouting());
            },
            (result) => {
                if (result && result.reason === 'company') {
                    this.router.push({name: 'company'});
                    this.showRouting();
                }
            })
            .catch(() => {
                this.showRouting();
            });
        } else {
            if (this.router.currentRoute.name !== 'auth-callback') {
                this.context.dispatch(new ProfileActions.LoginAction());
            }
        }

        if (this.router.currentRoute.name === 'auth-callback') {
            this.showRouting();
            return Promise.resolve({});
        }

        if (this.context.state.contentIsReady) {
            return Promise.resolve({});
        }
    }

    private showRouting() {
        this.context.dispatch(UIActions.showMainContent);
    }

    private preloadData(): Promise<any> {
        return this.context.dispatch(new CompanyActions.LoadCompanyAction()).then(() => {
            if (!this.context.rootState.company.currentCompany) {
                return Promise.reject({ reason: 'company'});
            }
            return Promise.all([
                this.context.dispatch(new StudioActions.LoadStudiosAction()),
                this.context.dispatch(new ClassActions.LoadClassCategoriesAction()),
                this.context.dispatch(new StudioActions.LoadStudioFacilitiesDictAction())
            ]);
        });
    }
}

export default InitApplicationRunner;
