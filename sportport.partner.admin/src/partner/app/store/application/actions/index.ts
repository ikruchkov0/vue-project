import InitApplicationRunner from './init-application-runner';
import RedirectToDefaultRunner from './redirect-to-default-runner';
import * as Mutations from '../mutations';

import AppConstants from '../../constants';

import { getGoogleMapsService } from '../../../../google-maps-service';

class InitApplication {
    static get type() { return 'initApplication'; }
    get type() { return InitApplication.type; }
};

class InitMapServices {
    static get type() { return 'initMapServices'; }
    get type() { return InitMapServices.type; }
};

class RedirectToDefault {
    static get type() { return 'redirectToDefault'; }
    get type() { return RedirectToDefault.type; }
    constructor(private _force: boolean = false) { }
    get force() { return this._force; }
};

const actions = {
    [InitApplication.type]: (context) => new InitApplicationRunner(context).run(),
    [RedirectToDefault.type]: (context, payload) => new RedirectToDefaultRunner(context, payload).run(),

    [InitMapServices.type]: (context) => {
        return getGoogleMapsService().then((service) => {
            return service.load(AppConstants.googleMapsApiKey).then(google => {
                let mapsMutation = new Mutations.InitGMapServicesMutation(google);
                context.commit(mapsMutation);
            });
        });
    }
};

const ApplicationActions = { InitApplication, RedirectToDefault, InitMapServices };
export { ApplicationActions };
export default actions;