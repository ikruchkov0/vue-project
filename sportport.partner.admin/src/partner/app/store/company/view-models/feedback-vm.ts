import { validate,
    IsDefined, IsNotEmpty, IsPositive, MinLength, ValidationError } from 'class-validator';
import { BaseVM } from '@sp/shared/base-vm';
import { ClassCategoryVM } from '@sp/partner/view-models';

export default class FeedbackVM extends BaseVM<FeedbackVM> {
    id: number;

    @IsDefined({ message: 'Идентификатор компании не задан' })
    @IsPositive({ message: 'Идентификатор компании не задан' })
    companyId: number;

    @IsDefined({ message: 'Идентификатор пользователя не задан' })
    @IsPositive({ message: 'Идентификатор пользователя не задан' })
    userId: number;

    @IsDefined({ message: 'Введите заголовок' })
    @IsNotEmpty({ message: 'Введите заголовок' })
    @MinLength(4, { message: 'Заголовок должен быть не короче $constraint1 символов' })
    title: string;

    @IsDefined({ message: 'Сообщение должно быть заполнено' })
    @IsNotEmpty({ message: 'Сообщение должно быть заполнено' })
    message: string;
}