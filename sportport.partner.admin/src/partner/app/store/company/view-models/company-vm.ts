import { validate, IsDefined, IsNotEmpty, IsEmail, MinLength, MaxLength, Min, Max, IsMobilePhone, ValidationError } from 'class-validator';
import { StudioVM } from '@sp/partner/view-models';
import { BaseVM } from '@sp/shared/base-vm';

export default class CompanyVM extends BaseVM<CompanyVM> {

    id: number;

    @IsNotEmpty({
        message: 'Введите название компании'
    })
    title: string;

    @IsNotEmpty({
        message: 'Введите телефон компании'
    })
    @MinLength(6, {
        message: 'Телефон должен быть не короче $constraint1 символов'
    })
    phone: string;

    @MinLength(10, {
        message: 'Мобильный телефон должен быть не короче $constraint1 символов'
    })
    @IsMobilePhone('ru-RU', {
        message: 'Введите корректный мобильный телефон'
    })
    mobile: string;

    @IsNotEmpty({
        message: 'Введите email'
    })
    @IsEmail({}, {
        message: 'Введите корректный email адрес'
    })
    email: string;

    @IsNotEmpty({
        message: 'Введите город'
    })
    @MinLength(2, {
        message: 'Город должен быть не короче $constraint1 символов'
    })
    city: string;

    @IsNotEmpty({
        message: 'Введите адрес'
    })
    @MinLength(8, {
        message: 'Адрес должен быть не короче $constraint1 символов'
    })
    address: string;

    @IsNotEmpty({
        message: 'Введите область'
    })
    @MinLength(2, {
        message: 'Область должна быть не короче $constraint1 символов'
    })
    state: string;

    zip: string;
};