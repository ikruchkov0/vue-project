import * as Vuex from 'vuex';
import CompanyService from './services/company-service';
import FeedbackService from './services/feedback-service';
import RootState from '../root-state';
import DashboardState from './state';
import { CompanyVM, FeedbackVM } from '@sp/partner/view-models';

import * as Mutations from './mutations';

class LoadCompanyAction {
    static get type() { return 'loadCompany'; }
    get type() { return LoadCompanyAction.type; }
}

class UpdateCompanyAction {
    constructor(private _company: CompanyVM) { }

    static get type() { return 'updateCompany'; }
    get type() { return UpdateCompanyAction.type; }
    get company() { return this._company; }
}

class CreateCompanyAction {
    constructor(private _company: CompanyVM) { }

    static get type() { return 'createCompanyAction'; }
    get type() { return CreateCompanyAction.type; }
    get company() { return this._company; }
}

class UploadCompanyFeedbackAction {
    constructor(private _feedback: FeedbackVM) { }
    static get type() { return 'uploadCompanyFeedback'; }
    get type() { return UploadCompanyFeedbackAction.type; }
    get feedback() { return this._feedback; }
}

const actions = {
    [LoadCompanyAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: LoadCompanyAction) {
        let service = new CompanyService();
        return service.getCompanyForUser().then((company) => {
            let mutation = new Mutations.SetCompanyMutation(company);
            context.commit(mutation);
        });
    },

    [UpdateCompanyAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: UpdateCompanyAction) {
        let service = new CompanyService();
        return service.updateCompany(context.rootState.company.currentCompany.id, payload.company).then((company) => {
            let mutation = new Mutations.SetCompanyMutation(company);
            context.commit(mutation);
        });
    },

    [CreateCompanyAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: UpdateCompanyAction) {
        let service = new CompanyService();
        return service.saveCompany(payload.company).then((company) => {
            let mutation = new Mutations.SetCompanyMutation(company);
            context.commit(mutation);
        });
    },

    [UploadCompanyFeedbackAction.type](context: Vuex.ActionContext<DashboardState, RootState>, payload: UploadCompanyFeedbackAction) {
        let service = new FeedbackService();
        return service.uploadFeedback(context.rootState.company.currentCompany.id, payload.feedback).then((feedback) => {
        });
    }
};


const CompanyActions = { LoadCompanyAction, UpdateCompanyAction, UploadCompanyFeedbackAction, CreateCompanyAction };

export { CompanyActions };

export default actions;