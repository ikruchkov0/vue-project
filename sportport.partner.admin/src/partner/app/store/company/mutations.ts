import * as Vuex from 'vuex';
import CompanyState from './state';
import { CompanyVM } from './view-models';

class SetCompanyMutation {
    constructor(private _company: CompanyVM) { }
    static get type() { return 'setCurrentCompany'; }
    get type() { return SetCompanyMutation.type; }
    get company() { return this._company; }
}

const mutations = {
    [SetCompanyMutation.type](state: CompanyState, payload: SetCompanyMutation) {
        state.currentCompany = payload.company;
    },

};

export { mutations as CompanyMutations, SetCompanyMutation };