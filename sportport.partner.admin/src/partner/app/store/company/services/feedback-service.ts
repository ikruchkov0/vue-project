import { FeedbackVM } from '@sp/partner/view-models';
import { BaseDataService }  from '../../base-data-service';

export default class FeedbackService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    uploadFeedback(companyId: number, feedback: FeedbackVM): Promise<FeedbackVM> {
      return this.post(`api/v1/Companies/${companyId}/Feedback`, feedback)
        .then((json) => new FeedbackVM(json));
    }
}