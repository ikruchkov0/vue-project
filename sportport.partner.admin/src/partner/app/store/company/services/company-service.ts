import { CompanyVM, StudioVM } from '@sp/partner/view-models';
import { BaseDataService }  from '../../base-data-service';

export default class CompanyService extends BaseDataService {
    constructor() {
        super('CompaniesApi');
    }

    getCompanyForUser(): Promise<CompanyVM> {
        return this.get(`api/v1/Users/Companies`).then((json) => json ? new CompanyVM(json) : null);
    }

    updateCompany(companyId: number, company: CompanyVM): Promise<CompanyVM> {
        return this.put(`api/v1/Companies/${companyId}`, company).then((json) => new CompanyVM(json));
    }

    saveCompany(company: CompanyVM): Promise<CompanyVM> {
        return this.post(`api/v1/Users/Companies`, company).then((json) => new CompanyVM(json));
    }
}