import * as Vuex from 'vuex';
import RootState from '../root-state';
import CompanyState from './state';
import { CompanyMutations } from './mutations';
import CompanyActions from './actions';

const Module: Vuex.ModuleTree<RootState> = {
    state: new CompanyState(),
    mutations: CompanyMutations,
    actions: CompanyActions
};

export default Module;