import * as VueRouter from 'vue-router';
import { injectable } from 'inversify';

import AppComponent from './components/app/app.component';
import SportportStore from './store';
import SportportRouter from './router';
import SportportErrorHandler from './error-handler';

@injectable()
export default class App {
    private component: AppComponent;

    constructor(private store: SportportStore, private router: SportportRouter, private errorHandler: SportportErrorHandler) {
        this.component = new AppComponent({ router: this.router, store: this.store });
        this.errorHandler.register();
    }

    run(el: HTMLElement) {
         this.component.$mount(el);
    }
}