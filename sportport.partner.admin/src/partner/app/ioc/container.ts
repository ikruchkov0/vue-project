import { Container, injectable, inject } from 'inversify';
import App from '../app';

import SportportStore from '../store';
import SportportRouter from '../router';
import SportportErrorHandler from '../error-handler';

class SportportContainer {
    private container: Container;

    createAppInstance(): App {
        return this.container.get<App>(App);
    }

    getStore(): SportportStore {
        return this.container.get<SportportStore>(SportportStore);
    }

    getRouter(): SportportRouter {
        return this.container.get<SportportRouter>(SportportRouter);
    }

    init() {
        let container = new Container();
        container.bind(SportportStore).toConstantValue(new SportportStore());
        container.bind(SportportRouter).toConstantValue(new SportportRouter());
        container.bind(SportportErrorHandler).toSelf();
        container.bind(App).toSelf();
        this.container = container;
    }
}

let instance = new SportportContainer();

export { instance as SportportContainer };