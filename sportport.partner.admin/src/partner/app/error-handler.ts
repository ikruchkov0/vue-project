import { injectable } from 'inversify';

import SportportRouter from './router';
import SportportStore from './store';
import { UIActions, ProfileActions } from '@sp/partner/actions';
import * as ServerErrors from '@sp/shared/server-errors';
import SharedErrorHandler from '@sp/shared/error-handler';

@injectable()
export default class SportportErrorHandler extends SharedErrorHandler {

    constructor(private router: SportportRouter, private store: SportportStore) {
        super({
            default: () => store.dispatch(new UIActions.ShowAlertAction('Неизвестная ошибка', 'error')),
            undefinedError: () => store.dispatch(new UIActions.ShowAlertAction('Неизвестная ошибка', 'error')),
            [ServerErrors.AuthorizationError.errorName]: () => this.store.dispatch(new ProfileActions.SilentLoginAction()).then(() => this.router.push({ name: '401' })),
            [ServerErrors.ValidationError.errorName]: () => this.store.dispatch(new UIActions.ShowAlertAction('Введены неправильные данные', 'error')),
            [ServerErrors.ServerError.errorName]: () => this.store.dispatch(new UIActions.ShowAlertAction('Произошла непредвиденная ошибка на сервере', 'error'))
        });
    }
}