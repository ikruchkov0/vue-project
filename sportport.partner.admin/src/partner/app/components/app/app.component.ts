import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './app.template.html';

@WithRender
@Component({
  name: 'App'
})
export default class App extends SportportVue {
}