import Main from './main/main.component';
import MainRoutes from './main/routes';
import AuthCallback from './main/routes/auth-callback/auth-callback.component';

let publicPath = process.env.ENV_PUBLIC_PATH !== undefined
    ? process.env.ENV_PUBLIC_PATH.toString().replace(/\/$/, '')
    : '';

export default [
    {
        path: publicPath + '/auth-callback',
        name: 'auth-callback',
        meta: {
            text: 'Авторизация',
        },
        component: AuthCallback
    },
    {
        path: publicPath,
        meta: {
            text: 'Главная',
        },
        component: Main,
        children: MainRoutes
    },
    { path: '', redirect: 'main' },
    { path: '*', redirect: 'main' }
];