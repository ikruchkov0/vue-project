import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './content-header.template.html';

@WithRender
@Component({
    name: 'ContentHeader',
    props: ['title', 'description']
})
export default class ContentHeader extends SportportVue {
    title: string;
    description: string;

    private routeMap(x) {
        return {
            text: x.meta.text,
            name: x.name,
            active: x.path === this.$route.path
        };
    }

    get breadCrumbs() {
        return this.$route.matched.map(this.routeMap);
    }
}