import BaseInputComponent from '../base-input.component';
import Component from 'vue-class-component';
import * as WithRender from './addon-input.template.html';

@WithRender
@Component({
    name: 'AddonInput'
})
export default class AddonInput extends BaseInputComponent { }