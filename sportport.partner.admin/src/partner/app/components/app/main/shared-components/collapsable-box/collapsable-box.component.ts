import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './collapsable-box.template.html';
import './collapsable-box.less';

import SpinnerOverlay from '../spinner-overlay/spinner-overlay.component';

@WithRender
@Component({
    name: 'CollapsableBox',
    components: {
      'spinner-overlay': SpinnerOverlay
    },
    props: ['isBusy']
})

export default class CollapsableBox extends SportportVue {
  view = {
    _isShowDetails: false
  };
  isBusy: boolean;

  showDetails() {
    this.view._isShowDetails = !this.view._isShowDetails;

    if (this.view._isShowDetails)
      this.$emit('open');
    else
      this.$emit('close');
  }
}