import BaseInputComponent from '../base-input.component';
import Component from 'vue-class-component';
import * as WithRender from './inline-text-input.template.html';

@WithRender
@Component({
    name: 'InlineTextInput'
})
export default class TextInput extends BaseInputComponent { }