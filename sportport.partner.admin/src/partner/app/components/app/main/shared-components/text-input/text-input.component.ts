import BaseInputComponent from '../base-input.component';
import Component from 'vue-class-component';
import * as WithRender from './text-input.template.html';

@WithRender
@Component({
    name: 'TextInput'
})
export default class TextInput extends BaseInputComponent { }