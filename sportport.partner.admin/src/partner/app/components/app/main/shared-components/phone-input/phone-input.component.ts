import BaseInputComponent from '../base-input.component';
import Component from 'vue-class-component';
import * as WithRender from './phone-input.template.html';

@WithRender
@Component({
    name: 'PhoneInput'
})
export default class PhoneInput extends BaseInputComponent { }