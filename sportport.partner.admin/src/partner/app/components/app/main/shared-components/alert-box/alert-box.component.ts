import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './alert-box.template.html';

@WithRender
@Component({
    name: 'AlertBox',
    components: {

    },
    props: {
      'title': { type: String, required: true },
      'messages': { type: Array, required: false}
    }
})

export default class AlertBox extends SportportVue {
  title: string;
  messages: Array<string>;
}