import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './spinner-overlay.template.html';
import './spinner-overlay.less';

@WithRender
@Component({
    name: 'SpinnerOverlay',
    components: {

    },
    props: ['isBusy', 'title']
})

export default class SpinnerOverlay extends SportportVue {
  isBusy: boolean;
  title: string;
}