import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './box-info.template.html';
import './box-info.less';

import SpinnerOverlay from '../spinner-overlay/spinner-overlay.component';

@WithRender
@Component({
    name: 'BoxInfo',
    components: {
      'spinner-overlay': SpinnerOverlay
    },
    props: ['showFooter', 'isBusy', 'noPadding']
})

export default class BoxInfo extends SportportVue {
  showFooter: Boolean;
  isBusy: Boolean;
}