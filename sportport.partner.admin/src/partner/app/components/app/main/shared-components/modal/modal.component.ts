import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './modal.template.html';
import './modal.less';

import SpinnerOverlay from '../spinner-overlay/spinner-overlay.component';

@WithRender
@Component({
    name: 'Modal',
    components: {
      'spinner-overlay': SpinnerOverlay
    },
    props: {
      'title': { type: String, required: true },
      'isBusy': { type: Boolean },
      'submitTitle': { type: String, required: false, default: 'Сохранить'},
      'closeTitle': { type: String, required: false, default: 'Отмена'},
      'submitBtnClasses': { type: String, required: false, default: 'btn-outline'},
      'closeBtnClasses': { type: String, required: false, default: 'btn-outline'},
    }
})

export default class Modal extends SportportVue {
  title: string;
  isBusy: Boolean;
  submitTitle: string;
  closeTitle: string;
  submitBtnClasses: string;
  closeBtnClasses: string;
}