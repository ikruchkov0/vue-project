import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { ValidationError } from 'class-validator';

@Component({
    name: 'BaseInput',
    props: ['placeholder', 'title', 'change', 'value', 'field', 'validationResults', 'index', 'target', 'isDisabled']
})
export default class BaseInput extends SportportVue {
    placeholder: string;
    title: string;
    change: (value, field, index?) => void;
    value: string;
    field: string;
    index: number;
    target: Object;
    isDisabled: boolean;
    validationResults: Array<ValidationError>;

    get id() {
        return `input-${this.field}`;
    }

    get validationMessage() {
        if (!this.validationResults) {
            return '';
        }
        let result = this.validationResults.filter(x => x.property === this.field);
        if (this.target) {
            result = result.filter(x => x.target === this.target);
        }
        let validationError = result[0];
        if (validationError) {
            let messages = Object.keys(validationError.constraints).map(x => validationError.constraints[x]);
            return messages[0];
        } else {
            return '';
        }
    }

    get isValid() {
        if (this.validationMessage) {
            return false;
        }
        if (typeof this.value === 'undefined' || this.value === null || this.value.length === 0) {
            return null;
        }
        return true;
    }

    onChange(event) {
        let value = event.target.value;
        this.change(value, this.field, this.index);
    }
}