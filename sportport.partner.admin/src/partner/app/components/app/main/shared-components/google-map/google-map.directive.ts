import * as Vue from 'vue';

class GMapController {

    private mapsProvider: any = null;
    private lng: number = null;
    private lat: number = null;
    private zoom: number = null;

    bind(el, initialState) {
        this.initVariables(initialState);
        this.updateMap(el);
    }

    unbind() {

    }

    update(el, state) {
        if (state.lat === this.lat
            && state.lng === this.lng
            && state.zoom === this.zoom) {
            return;
        }
        this.initVariables(state);
        this.updateMap(el);
    }

    protected initVariables(updatedState) {
        this.mapsProvider = updatedState.mapsProvider;
        this.lng = updatedState.lng;
        this.lat = updatedState.lat;
        this.zoom = updatedState.zoom;
    }

    protected checkRequiredParameters() {
        if (this.mapsProvider == null
            || this.lng == null
            || this.lat == null)
            return false;
        return true;
    }

    protected updateMap(el) {
        if (!this.checkRequiredParameters())
            return;
        let mapsProvider = this.mapsProvider;
        const latlng = new mapsProvider.LatLng(this.lat, this.lng);
        const mapOptions = {
          zoom: this.zoom,
          center: latlng,
          disableDefaultUI: true
        };
        const map = new mapsProvider.Map(el, mapOptions);
        map.setOptions({draggable: false, zoomControl: true, scrollwheel: false, disableDoubleClickZoom: true});

        const marker = new mapsProvider.Marker({
          position: latlng,
          map: map
        });
    }
}

const controller = new GMapController();

export default {
    bind(el: HTMLElement, binding: Vue.VNodeDirective, vnode: Vue.VNode) {
        controller.bind(el, binding.value);
    },
    update(el: HTMLElement, binding: Vue.VNodeDirective, vnode: Vue.VNode) {
        controller.update(el, binding.value);
    },
    unbind() {
        controller.unbind();
    }
};