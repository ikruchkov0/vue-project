import BaseInputComponent from '../base-input.component';
import Component from 'vue-class-component';
import * as WithRender from './select-input.template.html';

@WithRender
@Component({
    name: 'SelectInput',
    props: {
      'options': { type: Array, required: true },
      'defaultTitle': { type: String, required: true },
      'selectedKey': { },
      'keyName': { type: String, required: false, default: 'id' },
      'valueName': { type: String, required: false, default: 'value' }
    }
})
export default class TextInput extends BaseInputComponent {
  options: Array<any>;
  defaultTitle: string;
  keyName: string;
  valueName: string;
  selectedKey: any;

  update() {
    console.log('UPDATED');
  }
}