import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './user-account-dropdown.template.html';
import { UserVM } from '@sp/partner/view-models';
import { ProfileActions } from '@sp/partner/actions';

@WithRender
@Component({
    name: 'UserAccountDropdown',
    props: ['user']
})
export default class UserAccountDropdown extends SportportVue {
    user: UserVM;

    get username() {
        return this.user.profile.name;
    }

    get avatar() {
        return this.user.profile.avatarUrl;
    }

    get role() {
        return this.user.profile.role;
    }

    logout() {
        this.$$store.dispatch(new ProfileActions.LogoutAction());
    }
}