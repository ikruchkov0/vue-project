import * as Vue from 'vue';

const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
const BOOTSTRAP_MOBILE_MAX_WIDTH = 768;

class SidebarController {
    private width: number = 0;
    private state: boolean = true;
    private changesHandler = () => this.updateWidth();

    bind(initialState) {
        this.width = 0;
        this.state = initialState.minimize;
        if (document.readyState !== 'complete' && document.readyState !== 'interactive') {
            document.addEventListener('DOMContentLoaded', this.changesHandler, true);
        } else {
            this.changesHandler();
        }
        window.addEventListener('resize', this.changesHandler, true);
    }

    unbind() {
        document.removeEventListener('DOMContentLoaded', this.changesHandler, true);
        window.removeEventListener('resize', this.changesHandler, true);
    }

    update(state) {
        this.state = state.minimize;
        this.minimizeSidebar();
    }

    private updateWidth() {
        this.width = document.body.clientWidth;
        this.minimizeSidebar();
    }

    private collapseSidebar(val) {
        if (val) {
            document.body.classList.add('sidebar-collapse');
        }
    }

    private hideSidebar(val) {
        if (!val) {
            document.body.classList.add('sidebar-open');
        }
    }

    private minimizeSidebar() {
        document.body.classList.remove('sidebar-collapse');
        document.body.classList.remove('sidebar-open');
        if (this.width < BOOTSTRAP_MOBILE_MAX_WIDTH) {
            this.hideSidebar(this.state);
        } else {
            this.collapseSidebar(this.state);
        }
    }
}

const controller = new SidebarController();

export default {
    bind(el: HTMLElement, binding: Vue.VNodeDirective, vnode: Vue.VNode) {
        controller.bind(binding.value);
    },
    update(el: HTMLElement, binding: Vue.VNodeDirective, vnode: Vue.VNode) {
        controller.update(binding.value);
    },
    unbind() {
        controller.unbind();
    }
};