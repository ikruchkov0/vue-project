import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './content.template.html';
import AlertBox from './alert-box/alert-box.component';
import './content.less';

@WithRender
@Component({
    name: 'Content',
    components: {
        'alert-box': AlertBox
    },
})
export default class Content extends SportportVue {
    get showMainContent() {
        return !!this.$$store.state.ui.mainContentVisible;
    }
}