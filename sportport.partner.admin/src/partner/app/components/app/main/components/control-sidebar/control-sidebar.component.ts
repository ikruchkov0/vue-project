import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './control-sidebar.template.html';

@WithRender
@Component({ name: 'ControlSidebar'})
export default class ControlSidebar extends SportportVue {
    get showControlSidebar(){
        return this.$$store.state.ui.showControlSidebar;
    }
}