import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './sidebar.template.html';
import SidebarControlDirective from './sidebar-control.directive';
import ScheduleMenu from './schedule-menu/schedule-menu.component';

@WithRender
@Component({
    name: 'Sidebar',
    directives: {
        'sidebar-control': SidebarControlDirective
    },
    components: {
        'schedule-menu': ScheduleMenu
    }
})
export default class Sidebar extends SportportVue {
    get showSidebar() {
        return !!this.$$store.state.application.contentIsReady;
    }

    get hasCompany() {
        return !!this.$$store.state.company;
    }

    get minimizeSidebar() {
        return this.$$store.state.ui.minimizeSidebar;
    }
}