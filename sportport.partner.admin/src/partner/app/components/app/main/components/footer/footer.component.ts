import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './footer.template.html';

@WithRender
@Component({ name: 'Footer'})
export default class Footer extends SportportVue {
}