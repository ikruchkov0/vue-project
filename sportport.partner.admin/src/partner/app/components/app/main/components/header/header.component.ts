import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './header.template.html';

import Messages from './messages/messages.component';
import Notifications from './notifications/notifications.component';
import UserAccount from './user-account/user-account.component';
import Logo from './logo/logo.component';
import { UIActions } from '@sp/partner/actions';


@WithRender
@Component({
    components: {
        'messages': Messages,
        'notifications': Notifications,
        'user-account': UserAccount,
        'logo': Logo
    },
    name: 'Header',
})
export default class Header extends SportportVue {

    get user() {
        return this.$$store.state.profile.user;
    }

    toggleSidebar() {
        this.$$store.dispatch(UIActions.toggleSidebar.type);
    }

    toggleControlSidebar() {
        this.$$store.dispatch(UIActions.toggleControlSidebar.type);
    }
}