import Content from './content/content.component';
import Header from './header/header.component';
import Sidebar from './sidebar/sidebar.component';
import ControlSidebar from './control-sidebar/control-sidebar.component';
import ControlSidebarBackground from './control-sidebar/background/background.component';
import Footer from './footer/footer.component';
import Spinner from './spinner/spinner.component';

export { Content, Header, Sidebar, ControlSidebar, ControlSidebarBackground, Footer, Spinner };