import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './logo.template.html';

@WithRender
@Component
export default class Logo extends SportportVue {
}