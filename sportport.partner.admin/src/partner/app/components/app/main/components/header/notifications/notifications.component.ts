import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './notifications.template.html';
import NotificationsDropdown from './notifications-dropdown/notifications-dropdown.component';
import HeaderItem from '../header-item/header-item.component';

@WithRender
@Component({
    name: 'Notifications',
    components: {
        'notifications-dropdown': NotificationsDropdown,
        'header-item': HeaderItem
    }
})
export default class Notifications extends SportportVue {

    showDropdown: boolean = false;

    toggleDropdown() {
        this.showDropdown = !this.showDropdown;
    }
}