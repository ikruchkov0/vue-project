import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './header-item.template.html';

@WithRender
@Component
export default class HeaderItem extends SportportVue {

    showDropdown: boolean = false;

    toggleDropdown() {
        this.showDropdown = !this.showDropdown;
    }
}