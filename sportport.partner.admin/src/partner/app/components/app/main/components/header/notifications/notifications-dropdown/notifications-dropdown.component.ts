import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './notifications-dropdown.template.html';

@WithRender
@Component({
    name: 'NotificationsDropdown'
})
export default class NotificationsDropdown extends SportportVue {
}