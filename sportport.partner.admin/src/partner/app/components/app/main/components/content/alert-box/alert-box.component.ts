import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './alert-box.template.html';

import './alert-box.less';
import { UIActions } from '@sp/partner/actions';

@WithRender
@Component({
    name: 'AlertBox'
})
export default class AlertBox extends SportportVue {
    get showBox() {
        return this.$$store.state.ui.showAlert;
    }

    get message() {
        return this.$$store.state.ui.alertMessage;
    }

    get isError() {
        return this.alertType === 'error';
    }

    get isWarning() {
        return this.alertType === 'warning';
    }

    close() {
        this.$$store.dispatch(UIActions.hideAlert);
    }

    private get alertType() {
        return this.$$store.state.ui.alertType;
    }
}