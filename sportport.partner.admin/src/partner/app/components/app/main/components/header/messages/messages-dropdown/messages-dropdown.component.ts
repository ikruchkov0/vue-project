import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './messages-dropdown.template.html';

@WithRender
@Component({
    name: 'MessagesDropdown'
})
export default class MessagesDropdown extends SportportVue {
}