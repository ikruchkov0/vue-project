import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './messages.template.html';
import MessagesDropdown from './messages-dropdown/messages-dropdown.component';
import HeaderItem from '../header-item/header-item.component';


@WithRender
@Component({
    name: 'Messages',
    components: {
        'messages-dropdown': MessagesDropdown,
        'header-item': HeaderItem
    }
})
export default class Messages extends SportportVue {

    showDropdown: boolean = false;

    toggleDropdown() {
        this.showDropdown = !this.showDropdown;
    }
}