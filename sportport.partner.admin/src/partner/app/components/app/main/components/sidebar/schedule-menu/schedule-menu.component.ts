import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './schedule-menu.template.html';
import * as Actions from '@sp/partner/actions';
import './schedule-menu.less';

@WithRender
@Component({
    name: 'ScheduleMenu',
    props: ['sidebarMinimized']
})
export default class ScheduleMenu extends SportportVue {
    menuOpened: boolean = false;
    sidebarMinimized: boolean;

    get studios() {
        return this.$$store.state.studios.companyStudios;
    }

    get showMenu() {
        return this.menuOpened || this.sidebarMinimized;
    }

    toggle() {
        if (this.sidebarMinimized) {
            return;
        }
        this.menuOpened = !this.menuOpened;
    }
}