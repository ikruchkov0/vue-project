import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './background.template.html';

@WithRender
@Component({ name: 'ControlSidebarBackground'})
export default class ControlSidebarBackground extends SportportVue {
}