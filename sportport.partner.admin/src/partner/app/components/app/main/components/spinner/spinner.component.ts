import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './spinner.template.html';

@WithRender
@Component({
    name: 'Spinner'
})
export default class Spinner extends SportportVue {
    get showSpinner() {
        return this.$$store.state.ui.showSpinner;
    }
}