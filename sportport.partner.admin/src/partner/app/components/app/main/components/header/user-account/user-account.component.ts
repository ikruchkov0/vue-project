import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './user-account.template.html';
import UserAccountDropdown from './user-account-dropdown/user-account-dropdown.component';
import HeaderItem from '../header-item/header-item.component';
import { UserVM } from '@sp/partner/view-models';
import { ProfileActions } from '@sp/partner/actions';
import './user-account.less';

@WithRender
@Component({
    name: 'UserAccount',
    components: {
        'user-account-dropdown': UserAccountDropdown,
        'header-item': HeaderItem
    },
    props: ['user']
})
export default class UserAccount extends SportportVue {
    user: UserVM;

    get username() {
        return this.user.profile.name;
    }

    get avatar() {
        return this.user.profile.avatarUrl;
    }

    login() {
        this.$$store.dispatch(new ProfileActions.LoginAction());
    }
}