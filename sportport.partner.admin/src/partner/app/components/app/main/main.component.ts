import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './main.template.html';

import { Content, Header, Sidebar, ControlSidebar, ControlSidebarBackground, Footer, Spinner } from './components';

import { ApplicationActions } from '@sp/partner/actions';
import { SpinnerOverlay } from '@sp/partner/shared-components';

import './main.less';

@WithRender
@Component({
    components: {
        'main-header': Header,
        'main-footer': Footer,
        'main-sidebar': Sidebar,
        'main-content': Content,
        'control-sidebar': ControlSidebar,
        'control-sidebar-background': ControlSidebarBackground,
        'spinner': Spinner,
        'spinner-overlay': SpinnerOverlay
    },
    name: 'Main',
})
export default class Main extends SportportVue {

    get isAuthorized() {
        return this.$$store.state.profile.user;
    }

    mounted() {
        this.$$store.dispatch(new ApplicationActions.InitApplication());
    }
 }