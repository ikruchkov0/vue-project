import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './studio.template.html';
import { PartnerStatisticVM } from '@sp/partner/view-models';
import { BoxInfo } from '@sp/partner/shared-components';

@WithRender
@Component({
    name: 'Studio',
    props: ['studio'],
    components: {
        'box-info': BoxInfo
    }
})
export default class Studio extends SportportVue {

    studio: PartnerStatisticVM;

    showPaymentDetails: boolean = false;
    showTrainingsDetails: boolean = false;

    get totalIncome() {
        return this.studio.revenue;
    }

    get incomes() {
        return this.studio.classes.map(x => {
            return {
                name: x.title,
                amount: x.revenue,
                percentage: (x.revenue / this.studio.revenue) * 100
            };
        });
    }

    get name() {
        return this.studio.title;
    }

    get address() {
        return this.studio.address;
    }

    get visited() {
        return this.studio.visited;
    }

    get canceled() {
        return this.studio.canceled;
    }

    get pending() {
        return this.studio.pending;
    }

    get scheduled() {
        return this.visited + this.canceled + this.pending;
    }

    togglePaymentDetails() {
        this.showPaymentDetails = !this.showPaymentDetails;
    }

    toggleTrainingsDetails() {
        this.showTrainingsDetails = !this.showTrainingsDetails;
    }
};