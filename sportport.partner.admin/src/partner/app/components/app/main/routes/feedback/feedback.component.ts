import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { FeedbackVM } from '@sp/partner/view-models';
import { ValidationError } from 'class-validator';
import { CompanyActions } from '@sp/partner/actions';
import * as WithRender from './feedback.template.html';

import { BoxInfo, TextInput } from '@sp/partner/shared-components';

@WithRender
@Component({
    name: 'Feedback',
    components: {
      'text-input': TextInput,
      'box-info': BoxInfo
    }
})

export default class Feedback extends SportportVue {
  _feedback: FeedbackVM;
  validationResults: Array<ValidationError> = [];
  view = {
    _isBusy: false,
    _formSubmitted: false
  };
  isBusy: Boolean;

  get company() {
    return this.$$store.state.company.currentCompany;
  }

  get user() {
    return this.$$store.state.profile.user;
  }

  created() {
    this.createNewMessage();
  }

  onChange(value, field) {
    this._feedback[field] = value;
    this.validate();
  }

  validate() {
    this._feedback.validate().then((errors) => this.validationResults = errors);
  }

  createNewMessage() {
    this.view._formSubmitted = false;

    // TODO: Company ID
    const companyId = 1; // this.company.id;
    const userId = 1; // this.user.id;

    if (!this._feedback) {
      this._feedback = new FeedbackVM();
      this._feedback.companyId = companyId;
      this._feedback.userId = userId;
    }
    this._feedback.title = '';
    this._feedback.message = '';
  }

  async save() {
    let errors = await this._feedback.validate();
    if (errors && errors.length > 0) {
      console.log('ERRORS', errors);
      this.validationResults = errors;
      return;
    }

    this.view._isBusy = true;

    this.$$store.dispatch(new CompanyActions.UploadCompanyFeedbackAction(this._feedback))
        .then(() => {
          this.view._isBusy = false;
          this.view._formSubmitted = true;
        })
        .catch(errors => {
          this.view._isBusy = false;
          this.view._formSubmitted = true;
        });
  }
}