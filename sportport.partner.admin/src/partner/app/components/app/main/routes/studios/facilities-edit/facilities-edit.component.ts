import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { StudioVM, StudioFacilityVM } from '@sp/partner/view-models';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './facilities-edit.template.html';
import { ValidationError } from 'class-validator';

import { Modal } from '@sp/partner/shared-components';

@WithRender
@Component({
  name: 'FacilitiesEdit',
  components: {
    modal: Modal
  },
  props: ['studio', 'title']
})

export default class FacilitiesEdit extends SportportVue {
  title: string;
  studio: StudioVM;
  _studio: StudioVM;
  _facilities: Array<StudioFacilityVM>;
  isBusy: Boolean = false;

  get facilitiesDict() {
    return this._facilities;
  }

  created() {
    this._studio = this.studio.clone();
    this._facilities = this.$$store.state.studios.facilitiesDict.slice();
    this._facilities.filter(f => this._studio.facilities.indexOf(f.id) !== -1)
      .forEach(f => f.checked = true);
  }

  async save() {
    let changedFacilities = this._facilities.filter(f => f.checked === true);
    this._studio.facilities = changedFacilities.map(f => f.id);

    console.log('STUDIO:', this._studio);

    this.isBusy = true;
    this.$$store.dispatch(new StudioActions.UpdateStudioAction(this._studio.id, this._studio))
      .then(() => {
        this.isBusy = false;
        this.$emit('close');
      })
      .catch(() => this.isBusy = false);
  }
}