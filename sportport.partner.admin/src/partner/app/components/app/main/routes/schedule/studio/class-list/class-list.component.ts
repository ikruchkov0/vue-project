import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './class-list.template.html';
import { ScheduleActions } from '@sp/partner/actions';
import { ClassVM } from '@sp/partner/view-models';
import ClassItem from './class-item/class-item.component';
import ClassEdit from './class-edit/class-edit.component';

@WithRender
@Component({
    name: 'ClassList',
    props: ['classes', 'studioId'],
    components: {
        'class-item': ClassItem,
        'class-edit': ClassEdit
    }
})
export default class ClassList extends SportportVue {
    classes: Array<ClassVM>;
    editableClass: ClassVM = null;
    editableTitle: string = '';
    showEdit: boolean = false;
    studioId: number;

    get list() {
        return this.classes.slice();
    }

    editClass(classItem: ClassVM) {
        this.editableClass = classItem.clone();
        this.editableTitle = 'Редактирование: ' + (classItem.title || 'Новый класс');
        this.showEdit = true;
    }

    deleteClass(classItem: ClassVM) {
        this.$$store.dispatch(new ScheduleActions.DeleteClassAction(this.studioId, classItem));
    }
}