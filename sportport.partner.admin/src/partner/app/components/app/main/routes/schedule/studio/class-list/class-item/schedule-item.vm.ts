import { ScheduleVM, ClassVM, DayOfWeek } from '@sp/partner/view-models';

const DaysOfWeeksShort = {
    [DayOfWeek.Monday]: 'Пн',
    [DayOfWeek.Tuesday]: 'Вт',
    [DayOfWeek.Wednesday]: 'Ср',
    [DayOfWeek.Thursday]: 'Чт',
    [DayOfWeek.Friday]: 'Пт',
    [DayOfWeek.Sunday]: 'Вс',
    [DayOfWeek.Saturday]: 'Сб'
};

const DaysOfWeeksFull = {
    [DayOfWeek.Monday]: 'Понедельник',
    [DayOfWeek.Tuesday]: 'Вторник',
    [DayOfWeek.Wednesday]: 'Срежа',
    [DayOfWeek.Thursday]: 'Четверг',
    [DayOfWeek.Friday]: 'Пятница',
    [DayOfWeek.Sunday]: 'Воскресенье',
    [DayOfWeek.Saturday]: 'Суббота'
};

export default class ScheduleItemVM {
    isScheduled: boolean;
    labelShort: string;
    label: string;
    day: DayOfWeek;

    static get DaysOfWeek() {
        return <Array<DayOfWeek>><any>Object.keys(DaysOfWeeksFull);
    }

    constructor(day: DayOfWeek, schedules: Array<ScheduleVM>) {
        this.day = Number(day);
        let schedule = schedules.find(x => x.dayOfWeek === this.day);
        this.isScheduled = !!schedule;
        this.labelShort = DaysOfWeeksShort[day];
        this.label = DaysOfWeeksFull[day];
    }
}