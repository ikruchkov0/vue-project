import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { ScheduleVM, DayOfWeek, ClassVM, ScheduleStatus } from '@sp/partner/view-models';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './schedule-edit.template.html';
import { ValidationError } from 'class-validator';
import './schedule-edit.less';
import { ScheduleActions } from '@sp/partner/actions';
import { Modal, InlineTextInput } from '@sp/partner/shared-components';

@WithRender
@Component({
  name: 'ScheduleEdit',
  components: {
    modal: Modal,
    'inline-text-input': InlineTextInput
  },
  props: ['title', 'classItem', 'dayOfWeek']
})

export default class ScheduleEdit extends SportportVue {
  isBusy: boolean = false;
  validationResults: Array<ValidationError> = [];
  title: string;
  dayOfWeek: DayOfWeek;
  classItem: ClassVM;
  editableSchedules: Array<ScheduleVM> = [];

  get schedules() {
    return this.classItem.schedules.filter(x => x.dayOfWeek === this.dayOfWeek);
  }

  get studioId() {
    return Number.parseInt(this.$route.params.id);
  }

  get validationMessages() {
    return this.validationResults
      .map(result => Object.keys(result.constraints).map(x => result.constraints[x]))
      .reduce((array, value) => value.concat(array), []);
  }

  mounted() {
    if (!this.classItem) {
      return;
    }

    this.editableSchedules = this.schedules.map(x => x.clone());
  }

  async save() {
    this.isBusy = true;
    let schedulesForSave = this.editableSchedules.map(x => this.processScheduleForSaving(x)).filter(x => !!x);
    let deletedSchedules = this.getDeletedSchedules(schedulesForSave);
    schedulesForSave = schedulesForSave.concat(deletedSchedules);
    if (schedulesForSave.length === 0) {
      this.isBusy = false;
      this.$emit('close');
      return;
    }

    let validationErrors = await this.validate(schedulesForSave);
    if (validationErrors && validationErrors.length > 0) {
      this.validationResults = validationErrors;
      this.isBusy = false;
      return;
    }

    let action = new ScheduleActions.SaveScheduleAction(this.studioId, this.classItem.id, this.dayOfWeek, schedulesForSave);
    this.$$store.dispatch(action).then(() => {
      this.isBusy = false;
      this.$emit('close');
    }).catch((error) => {
      this.isBusy = false;
      this.$emit('close');
      throw error;
    });
  }

  addSchedule() {
    let newSchedule = new ScheduleVM({
      enabled: true,
      endTime: '13:00:00',
      quantity: 6,
      startTime: '11:00:00'
    });
    this.editableSchedules.push(newSchedule);
  }

  deleteSchedule(schedule: ScheduleVM) {
    schedule.status = ScheduleStatus.Deleted;
    this.editableSchedules = this.editableSchedules.filter(x => x !== schedule);
  }

  onChange(value, field, index) {
    let schedule = this.editableSchedules[index];
    schedule[field] = value;
    this.validate(this.editableSchedules).then(x => this.validationResults = x);
  }

  private async validate(schedules: Array<ScheduleVM>): Promise<Array<ValidationError>> {
    let validationErrors = await Promise.all(schedules.map(x => x.validate()));
    return validationErrors.reduce((array, value) => value.concat(array), []);
  }

  private processScheduleForSaving(schedule: ScheduleVM) {
    schedule.classCategoryId = this.classItem.classCategoryId;
    schedule.classId = this.classItem.id;
    schedule.studioId = this.classItem.studioId;
    schedule.dayOfWeek = this.dayOfWeek;
    let originalSchedule = this.classItem.schedules.find(x => x.id === schedule.id);
    if (!originalSchedule) {
      schedule.status = ScheduleStatus.Added;
      return schedule;
    }

    schedule.status = ScheduleStatus.Edited;
    return schedule;
  }

  private getDeletedSchedules(schedules: Array<ScheduleVM>) {
    return this.classItem.schedules.filter(x => !schedules.find(y => y.id === x.id) && x.dayOfWeek === this.dayOfWeek).map(x => {
      x.status = ScheduleStatus.Deleted;
      return x;
    });
  }
}
