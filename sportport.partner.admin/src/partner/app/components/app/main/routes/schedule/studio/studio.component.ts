import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './studio.template.html';
import { ScheduleActions } from '@sp/partner/actions';
import { StudioVM, ClassVM } from '@sp/partner/view-models';
import ClassList from './class-list/class-list.component';
import { BoxInfo } from '@sp/partner/shared-components';

@WithRender
@Component({
    name: 'Studio',
    props: ['studio', 'classes'],
    components: {
        'class-list': ClassList,
        'box-info': BoxInfo
    }
})
export default class Studio extends SportportVue {
    studio: StudioVM;
    classes: Array<ClassVM>;

    get title() {
        return this.studio.title;
    }

    get address() {
        return this.studio.address;
    }
}