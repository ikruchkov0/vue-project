import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './class-item.template.html';
import { ScheduleActions } from '@sp/partner/actions';
import { ScheduleVM, ClassVM, DayOfWeek } from '@sp/partner/view-models';
import './class-item.less';
import ScheduleEdit from './schedule-edit/schedule-edit.component';
import ScheduleItemVM from './schedule-item.vm';
import { Modal } from '@sp/partner/shared-components';

interface DayOfWeekMap {
    [day: string]: string;
}

@WithRender
@Component({
    name: 'ClassItem',
    props: ['classItem'],
    components: {
        'schedule-edit': ScheduleEdit,
        'modal': Modal
    }
})
export default class ClassItem extends SportportVue {
    classItem: ClassVM;
    showEditSchedule: boolean = false;
    editTitle: string;
    editSchedules: Array<ScheduleVM>;
    editDay: DayOfWeek;
    showDeleteApprove: boolean = false;

    get category() {
        return this.$$store.state.classes.classCategories.find(x => x.id === this.classItem.classCategoryId) || {};
    }

    get schedules() {
        let schedules = this.classItem.schedules;
        return ScheduleItemVM.DaysOfWeek.map(x => new ScheduleItemVM(x, schedules));
    }

    editSchedule(schedule: ScheduleItemVM) {
        this.editTitle = `${this.classItem.title} в ${schedule.label}`;
        this.editSchedules = this.classItem.schedules.filter(x => x.dayOfWeek === schedule.day).map(x => x.clone());
        this.editDay = schedule.day;
        this.showEditSchedule = true;
    }

    editClass() {
        this.$emit('edit', this.classItem);
    }

    deleteClass() {
        this.$emit('delete', this.classItem);
    }
}