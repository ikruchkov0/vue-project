import NotFound from './not-found/not-found.component';
import Error from './error/error.component';
import Company from './company/company.component';
import Dashboard from './dashboard/dashboard.component';
import Studios from './studios/studios.component';
import feedback from './feedback/feedback.component';
import Schedule from './schedule/schedule.component';
import Unauthorized from './unauthorized/unauthorized.component';

export default [
    {
        path: 'feedback',
        name: 'feedback',
        meta: {
            text: 'Обратная связь',
        },
        components: {
            default: feedback
        }
    },
    {
        path: 'studios',
        name: 'studios',
        meta: {
            text: 'Студии',
        },
        components: {
            default: Studios
        }
    },
    {
        path: 'company',
        name: 'company',
        meta: {
            text: 'Компания',
        },
        components: {
            default: Company
        }
    },
    {
        path: 'dashboard',
        name: 'dashboard',
        meta: {
            text: 'Dashboard',
        },
        component: Dashboard
    },
    {
        path: 'schedule/:id',
        name: 'schedule',
        meta: {
            text: 'Расписание',
        },
        component: Schedule
    },
    {
        name: '404',
        path: '404',
        component: NotFound
    },
    {
        name: '401',
        path: '401',
        component: Unauthorized
    },
    {
        name: 'error',
        path: 'error',
        component: Error
    },
    {
        path: ''
    },
    {
        path: '*',
        component: NotFound
    }
];