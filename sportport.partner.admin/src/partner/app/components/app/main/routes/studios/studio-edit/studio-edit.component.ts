import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { StudioVM } from '@sp/partner/view-models';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './studio-edit.template.html';
import { ValidationError } from 'class-validator';

import { Modal, TextInput, PhoneInput, AddonInput, AlertBox } from '@sp/partner/shared-components';

import { UIActions } from '@sp/partner/actions';

@WithRender
@Component({
  name: 'StudioEdit',
  components: {
    modal: Modal,
    'text-input': TextInput,
    'phone-input': PhoneInput,
    'addon-input': AddonInput,
    'alert-box': AlertBox
  },
  props: ['studio', 'isUpdateMode', 'title']
})

export default class StudioEdit extends SportportVue {
  validationResults: Array<ValidationError> = [];
  title: string;
  studio: StudioVM;
  _studio: StudioVM;
  isUpdateMode: Boolean;
  isBusy: Boolean = false;

  serverErrors: Array<string> = [];

  created() {
    if (this.isUpdateMode) {
      if (!this.studio.id)
        throw new TypeError('Studio id must be specified for UpdateMode');
      this._studio = this.studio;
      this.validate();
      return;
    }
    this._studio = new StudioVM();
    let companyId = this.$$store.state.company.currentCompany.id;
    this._studio.companyId = companyId;
    this._studio.enabled = false;

    // TODO: Remove later
    this._studio.city = 'Санкт-Петербург';
    this._studio.state = 'Ленинградская область';
  }

  onChange(value, field) {
    this._studio[field] = value;
    this.validate();
  }

  validate() {
    this._studio.validate().then((errors) => this.validationResults = errors);
  }

  async save() {
    console.log('SAVE');
    let errors = await this._studio.validate();
    console.log('ERRORS', errors);
    if (errors && errors.length > 0) {
      this.validationResults = errors;
      return;
    }

    this.isBusy = true;
    this.serverErrors = [];

    this.geocodeAddress()
      .then(location => {
        this._studio.lat = location.lat;
        this._studio.lng = location.lng;

        if (this.isUpdateMode) {
        this.$$store.dispatch(new StudioActions.UpdateStudioAction(this._studio.id, this._studio))
          .then(() => this.successSaveCallback())
          .catch(errors => this.failedSaveCallback(errors));
        } else {
          this.$$store.dispatch(new StudioActions.AddStudioAction(this._studio))
          .then(() => this.successSaveCallback())
          .catch(() => this.failedSaveCallback(errors));
        }

      })
      .catch(errors => {
        this.failedSaveCallback(errors);
      });
  }

  protected geocodeAddress(): Promise<any> {
    const geocoder = this.$$store.state.application.geocoder;
    const geocoderStatus = this.$$store.state.application.geocoderStatus;
    const studio = this._studio;
    let address = `Россия, ${studio.state}, ${studio.city}, ${studio.address}`;

    return new Promise((resolve, reject) => {
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status === geocoderStatus.OK) {
          resolve({
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
          });
        } else {
          reject(['Введенный вами адрес не определяется на карте. Пожалуйста, проверьте адрес. Адрес должен включать только название улицы и номер дома.']);
        }
      });
    });
  }

  protected successSaveCallback() {
    this.$$store.dispatch(UIActions.hideAlert);
    this.isBusy = false;
    this.$emit('close');
  }

  protected failedSaveCallback(errors) {
    this.isBusy = false;
    this.serverErrors = errors;
  }
}
