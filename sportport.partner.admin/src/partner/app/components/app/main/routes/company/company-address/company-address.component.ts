import SportportVue from '@sp/partner/sportport-vue';
import { ValidationError } from 'class-validator';
import Component from 'vue-class-component';
import * as WithRender from './company-address.template.html';
import { CompanyVM } from '@sp/partner/view-models';

import TextInput from '../../../shared-components/text-input/text-input.component';

@WithRender
@Component({
    name: 'CompanyAddress',
    components: {
        'text-input': TextInput
    },
    props: ['company', 'change', 'validationResults']
})
export default class CompanyAddress extends SportportVue {
    validationResults: Array<ValidationError>;
    change: (value, field: string) => void;
    company: CompanyVM;

    get city() { return this.company.city; }
    get address() { return this.company.address; }
    get state() { return this.company.state; }
    get zip() { return this.company.zip; }

    onChange(value, field) {
        this.change(value, field);
    }

    get isValid() {
        let fieldList = ['city', 'state', 'address', 'state', 'zip'];
        return !this.validationResults || this.validationResults.length === 0 || this.validationResults.filter(x => fieldList.some(t => t === x.property)).length === 0;
    }
}