import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './registration-header.template.html';

import ContentHeader from '../../../shared-components/content-header/content-header.component';

@WithRender
@Component({
    name: 'RegistrationHeader',
    components: {
        'content-header': ContentHeader
    }
})
export default class RegistrationHeader extends SportportVue {
}