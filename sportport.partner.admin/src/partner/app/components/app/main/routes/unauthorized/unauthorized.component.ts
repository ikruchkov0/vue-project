import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './unathorized.template.html';

@WithRender
@Component
export default class Unauthorized extends SportportVue {
}