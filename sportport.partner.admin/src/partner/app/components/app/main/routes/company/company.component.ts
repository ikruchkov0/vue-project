import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { CompanyVM } from '@sp/partner/view-models';
import { ValidationError } from 'class-validator';
import * as WithRender from './company.template.html';
import { CompanyActions, UIActions, ApplicationActions } from '@sp/partner/actions';

import CompanyInfo from './company-info/company-info.component';
import CompanyAddress from './company-address/company-address.component';

type ModelStatuses = 'loaded' | 'saving' | 'loading';

@WithRender
@Component({
    name: 'Company',
    components: {
        'company-info': CompanyInfo,
        'company-address': CompanyAddress
    }
})
export default class Company extends SportportVue {
    company: CompanyVM = null;
    status: ModelStatuses = 'loaded';

    validationErrors: Array<ValidationError> = [];

    get companyId() {
        return Number.parseInt(this.$route.params.id);
    }

    created() {
        if (this.$$store.state.company.currentCompany) {
            this.company = this.$$store.state.company.currentCompany.clone();
        }
        this.status = 'loading';
        this.$$store.dispatch(new CompanyActions.LoadCompanyAction()).then(() => {
            this.company = this.$$store.state.company.currentCompany || this.newCompany();
            if (!this.$$store.state.company.currentCompany) {
                this.$$store.dispatch(new UIActions.ShowAlertAction('Данные о компании должны быть заполнены и сохранены для того чтобы получить доступ к остальным разделам.', 'warning'));
            }
            this.validate();
            this.status = 'loaded';
        });
    }

    onChange(value, field) {
        this.$set(this.company, field, value);
        this.validate();
    }

    validate() {
        if (!this.company) {
            return Promise.reject([]);
        }
        return this.company.validate().then((errors) => {
            this.validationErrors = errors;
        });
    }

    get isValid() {
        return !this.validationErrors || this.validationErrors.length === 0;
    }

    save() {
        this.status = 'saving';
        this.validate().then(() => {
            if (this.validationErrors && this.validationErrors.length > 0) {
                this.status = 'loaded';
                return;
            }
            return this.company.id ? this.updateCompany() : this.saveCompany();
        });
    }

    private updateCompany() {
        return this.$$store.dispatch(new CompanyActions.UpdateCompanyAction(this.company)).then(() => this.status = 'loaded').catch(() => this.status = 'loaded');
    }

    private saveCompany() {
        return this.$$store.dispatch(new CompanyActions.CreateCompanyAction(this.company))
            .then(() => {
                return this.$$store.dispatch(new ApplicationActions.InitApplication()).then(() => {
                    return this.$$store.dispatch(UIActions.hideAlert).then(() => this.status = 'loaded');
                });
            })
            .catch(() => this.status = 'loaded');
    }

    private newCompany() {
        return new CompanyVM({
            title: '',
            phone: '',
            email: '',
            city: '',
            address: '',
            state: ''
        });
    }
}