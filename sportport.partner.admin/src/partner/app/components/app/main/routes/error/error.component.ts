import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './error.template.html';

@WithRender
@Component({name: 'Error'})
export default class Error extends SportportVue {
}