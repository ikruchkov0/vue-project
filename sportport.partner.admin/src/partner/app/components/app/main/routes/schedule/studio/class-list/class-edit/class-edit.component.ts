import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { ScheduleVM, DayOfWeek, ClassVM, ScheduleStatus } from '@sp/partner/view-models';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './class-edit.template.html';
import { ValidationError } from 'class-validator';
import { Modal, TextInput, InlineTextInput, SelectInput } from '@sp/partner/shared-components';
import { ScheduleActions } from '@sp/partner/actions';

@WithRender
@Component({
  name: 'ClassEdit',
  components: {
    modal: Modal,
    'text-input': TextInput,
    'select-input': SelectInput
  },
  props: ['title', 'classItem']
})

export default class ClassEdit extends SportportVue {
  isBusy: boolean = false;
  validationResults: Array<ValidationError> = [];
  title: string;
  classItem: ClassVM;

  get studioId() {
    return Number.parseInt(this.$route.params.id);
  }

  get categories() {
    return this.studio.classCategories || [];
  }

  get studio() {
      return this.$$store.state.studios.companyStudios.find(x => x.id === this.studioId);
  }

  get validationMessages() {
    return this.validationResults
      .map(result => Object.keys(result.constraints).map(x => result.constraints[x]))
      .reduce((array, value) => value.concat(array), []);
  }

  mounted() {
    if (!this.classItem) {
      return;
    }
  }

  async save() {
    let errors = await this.validate(this.classItem);
    if (errors && errors.length > 0) {
      this.validationResults = errors;
      return;
    }
    this.isBusy = true;

    let action = isNaN(this.classItem.id)
      ? new ScheduleActions.AddClassAction(this.studioId, this.classItem)
      : new ScheduleActions.SaveClassAction(this.studioId, this.classItem);

    this.$$store.dispatch(action).then(() => {
      this.isBusy = false;
      this.$emit('close');
    }).catch((error) => {
      this.isBusy = false;
      this.$emit('close');
      throw error;
    });
  }

  onChange(value, field) {
    this.classItem[field] = value;
    this.validate(this.classItem);
  }

  private validate(classItem: ClassVM): Promise<Array<ValidationError>> {
    return classItem.validate().then((errors) => this.validationResults = errors);
  }
}
