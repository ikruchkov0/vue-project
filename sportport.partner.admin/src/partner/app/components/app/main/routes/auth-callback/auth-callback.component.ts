import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './auth-callback.template.html';

import { ProfileActions, ApplicationActions } from '@sp/partner/actions';
import config from '@sp/shared/config';

@WithRender
@Component({name: 'AuthCallback'})
export default class AuthCallback extends SportportVue {
    created() {
        if ((window.location.origin + window.location.pathname + window.location.search) === config.Client.PartnerFrontendSilentRedirectUrl) {
            debugger;
            this.$$store.dispatch(new ProfileActions.ProcessSilentCallbackAction(() => {}));
            return;
        }
        this.$$store.dispatch(new ProfileActions.ProcessCallbackAction(() => this.redirect()));
    }

    redirect() {
        return this.$$store.dispatch(new ApplicationActions.InitApplication()).then(() => this.$$store.dispatch(new ApplicationActions.RedirectToDefault(true)));
    }
}