import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { StudioVM, ClassCategoryVM } from '@sp/partner/view-models';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './categories-edit.template.html';
import { ValidationError } from 'class-validator';

import { Modal } from '@sp/partner/shared-components';

@WithRender
@Component({
  name: 'CategoriesEdit',
  components: {
    modal: Modal
  },
  props: ['studio', 'title']
})

export default class CategoriesEdit extends SportportVue {
  title: string;
  studio: StudioVM;
  _currentCategories: Array<ClassCategoryVM>;
  _classCategories: Array<ClassCategoryVM> = [];
  isBusy: Boolean = false;

  created() {
    this._currentCategories = this.studio.classCategories ? this.studio.classCategories.slice() : [];
    this._classCategories = this.$$store.state.classes.classCategories.slice();
    this._classCategories.forEach(cc => cc.selected = this._currentCategories.find(scc => scc.id === cc.id) ? true : false);
  }

  async save() {
    this.isBusy = true;
    this.$$store.dispatch(new StudioActions.UpdateStudioClassCategoriesAction(this.studio.id, this._classCategories.filter(scc => scc.selected)))
    .then(() => {
      this.isBusy = false;
      this.$emit('close');
    })
    .catch(() => this.isBusy = false);
  }
}