import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './dashboard.template.html';
import './dahsboard.less';
import { PartnerStatisticVM } from '@sp/partner/view-models';
import { LoadStatisticPerMonthAction } from '@sp/partner/actions';

import Studio from './studio/studio.component';


@WithRender
@Component({
    name: 'Dashboard',
    components: {
        studio: Studio
    }
})
export default class Dashboard extends SportportVue {
    statistic: PartnerStatisticVM = null;

    created() {
        if (this.$$store.state.statistic.data) {
            this.statistic = this.$$store.state.statistic.data.clone();
        }
        this.$$store.dispatch(new LoadStatisticPerMonthAction()).then(() => {
            this.statistic = this.$$store.state.statistic.data.clone();
        });
    }

    get studios() {
        if (!this.statistic) {
            return;
        }
        return this.statistic.affiliates;
    }

    get totalIncome() {
        if (!this.statistic) {
            return;
        }
        return this.statistic.revenue;
    }

    get totalScheduled() {
        if (!this.statistic) {
            return;
        }
        return this.statistic.visited + this.statistic.canceled + this.statistic.pending;
    }
}