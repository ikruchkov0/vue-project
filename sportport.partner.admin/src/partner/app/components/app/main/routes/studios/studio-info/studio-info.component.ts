import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { StudioVM } from '@sp/partner/view-models';
import { ValidationError } from 'class-validator';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './studio-info.template.html';
import { BoxInfo, AlertBox, CollapsableBox, Modal, GoogleMapDirective } from '@sp/partner/shared-components';

import StudioEdit from '../studio-edit/studio-edit.component';
import CategoriesEdit from '../categories-edit/categories-edit.component';
import FacilitiesEdit from '../facilities-edit/facilities-edit.component';
import StudioPartialInfo from '../studio-partial-info/studio-partial-info.component';
import '../studios.less';


@WithRender
@Component({
    name: 'StudioInfo',
    components: {
        'collapsable-box': CollapsableBox,
        'studio-edit': StudioEdit,
        'categories-edit': CategoriesEdit,
        'facilities-edit': FacilitiesEdit,
        'box-info': BoxInfo,
        'studio-partial-info': StudioPartialInfo,
        'alert-box': AlertBox,
        'modal': Modal
    },
    directives: {
        'google-map': GoogleMapDirective
    },
    props: ['studio', 'isGetDetails']
})

export default class StudioInfo extends SportportVue {
    private readonly DISABLED_WARNING = 'Студия отключена. Вся информация связанная со студией недоступна для пользователей';
    private readonly CLASS_CATEGORIES_WARNING = 'Виды классов не заданы. Вы не сможете добавлять расписание без указания видов классов.';
    studio: StudioVM;
    showDetails: Boolean = false;

    showEditInfo: Boolean = false;
    showEditCategories: Boolean = false;
    showEditFacilities: Boolean = false;
    showDeleteApprove: Boolean = false;

    isGetDetails: Boolean;

    attentions: Array<string> = [];

    isBusy: Boolean = false;

    validationErrors: Array<ValidationError> = [];

    get studioId() {
        return this.studio.id;
    }

    get showAttentions() {
        return this.attentions && this.attentions.length > 0;
    }

    get GoogleMaps() {
        return this.$$store.state.application.googleMaps;
    }

    created() {
        if (this.isGetDetails)
            this.studioDetails();
    }

    updated() {
        if (this.showDetails) {
            this._checkStudioData();
        }
    }

    get facilities() {
        return this.$$store.state.studios.facilitiesDict
            .filter(f => this.studio.facilities.indexOf(f.id) !== -1)
            .map(f => f.title);
    }

    studioDetails() {
        this.showDetails = true;
        this.isBusy = true;

        this.$$store.dispatch(new StudioActions.LoadStudioDetailsAction(this.studioId))
        .then(() => {
            this.isBusy = false;
        })
        .catch(() => console.log('Error'));
    }

    hideDetails() {
        this.showDetails = false;
        this.isBusy = false;
    }

    _checkStudioData() {
        if (!this.studio.enabled) {
            if (!this.attentions.find(x => x === this.DISABLED_WARNING))
                this.attentions.push(this.DISABLED_WARNING);
        } else {
            if (this.attentions.find(x => x === this.DISABLED_WARNING))
                this.attentions = this.attentions.filter(x => x !== this.DISABLED_WARNING);
        }
        if (!this.studio.classCategories || this.studio.classCategories.length === 0) {
            if (!this.attentions.find(x => x === this.CLASS_CATEGORIES_WARNING))
                this.attentions.push(this.CLASS_CATEGORIES_WARNING);
        } else {
            if (this.attentions.find(x => x === this.CLASS_CATEGORIES_WARNING))
                this.attentions = this.attentions.filter(x => x !== this.CLASS_CATEGORIES_WARNING);
        }
    }

    async deleteStudio() {
        this.showDeleteApprove = false;
        this.isBusy = true;
        this.$$store.dispatch(new StudioActions.DeleteStudioAction(this.studio.id))
        .then(() => {
            this.isBusy = false;
        })
        .catch(() => {
            this.isBusy = false;
        });
    }

    async toggleStudio() {
        this.isBusy = true;
        let studioUpdate = this.studio.clone();
        studioUpdate.enabled = !studioUpdate.enabled;
        this.$$store.dispatch(new StudioActions.UpdateStudioAction(studioUpdate.id, studioUpdate))
        .then(() => {
            this.isBusy = false;
        })
        .catch(() => {
            this.isBusy = false;
        });
    }
}