import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './not-found.template.html';

@WithRender
@Component({name: 'NotFound'})
export default class NotFound extends SportportVue {
}