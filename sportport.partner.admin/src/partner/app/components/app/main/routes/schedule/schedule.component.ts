import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import * as WithRender from './schedule.template.html';
import { ScheduleActions, StudioActions } from '@sp/partner/actions';
import { DayOfWeek, ClassVM } from '@sp/partner/view-models';
import Studio from './studio/studio.component';
import ClassEdit from './studio/class-list/class-edit/class-edit.component';

@WithRender
@Component({
    name: 'Schedule',
    components: {
        'studio': Studio,
        'class-edit': ClassEdit
    }
})
export default class Schedule extends SportportVue {
    status: string = 'loading';
    showAdd: boolean = false;

    get studioId() {
        return Number.parseInt(this.$route.params.id);
    }

    get studio() {
        return this.$$store.state.studios.companyStudios.find(x => x.id === this.studioId);
    }

    get classes() {
        let studio = this.$$store.state.schedule.studios[this.studioId];
        if (!studio) {
            return [];
        }
        return studio;
    }

    created() {
        this.loadStudio().then(() => this.$watch(() => this.studioId, () => this.loadStudio()));
    }

    private loadStudio() {
        if (!this.studioId) {
            return;
        }
        this.status = 'loading';
        const studioDetailsPromise = this.$$store.dispatch(new StudioActions.LoadStudioDetailsAction(this.studio.id));
        const studioClassesPromise = this.$$store.dispatch(new ScheduleActions.LoadClassesForStudioAction(this.studioId));

        return Promise.all([studioDetailsPromise, studioClassesPromise])
            .then(() => this.status = 'loaded')
            .catch(() => this.status = 'loaded');
    }

    addClass() {
        let newClass = new ClassVM({
           studioId: this.studioId,
           schedules: [],
           enabled: true
        });
        return newClass;
    }
}