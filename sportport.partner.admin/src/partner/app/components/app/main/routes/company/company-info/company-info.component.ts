import SportportVue from '@sp/partner/sportport-vue';
import { ValidationError } from 'class-validator';
import Component from 'vue-class-component';
import * as WithRender from './company-info.template.html';
import { CompanyVM } from '@sp/partner/view-models';

import TextInput from '../../../shared-components/text-input/text-input.component';
import PhoneInput from '../../../shared-components/phone-input/phone-input.component';

@WithRender
@Component({
    name: 'CompanyInfo',
    components: {
        'text-input': TextInput,
        'phone-input': PhoneInput
    },
    props: ['company', 'change', 'validationResults']
})
export default class CompanyInfo extends SportportVue {
    validationResults: Array<ValidationError>;
    change: (value, field: string) => void;
    company: CompanyVM;

    get companyName() { return this.company.title; }
    get phone() { return this.company.phone; }
    get email() { return this.company.email; }
    get mobile() { return this.company.mobile; }

    onChange(value, field) {
        this.change(value, field);
    }

    get isValid() {
        let fieldList = ['title', 'phone', 'email', 'mobile'];
        return !this.validationResults || this.validationResults.length === 0 || this.validationResults.filter(x => fieldList.some(t => t === x.property)).length === 0;
    }
}