import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { StudioVM } from '@sp/partner/view-models';
import { ValidationError } from 'class-validator';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './studios.template.html';

import {  } from '@sp/partner/shared-components';
import { UIActions } from '@sp/partner/actions';

import StudioInfo from './studio-info/studio-info.component';
import StudioEdit from './studio-edit/studio-edit.component';

@WithRender
@Component({
    name: 'Studios',
    components: {
        'studio-info': StudioInfo,
        'studio-edit': StudioEdit
    }
})

export default class Studios extends SportportVue {
    showEdit: Boolean = false;
    studiosState: any;

    get studios(): Array<StudioVM> {
        return this.$$store.state.studios.companyStudios;
    }

    created() {
        this.$$store.dispatch(new StudioActions.LoadStudiosAction())
        .then(() => {
            if (!this.studios || this.studios.length === 0) {
                this.$$store.dispatch(new UIActions.ShowAlertAction('Добавьте студию, чтобы иметь возможность редактировать классы и раписание.', 'warning'));
            }
        })
        .catch(() => {

        });
    }
}