import SportportVue from '@sp/partner/sportport-vue';
import Component from 'vue-class-component';
import { StudioVM } from '@sp/partner/view-models';
import { ValidationError } from 'class-validator';
import { StudioActions } from '@sp/partner/actions';
import * as WithRender from './studio-partial-info.template.html';
import { BoxInfo } from '@sp/partner/shared-components';

import StudioEdit from '../studio-edit/studio-edit.component';
import '../studios.less';


@WithRender
@Component({
    name: 'StudioPartialInfo',
    components: {
        'box-info': BoxInfo
    },
    props: {
    'title': { type: String, required: true },
    'iconClass': { type: String, required: true }
  }
})

export default class Studio extends SportportVue {
  title: string;
  iconClass: string;

  // Emits 'edit' event
}