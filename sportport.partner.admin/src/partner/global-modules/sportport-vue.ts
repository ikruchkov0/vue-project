import Vue from 'vue';
import SportportStore from '../app/store';

class SportportVue extends Vue {
    get $$store(): SportportStore {
        return <SportportStore>this.$store;
    }
}


export default SportportVue;