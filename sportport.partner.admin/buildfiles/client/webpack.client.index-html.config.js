const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');

module.exports = function () {
    return {
        plugins: [
            new CopyWebpackPlugin([
                { from: 'node_modules/bootstrap/dist/css', to: 'css/' }, 
                { from: 'src/assets/partner/css', to: 'css/' },               
                { from: 'src/assets/partner/fonts', to: 'fonts/' },
            ], { debug: 'warning' }),
            new HtmlWebpackPlugin({
                inject: 'head',
                title: 'SportPort',
                template: '!!html-loader!src/client/index.html'
            }),
            new HtmlWebpackIncludeAssetsPlugin({
                assets: [         
                'css/bootstrap.min.css',
                'css/font-awesome.min.css',
                ],
                append: false
            })
        ]
    };
};

