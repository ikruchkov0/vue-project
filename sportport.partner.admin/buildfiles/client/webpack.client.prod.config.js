const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const prodConfigFn = require('../common/webpack.prod.config');
const indexHtmlFn = require('./webpack.client.index-html.config');

module.exports = prodConfigFn('./src/client/entry.ts', indexHtmlFn, '/client/', path.resolve('./dist/client'));