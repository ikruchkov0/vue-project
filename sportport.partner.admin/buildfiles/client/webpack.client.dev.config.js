const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const devConfigFn = require('../common/webpack.dev.config');
const indexHtmlFn = require('./webpack.client.index-html.config');

module.exports = devConfigFn('./src/client/entry.ts', indexHtmlFn, '/', path.resolve('./dist/client'));
