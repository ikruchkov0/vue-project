const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin')

const baseConfigFn = require('../common/webpack.base.config');

var publicPath = '/client';

const baseConfig = baseConfigFn(['./src/client/entry-ssr.ts'], () => { }, publicPath, path.resolve('./dist/client.ssr'));

const config = {
    target: 'node',
    devtool: 'source-map',
    output: {
        libraryTarget: 'commonjs2',
        filename: 'ssr.[name].js'
    },
    plugins: [
        //new VueSSRServerPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"',
                VUE_ENV: '"server"',
                ENV_PUBLIC_PATH: '"' + publicPath + '"'
            }
        })
    ]
};

const result = merge(baseConfig, config);

module.exports = result;


