const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');

module.exports = function () {
    return {
        plugins: [
            new CopyWebpackPlugin([
                { from: 'node_modules/bootstrap/dist/css', to: 'css/' },
                { from: 'node_modules/admin-lte/dist/css', to: 'css/' },
                { from: 'node_modules/admin-lte/dist/css/skins/', to: 'css/skins' },
                { from: 'node_modules/admin-lte/plugins/pace', to: 'css/plugins' },
                { from: 'src/assets/partner/css', to: 'css/' },
                { from: 'src/assets/partner/fonts', to: 'fonts/' },
                { from: 'node_modules/admin-lte/dist/img', to: 'img/' },
                { from: 'node_modules/bootstrap/dist/fonts', to: 'fonts/' }
            ], { debug: 'warning' }),
            new HtmlWebpackPlugin({
                inject: 'head',
                title: 'SportPort Партнерский портал',
                template: '!!html-loader!src/partner/index.html'
            }),
            new HtmlWebpackIncludeAssetsPlugin({
                assets: [
                    'css/skins/skin-blue.min.css',
                    'css/AdminLTE.min.css',
                    'css/font-awesome.min.css',
                    'css/ionicons.min.css',
                    'css/bootstrap.min.css',
                    'css/plugins/pace.min.css'
                ],
                append: false
            })
        ]
    };
};

