const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const devConfigFn = require('../common/webpack.dev.config');
const indexHtmlFn = require('./webpack.partner.index-html.config');

module.exports = devConfigFn('./src/partner/entry.ts', indexHtmlFn, '/', path.resolve('./dist/partner'));