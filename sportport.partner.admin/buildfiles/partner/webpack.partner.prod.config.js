const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const prodConfigFn = require('../common/webpack.prod.config');
const indexHtmlFn = require('./webpack.partner.index-html.config');

module.exports = prodConfigFn('./src/partner/entry.ts', indexHtmlFn, '/partner/', path.resolve('./dist/partner'));