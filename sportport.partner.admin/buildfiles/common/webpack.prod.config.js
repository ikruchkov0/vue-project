const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");

const baseConfigFn = require('./webpack.base.config');

function constructFn(entry, indexHtmlFn, publicPath, outputDir) {
    const baseConfig = baseConfigFn(entry, indexHtmlFn, publicPath, outputDir);

    const prodConfig = {
        devtool: 'source-map',
        stats: 'verbose',
        plugins: [
            /*new webpack.optimize.UglifyJsPlugin({
                comments: false
            }),*/
            new CleanWebpackPlugin([outputDir], {
                verbose: true,
                root: path.resolve(outputDir, '..')
            }),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: '"production"',
                    ENV_PUBLIC_PATH: '"' + publicPath + '"'
                }
            })
        ],
        stats: 'verbose'
    };

    return merge(baseConfig, prodConfig);
}

module.exports = constructFn; 