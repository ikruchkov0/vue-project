const merge = require('webpack-merge');
const webpack = require('webpack');

const baseConfigFn = require('./webpack.base.config');

function constructFn(entry, indexHtmlFn, publicPath, outputDir) {
    const baseConfig = baseConfigFn(entry, indexHtmlFn, publicPath, outputDir);
    const devConfig = {
        devtool: 'inline-source-map',
        stats: 'errors-only',
        devServer: {
            port: 50002,
            noInfo: true,
            proxy: {
                "/api": {
                    target: "http://localhost:50001",
                    bypass: function (req, res, proxyOptions) {
                        if (req.url.indexOf("/api") === -1) {
                            console.log("Skipping proxy for browser request. " + req.url);
                            return "/index.html";
                        }
                    }
                }
            }
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: '"development"'
                }
            })
        ]
    };

    return merge(baseConfig, devConfig);
}

module.exports = constructFn; 