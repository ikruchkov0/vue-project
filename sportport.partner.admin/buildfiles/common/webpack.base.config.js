const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');

function constructFn(entry, indexHtmlFn, publicPath, outputDir) {
    const indexHtmlConfig = indexHtmlFn();
    const config = {
        entry: !Array.isArray(entry) ? [
            'babel-polyfill',
            'whatwg-fetch',
            entry
        ] : entry,
        output: {
            publicPath: publicPath,
            path: outputDir,
            filename: '[name].[hash].js'
        },
        externals: {
            'jsdom-global': 'commonjs jsdom-global',
            'jsdom': 'commonjs jsdom',
            'node-fetch': 'commonjs node-fetch'
        },
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.tsx?$/,
                    loader: 'tslint-loader',
                    exclude: /(node_modules)/,
                },
                { test: /index\.html$/, loader: 'html-loader' },
                { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
                { test: /\.html$/, loader: 'vue-template-loader' },
                {
                    test: /\.styl$/,
                    use: ExtractTextPlugin.extract({
                        loader: ['css-loader', 'stylus-loader'],
                        fallback: 'style-loader'
                    })
                },
                {
                    test: /\.less$/,
                    use: ExtractTextPlugin.extract({
                        loader: ['css-loader', 'less-loader'],
                        fallback: 'style-loader'
                    })
                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        loader: ['css-loader'],
                        fallback: 'style-loader'
                    })
                },
                // {
                //     test: /iview\/.*?js$/,
                //     loader: 'babel-loader'
                // },
                {
                    test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
                    loader: 'url-loader?limit=1024'
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.ts', '.tsx', '.html', '.json'],
            modules: ['node_modules'],
            alias: {
                'applicationUrls': path.resolve('../../../applicationUrls.json'),
                '@sp/partner': path.resolve('./src/partner/global-modules/'),
                '@sp/client': path.resolve('./src/client/global-modules/'),
                '@sp/shared': path.resolve('./src/shared/')
            }
        },
        plugins: [
            FailPlugin,
            new webpack.LoaderOptionsPlugin({
                options: {
                    less: {
                        sourceMap: true,
                    },
                    css: {
                        sourceMap: true,
                        importLoaders: 1,
                    },
                    tslint: {
                        emitErrors: true,
                        failOnHint: true
                    }
                }
            }),
            new ExtractTextPlugin({ filename: 'app.[hash].css', allChunks: true })
        ]
    };

    return merge(config, indexHtmlConfig);
}

module.exports = constructFn;